<?php
/**
 RemoteEngineerHistoryContoller.class.php
 * 
 * Implementation of Remote Engineer History for SkyLine
 * Part of Skyline Remote Engineer History Project
 *
 * @author     Krishnam Raju Nalla <k.nalla@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems     
 * @version    1.00
 * 
 * Changes
 * Date        Version Author                Reason
 * ****************************************************************************** */
require_once('CustomSmartyController.class.php');
include_once(APPLICATION_PATH . '/include/xml2array.php');

class RemoteEngineerHistoryController extends CustomSmartyController{
    
    public $config;
    
    public function __construct() {
        parent::__construct();
        $this->config = $this->readConfig('application.ini');
    }
    
    public function RM_PutRemoteEngineerHistoryAction($args) {
        $json = @file_get_contents('php://input');
        $params = xml2array($json);
        $this->log($params,"Remote_Engineer_History_");
        $user_model = $this->loadModel('Users');
        $jobs_model = $this->loadModel('Job');
        $reh_model = $this->loadModel('RemoteEngineerHistory');
        $user = $user_model->CheckUser($params['PutRemoteEngineerHistory']['SLUsername'], $params['PutRemoteEngineerHistory']['SLPassword']);
        $data = array();
        if($user->AuthOK == 1)
        {
            $sklJobNumber = $jobs_model->getSkylineJobNumber($user->ServiceProviderID,$params['PutRemoteEngineerHistory']['History']['SBJobNo']);
            $dateArr = explode("/",$params['PutRemoteEngineerHistory']['History']['Date']);
            $data['AppointmentID'] = $params['PutRemoteEngineerHistory']['History']['AppID'];
            if($sklJobNumber)
            {
                $data['JobID'] = $sklJobNumber->JobID;
            }
            else
            {
                $data['JobID'] = NULL;
            }
            $data['SBJobNo'] = $params['PutRemoteEngineerHistory']['History']['SBJobNo'];
            $data['Date'] = $dateArr[2]."-".$dateArr[1]."-".$dateArr[0];
            $data['Time'] = $params['PutRemoteEngineerHistory']['History']['Time'].":00";
            $data['UserCode'] = $params['PutRemoteEngineerHistory']['Username'];
            $data['Action'] = $params['PutRemoteEngineerHistory']['History']['Action'];
            $data['Notes'] = $params['PutRemoteEngineerHistory']['History']['Notes'];
            $data['URL'] = $params['PutRemoteEngineerHistory']['History']['URL'];
            $data['Latitude'] = $params['PutRemoteEngineerHistory']['History']['Latitude'];
            $data['Longitude'] = $params['PutRemoteEngineerHistory']['History']['Longitude'];
            $result = $reh_model->captureRemoteEngineerHistoryDetails($data);
            if($result > 0)
                $response = 0;
        }
        else
            $response = 1;
        $finalResponse='<Response><APIName>RM_PutRemoteEngineerHistory</APIName>';
        $finalResponse.='<UUID>'.$params['PutRemoteEngineerHistory']['UUID'].'</UUID>';
        $finalResponse.='<AppID>'.$params['PutRemoteEngineerHistory']['History']['AppID'].'</AppID>';
        if($response == 0)
        {
            $finalResponse.='<ResponseCode>SC0001</ResponseCode>';
            $finalResponse.='<ResponseDescription>Success</ResponseDescription>';
        }
        else
        {
            $finalResponse.='<ResponseCode>SC0002</ResponseCode>';
            $finalResponse.='<ResponseDescription>Failure</ResponseDescription>';
        }
        $finalResponse.='</Response>';
        echo $finalResponse;
    }
}
?>