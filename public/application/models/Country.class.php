<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Country Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */


class Country extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.CountryID', 't1.CountryCode', 't1.Name',  't1.InternationalCode', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "country AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID";
    private $table     = "country";
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        return  $output;
        
    }
    
    
    
    /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['CountryID']) || !$args['CountryID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
     
     
      /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountryCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY CountryID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
     /**
     * Description
     * 
     * This method is used for to validate code for given brand.
     *
     * @param interger $CountryCode  
     * @param interger $BrandID.
     * @param interger $CountryID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
     public function isValidAction($CountryCode, $BrandID, $CountryID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountryID FROM '.$this->table.' WHERE CountryCode=:CountryCode AND BrandID=:BrandID AND CountryID!=:CountryID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':CountryCode' => $CountryCode, ':BrandID' => $BrandID, ':CountryID' => $CountryID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['CountryID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (Name, CountryCode, InternationalCode, Status, BrandID)
//            VALUES(:Name, :CountryCode, :InternationalCode, :Status, :BrandID)';
        $fields = explode(', ', 'Name, CountryCode, InternationalCode, Status, BrandID');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::Country()->insertCommand($fields);        
        
        if(!isset($args['CountryCode']) || !$args['CountryCode'])
        {
            $args['CountryCode'] = $this->getCode($args['BrandID'])+1;//Preparing next country code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['CountryCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':Name' => $args['Name'], ':CountryCode' => $args['CountryCode'], ':InternationalCode' => strtoupper($args['InternationalCode']), ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['data_inserted_msg']);
        }
         else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT CountryID, CountryCode, Name, InternationalCode, Status, BrandID FROM '.$this->table.' WHERE CountryID=:CountryID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':CountryID' => $args['CountryID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['CountryCode'], $args['BrandID'], $args['CountryID']))
        {        
            
            $fields = explode(', ', 'Name, CountryCode, InternationalCode, Status, BrandID');
            $fields = array_combine($fields , $fields);
            
            $where = 'CountryID=:CountryID';

            $sql = TableFactory::Country()->updateCommand($fields, $where);
            
            #$this->controller->log( $sql );
            
            /* Execute a prepared statement by passing an array of values */
//            $sql = 'UPDATE '.$this->table.' SET Name=:Name, CountryCode=:CountryCode, InternationalCode=:InternationalCode, Status=:Status, BrandID=:BrandID
//            WHERE CountryID=:CountryID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':Name' => $args['Name'], ':CountryCode' => $args['CountryCode'], ':InternationalCode' => strtoupper($args['InternationalCode']), ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':CountryID' => $args['CountryID']));
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * getCountryId
     *  
     * Get the Country ID from the country name
     * 
     * @param string $cName  Country Name
     * 
     * @return integer  Country ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getCountryId($cName) {
        $sql = "
                SELECT
			`CountryID`
		FROM
			`country`
		WHERE
			`Name` = '$cName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['CountryID']);                                    /* Country exists so return Country ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
        
}
?>