{extends "Performance/MasterLayout.tpl"}

{block name=scripts append}

<script type="text/javascript">	
google.load('visualization', '1');
function drawPanels() {
	var h = $('#container').height()-$('#menu-panel').height()-25;
	var chart_height = h-$('#gauge-panel').height()-5;
    $('#left-panel').css("height",h);
    $('#right-panel').css("height",h);
    $('#chart-panel').css("height",chart_height);
    $('#graph').css("height",chart_height);
    $('#legend').css("height",chart_height-10);
    $('.overflow').css("height",h-40);
}
function drawVisualization() {
    var wrapper = new google.visualization.ChartWrapper({
    	'chartType': 'ColumnChart',
        'dataTable': [   

            ['',{foreach $graph as $graphdata}"{$graphdata.Name|lower|capitalize}"{if not $graphdata@last},{/if}{/foreach}],
                   ['',{foreach $graph as $graphdata}{$graphdata.Count}{if not $graphdata@last},{/if}{/foreach}]
        ],
        'options': { 'title': "Open Jobs {$graphtitle}{$ncbFilter|lower|capitalize} {if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}",
        {if isset($graphcolour) }
            'colors': [  
                {foreach $graphcolour key=k item=i}
                    '#{$i}' {if not $i@last},{/if}
                {/foreach}    
            ],
        {/if} 
                  'backgroundColor': 'transparent',
                  'colour':'#1F5676',                  
                  'chartArea': { 'left':10,'top':20, 'width': '95%', 'height': '85%'},
                  'legend': { 'position': 'none', 'textStyle': { 'color': '#1F5676', 'fontSize': 10 } },
                  'titlePosition': 'out', 
                  'axisTitlesPosition': 'in',
                  'hAxis': { 'textPosition': 'in' }, 
                  'vAxis': { 'textPosition': 'in' }
          },
          'containerId': 'graph'
        });
    wrapper.draw();
    
    var wrappertable = new google.visualization.ChartWrapper({
        'chartType': 'Table',
        'dataTable': [   
            ['{$graphtitle|replace:' by ':''}','Count','%'],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count}, parseFloat('{($graphdata.Count/$graph_total_count*100)|number_format:2:".":","|string_format:"%.2f"}') ],
            {/foreach}
            ['Total', {$graph_total_count} ,100]
            ],
        'cols': [ { 'p': { style :'text-align: left;border: 1px solid green' } } , { 'p': { style :'text-align: right' } } ],
        'options': { 'title': "Open Jobs by Status{$ncbFilter|lower|capitalize} {if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}",
                  'backgroundColor': 'transparent',
                  'colour':'#1F5676',                  
                  'legend': { 'position': 'none', 'textStyle': { 'color': '#1F5676', 'fontSize': 10 } },
                  'titlePosition': 'out', 
                  'axisTitlesPosition': 'in',
                  'hAxis': { 'textPosition': 'in' }, 
                  'vAxis': { 'textPosition': 'in' },
                  'width': '100%'
          },
          'containerId': 'graphtable'
            });
    wrappertable.draw();
    
    var wrapperpie = new google.visualization.ChartWrapper({
    	'chartType': 'PieChart',
        'dataTable': [   
            ['',0],
            {foreach $graph as $graphdata}
                ["{$graphdata.Name|lower|capitalize}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
            {/foreach}    
            ],
        'options': { 'title': "Open Jobs {$graphtitle}{$ncbFilter|lower|capitalize} {if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}",
        {if isset($graphcolour) }
            colors: [  
                {foreach $graphcolour key=k item=i}
                    '#{$i}' {if not $i@last},{/if}
                {/foreach}    
            ],
        {/if} 
                  'backgroundColor': 'transparent',
                  'colour':'#1F5676',                  
                  'chartArea': { 'left':10,'top':50, 'width': '90%', 'height': '50%'},
                  'legend': { 'position': 'bottom', 'textStyle': { 'color': '#1F5676', 'fontSize': 10 }, 'alignment' : 'start' },
                  'titlePosition': 'out', 
                  'width': '100%'
          },
          'containerId': 'graphpie'
        });
    wrapperpie.draw();
    
    $(document).on('click', "#StatusPreferences", function() { 
        //It opens color box popup page.              
        $.colorbox({   
            inline:		true,
            href:		"#DivStatusPreferences",
            title:		'',
            opacity:	0.75,
            height:		940,
            width:		720,
            overlayClose:	false,
            escKey:		false,
            onLoad: function() {
                //$('#cboxClose').remove();
            },
            onClosed: function() {
                //location.href = "#EQ7";
            },
            onComplete: function() {
                $.colorbox.resize();
            }
        }); 
    });
    
}

// PCCS Date table for matching jobs - select function
function openJob($rowData){

    if($rowData[0])
    document.location.href = "{$_subdomain}/index/jobupdate/"+urlencode($rowData[0]); 

}

$(document).ready(function(){
    $(document).on('click', "#gauge_insert_save_btn", function() { 
        $("#gauge_insert_save_btn").hide();
        $("#gauge_cancel_btn").hide();
        $("#gauge_processDisplayText").show();

        $.post("{$_subdomain}/Data/updateUserGaugePreferences",        

                $("#GaugePreferencesForm").serialize(),      
                function(data){

                    //var p = eval("(" + data + ")");

                     document.location.href = "{$_subdomain}/Performance/JobsGauges/OpenTAT";


                }); //Post ends here...

     return false;

    }); 
    
    $(document).on('click', "#gauge_cancel_btn", function() { 
        $("#gauge_save_btn").hide();
        $("#gauge_cancel_btn").hide();
        $("#processDisplayText").show();
        $("#processDisplayText").html('Cancelling');
    document.location.href =  "{$_subdomain}/Performance/JobsGauges/OpenTAT";
    });  
    
    drawPanels();
	
	// Instantiate our gauge object.
    var gauge = new pccs.Gauge('gauge');
    var gauge1 = new pccs.Gauge('gauge1');
    var gauge2 = new pccs.Gauge('gauge2');
    var gauge3 = new pccs.Gauge('gauge3');
    var gauge4 = new pccs.Gauge('gauge4');
    var gauge5 = new pccs.Gauge('gauge5');
        
    // set up gauge options
    var gauge_options = { scale_start: {$gauge0.min},
                           scale_inc: {$gauge0.inc},
                           scale_divisions: {$gauge0.div},
                           red_start: {$gauge0.red},
                           red_end: {$gauge0.inc*$gauge1.div},
                           amber_start: {$gauge0.yellow},
                           amber_end: {$gauge0.red},
                          show_minor_ticks: false };
    var gauge1_options = { scale_start: {$gauge1.min},
                           scale_inc: {$gauge1.inc},
                           scale_divisions: {$gauge1.div},
                           red_start: {$gauge1.red},
                           red_end: {$gauge1.inc*$gauge1.div},
                           amber_start: {$gauge1.yellow},
                           amber_end: {$gauge1.red},
                           show_minor_ticks: false };                  
    var gauge2_options = { scale_start: {$gauge2.min},
                           scale_inc: {$gauge2.inc},
                           scale_divisions: {$gauge2.div},
                           red_start: {$gauge2.red},
                           red_end: {$gauge2.inc*$gauge2.div},
                           amber_start: {$gauge2.yellow},
                           amber_end: {$gauge2.red},
                           show_minor_ticks: false };                          
    var gauge3_options = { scale_start: {$gauge3.min},
                           scale_inc: {$gauge3.inc},
                           scale_divisions: {$gauge3.div},
                           red_start: {$gauge3.red},
                           red_end: {$gauge3.inc*$gauge3.div},
                           amber_start: {$gauge3.yellow},
                           amber_end: {$gauge3.red},
                               show_minor_ticks: false };   
    var gauge4_options = { scale_start: {$gauge4.min},
                           scale_inc: {$gauge4.inc},
                           scale_divisions: {$gauge4.div},
                           red_start: {$gauge4.red},
                           red_end: {$gauge4.inc*$gauge4.div},
                           amber_start: {$gauge4.yellow},
                           amber_end: {$gauge4.red},
                               show_minor_ticks: false }; 
    var gauge5_options = { scale_start: {$gauge5.min},
                           scale_inc: {$gauge5.inc},
                           scale_divisions: {$gauge5.div},
                           red_start: {$gauge5.red},
                           red_end: {$gauge5.inc*$gauge5.div},
                           amber_start: {$gauge5.yellow},
                           amber_end: {$gauge5.red},
                               show_minor_ticks: false };
                               
    // Draw our gauge.
    gauge.draw({$gauge0.value}, gauge_options );
    gauge1.draw({$gauge1.value}, gauge1_options );
    gauge2.draw({$gauge2.value}, gauge2_options );
    gauge3.draw({$gauge3.value}, gauge3_options );
    gauge4.draw({$gauge4.value}, gauge4_options );
    gauge5.draw({$gauge5.value}, gauge5_options );
                               
    drawVisualization();
            
    //navigation bar
    $('#myTab a').click(function (e) {
    		e.preventDefault();
    		$(this).tab('show');
    	});
    
    // scrolling plugin
    //$('#myTab').scrollspy();
    

                            $("#jncClientID").attr('disabled', '');
                            $("#jncBranchID").attr('disabled', '');

                            $(document).on('click', '#filter_btn', 
                                function() {

                                    $('#NetworkClientForm').validate({


                                                rules:  {
                                                            NetworkID:
                                                            {
                                                                    required: true

                                                            },
                                                            ClientID:
                                                            {
                                                                required: true
                                                            },
                                                            BranchID:
                                                            {
                                                                required: true
                                                            },
                                                            JobTypeID:
                                                            {
                                                                required: true
                                                            }

                                                },
                                                messages: {
                                                            NetworkID:
                                                            {
                                                                    required: "Please select Network."
                                                            },
                                                            ClientID:
                                                            {
                                                                    required: "Please select Client."
                                                            },
                                                            BranchID:
                                                            {
                                                                    required: "Please select Branch."
                                                            },
                                                            JobTypeID:
                                                            {
                                                                    required: "Please select Job Type."
                                                            }
                                                },

                                                errorPlacement: function(error, element) {

                                                        error.insertAfter( element );

                                                },
                                                errorClass: 'fieldError',
                                                onkeyup: false,
                                                onblur: false,
                                                errorElement: 'label'



                                                });




                                });





                            /* Add a change handler to the network dropdown - strats here*/
                            $(document).on('change', '#jncNetworkID', 
                                function() {

                                    $clientDropDownList  = '<option value="">Select from drop down</option>';

                                        var $NetworkID = $("#jncNetworkID").val();
                                        
                                        if( (!$NetworkID || $NetworkID=='') && ({$nid} != 0)) {
                                            $NetworkID = {$nid};
                                        }

                                        if($NetworkID && $NetworkID!='')
                                        {

                                            //Getting clients for selected network.   
                                            $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                            '',      
                                            function(data){
                                                    var $networkClients = eval("(" + data + ")");

                                                    if($networkClients)
                                                    {

                                                        for(var $i=0;$i<$networkClients.length;$i++)
                                                        {

                                                            $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                        }
                                                    }

                                                    $("#jncClientID").removeAttr('disabled');
                                                    $("#jncClientID").html('');
                                                    $("#jncClientID").html($clientDropDownList);

                                            });
                                        }    
                                        else
                                        {
                                            $("#jncClientID").attr('disabled', '');
                                        }
                                }      
                            );
                        /* Add a change handler to the network dropdown - ends here*/

                        /* Add a change handler to the client dropdown - strats here*/
                            $(document).on('change', '#jncClientID', 
                                function() {

                                    $branchDropDownList  = '<option value="">Select from drop down</option>';

                                        var $ClientID = $("#jncClientID").val();

                                        if($ClientID && $ClientID!='')
                                        {

                                            //Getting clients for selected network.   
                                            $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+$("#jncNetworkID").val(),        

                                            '',      
                                            function(data){
                                                    var $clientBranches = eval("(" + data + ")");

                                                    if($clientBranches)
                                                    {
                                                        for(var $i=0;$i<$clientBranches.length;$i++)
                                                        {

                                                            $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                        }
                                                    }

                                                    $("#jncBranchID").removeAttr('disabled');
                                                    $("#jncBranchID").html('');
                                                    $("#jncBranchID").html($branchDropDownList);

                                            });
                                        } 
                                        else
                                        {
                                            $("#jncBranchID").attr('disabled', '');
                                        }

                                }      
                            );
                        /* Add a change handler to the client dropdown - ends here*/ 
                        
                /*
                 * Matching jobs table 
                 */
                if ( {$spid} != '0' )
                {
                    var spParam = '/spid='+{$spid};
                } 
                else
                {
                    var spParam = '';
                } 
                
                if ( {$nid} != '0' )
                {
                    var nParam = '/nid='+{$nid}
                } 
                else
                {
                    var nParam = '';
                }
                
                if ( {$cid} != '0' )
                {
                    var cParam = '/cid='+{$cid};
                } 
                else
                {
                    var cParam = '';
                }
                
                if ( {$bid} != '0' )
                {
                    var bParam = '/bid='+{$bid};
                } 
                else
                {
                    var bParam = '';
                }
                                       //Open job result data table starts here...   
                $('#openJobsResults').PCCSDataTable( {

                        displayButtons: "P",
                        bServerSide: false,
                        htmlTablePageId:    'openJobsResultsPanel',
                        htmlTableId:        'openJobsResults',
                        fetchDataUrl:       '{$_subdomain}/Performance/jobsTable/tab={$tab}/openby={$openby}{if isset($daysFrom)}/daysFrom={$daysFrom}{/if}{if isset($daysTo)}/daysTo={$daysTo}{/if}'+spParam+nParam+cParam+bParam,
                        pickCallbackMethod: 'openJob',
                        dblclickCallbackMethod: 'openJob',
                        tooltipTitle: "-",
                        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                        colorboxForceClose:false,
                        aoColumns: [
			{ 'sWidth': '50px' },                                   // Skyline Job Number
                        { 'sWidth': '60px' },                                   // Booked Date
                        { 'sWidth': '30px' },                                   // Days    
                        {                                                       // Service Centre                          
                        },
                        { 'sWidth': '50px' },                                   // Servicebase Job Number
                        {                                                       // Unit Type
                            "mRender": function ( data, type, row ) {
                                if( data != null) {
                                    data = toTitleCase(data);
                                }
                                return data; 
                            },
                            'sWidth': '185px'
                        },
                        {                                                       // Manufacturer
                            'sWidth': '60px',
                            "mRender": function ( data, type, row ) {
                                if( data != null) {
                                    data = toTitleCase(data);
                                }
                                return data; 
                            }            
                        },                                   
                        {                                                       // Service Type
                            'sWidth': '40px',
                            "mRender": function ( data, type, row ) {
                                if( data != null) {
                                    data = toTitleCase(data);
                                }
                                return data; 
                            }            
                        },                                     
                        {                                                       // Status
                            'sWidth': '155px',
                            "mRender": function ( data, type, row ) {
                                if( data != null) {
                                    data = toTitleCase(data);
                                }
                                return data; 
                            } 
                        },
			{ 'bVisible':false },                                   // Item Location
                        {                                                       // Site
                            "mRender": function ( data, type, row ) {
                                if( data != null) {
                                    data = toTitleCase(data);
                                }
                                return data; 
                            } 
                        },
                        { bSortable: false }
		]

                    });

    if ({$nid} != 0) {
        $("#jncNetworkID").removeAttr('disabled');
        $('#jncNetworkID').value = {$nid};
        $('#jncNetworkID').change();
        $("#jncBranchID").removeAttr('disabled');
    }
 
    if ({$cid} != 0) {
        $("#jncClientID").removeAttr('disabled');
        $('#jncClientID').value = {$cid};
        $('#jncClientID').change();
        $("#jncBranchID").removeAttr('disabled');
    }

});

               //Open job result data table ends here... 
$(window).resize(function() {
	drawPanels();
	drawVisualization();
});

$(document).on('click', "#filter", function() {
    $.colorbox( {   inline:true,
                                            href:"#FilterForm",
                                            title: 'Filter by Type',
                                            opacity: 0.75,
                                            height:450,
                                            width:500,
                                            overlayClose: false,
                                            escKey: false
                                         });
});

$(document).on('click', "#filter_btn", function() {
    var filter;
    filter = ''; 
    
    if ($('#jncNetworkID').val() != '') {
        filter = filter + '/nid=' + $('#jncNetworkID').val();
    }
    
    if ($('#jncClientID').val() != '') {
        filter = filter + '/cid=' + $('#jncClientID').val();
    }
    
    if ($('#jncBranchID').val() != '') {
        filter = filter + '/bid=' + $('#jncBranchID').val();
    }
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage=1'+filter;    
});

$(document).on('change', "#graphdatasource", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph='+$('#graphdatasource').val()+'/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}';
});

$(document).on('change', "#listdatasource", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc='+$('#listdatasource').val()+'/guage={$guage}/showguage={$showguage}';
});

$(document).on('change', "#showguage", function() { 
    if( $('#showguage').attr('checked') ) 
        window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage=1';
    else
        window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}';
});   

$(document).on('click', "#openbysp", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby=sp{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}';
}); 

$(document).on('click', "#openbybranch", function() { 
    window.location = '{$_subdomain}/Performance/JobsGauges/{$tab}/openby=branch{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}';
});

$(document).on('click', "#GaugePreferences", function() { 
    //It opens color box popup page.              
    $.colorbox( {   inline:true,
                    href:"#DivGaugePreferences",
                    title: '',
                    opacity: 0.75,
                    height:100,
                    width:550,
                    overlayClose: false,
                    escKey: false,
                    onLoad: function() {
                       // $('#cboxClose').remove();
                    },
                    onClosed: function() {

                       //location.href = "#EQ7";
                    },
                    onComplete: function()
                    {
                        $.colorbox.resize();
                    }
                 }); 
});

$(document).on('click', "#status_insert_save_btn", function() { 
    $("#status_insert_save_btn").hide();
    $("#status_cancel_btn").hide();
    $("#processDisplayText").show();

    $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=openJobs/Type=js",        
            $("#StatusPreferencesForm").serialize(), 
            function(data) {
                //var p = eval("(" + data + ")");
                document.location.href = "{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}";
            }
    ); //Post ends here...

    return false;

});

$(document).on('click', "#status_cancel_btn", function() { 
    $("#staus_insert_save_btn").hide();
    $("#cancel_btn").hide();
    $("#processDisplayText").show();
    document.location.href = "{$_subdomain}/Performance/JobsGauges/{$tab}/openby={$openby}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage={$guage}/showguage={$showguage}";
});  

$(document).on('click', "#StatusTagAll", function() { 
    $('.StatusCheckBox').each(function() {
        $(this).attr("checked", true); 
        $(this).parent().show();
    });
    return false;
});


$(document).on('click', "#StatusClearAll", function() { 
    $("#StatusDisplaySelected").removeAttr("checked");  
    $('.StatusCheckBox').each(function () {
        $(this).removeAttr("checked"); 
        $(this).parent().show();
    });
    return false;
});


$(document).on('click', "#StatusDisplaySelected", function() { 
    if($(this).is(':checked')) {
        $('.StatusCheckBox').each(function() {
            if(!this.checked){
                $(this).parent().hide(); 
            }
        });
    } else {
        $('.StatusCheckBox').each(function() {
            $(this).parent().show(); 
        });
    } 
});


$(document).on('click', ".StatusCheckBox", function() { 
    if($(this).is(':checked')) {

    } else {
        if($("#StatusDisplaySelected").is(':checked')) {
            $(this).parent().hide(); 
        }
    } 
	});
</script>

    
{/block}

{block name=filters append}
<div class="performance">
    <h3>{$tab|replace:'TAT':" Jobs by Turnaround Time (Customer's Perception)"}{$ncbFilter|lower|capitalize}{if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if}</h3>
    <p>
    <ul id="submenu" style="margin:0 auto; text-align: center;">
        <style>
            #submenu li{
                display:inline; /* to make the list horizontal */
                padding:18px; /*some padding */
                text-align: left;  /* rest text align to left  for list items*/
            }
        </style>
        <li>
            Open Jobs :
            <input type="radio" name="openbysp" id="openbysp" value="sp" {if $openby == 'sp'}checked="checked"{/if}> By Service Provider
            &nbsp;&nbsp;
            <input type="radio" name="openbybranch" id="openbybranch" value="branch" {if $openby == 'branch'}checked="checked"{/if}> By Branch&nbsp;&nbsp;&nbsp;
        </li>
        <li>
            <input type="checkbox" id="showguage" name="showguage" {if $showguage == 1}checked="checked"{/if}> Filter By Dial&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </li>
        <li>
            <a href="#" id="filter">Organisation</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </li>
        <li>
            <a href="{$_subdomain}/Performance/JobsGauges/{$tab}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}" >Clear All Filters</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </li>
        <li>
            <a href="{$_subdomain}/Performance/jobsTableExport{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}{if isset($daysFrom)}/daysFrom={$daysFrom}{/if}{if isset($daysTo)}/daysTo={$daysTo}{/if}" >Excel Report</a>&nbsp;&nbsp;&nbsp;
        </li>
    </ul>
{/block}

{block name=PerformanceBody}
    <div class="performance" style="background: #ffffff;">
<div class="SearchPanel">
<form class="span-15 inline" style="margin-left:0;">
    <div style="padding-right: 10px;border: 1px solid #c9c9c9;">

        <div id="gauge-panel">        
            <a href="#" id="StatusPreferences" style="float:left;font-size:12px;margin-left:7px;">Status Preferences </a>
            <a href="#" id="GaugePreferences" style="float:right;font-size:12px;" >Gauge Preferences</a><br>
            <div class="clearfix"><!-- first row of gauges -->

                <div class="square">
                        <div id="gauge"></div>
                        <h4 class="center {if $guage != '7percent'}blue-letters{/if}">{if $guage != '7percent'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=7percent/showguage={$showguage}">{/if}% Jobs Under {$DialBoundaryLower} Days{if $guage != '7percent'}</a>{/if}<br />&nbsp;</h4>
                </div>

                <div class="square">
                        <div id="gauge1"></div>
                        <h4 class="center {if $guage != '714percent'}blue-letters{/if}">{if $guage != '714percent'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=714percent/showguage={$showguage}">{/if}% Jobs {$DialBoundaryLower} to {$DialBoundaryUpper} Days{if $guage != '714percent'}</a>{/if}<br />&nbsp;</h4>
                </div>

                <div class="square">
                        <div id="gauge2"></div>
                        <h4 class="center {if $guage != '14percent'}blue-letters{/if}">{if $guage != '14percent'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=14percent/showguage={$showguage}">{/if}% Jobs Over {$DialBoundaryUpper} Days{if $guage != '14percent'}</a>{/if}<br />&nbsp</h4>
                </div>
        </div>
        <div class="clearfix"><!-- second gauge row -->

                        <div class="square">
                        <div id="gauge3"></div>
                        <h4 class="center {if $guage != '7'}blue-letters{/if}">{if $guage != '7'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=7/showguage={$showguage}">{/if}Qty Jobs Under {$DialBoundaryLower} Days{if $guage != '7'}</a>{/if}<br />&nbsp;</h4>
                </div>

                        <div class="square">
                        <div id="gauge4"></div>
                        <h4 class="center{if $guage != '714'} blue-letters{/if}">{if $guage != '714'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=714/showguage={$showguage}">{/if}Qty Jobs {$DialBoundaryLower} to {$DialBoundaryUpper} Days{if $guage != '714'}</a>{/if}<br />&nbsp;</h4>
                </div>

                        <div class="square">
                        <div id="gauge5"></div>
                        <h4 class="center{if $guage != '14'} blue-letters{/if}">{if $guage != '14'}<a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/listdatasrc={$listdatasrc}/guage=14/showguage={$showguage}">{/if}Jobs Over {$DialBoundaryUpper} Days{if $guage != '14'}</a>{/if}<br />&nbsp</h4>
                </div>

                </div><!-- end of second gauge row -->      
        </div>
        <p align="center">Total Open Jobs {if $sp_name != ""} for {$sp_name|lower|capitalize|replace:'Tv ':'TV '}{/if} : {($gauge3.value + $gauge4.value + $gauge5.value)|number_format:0:".":","}
    </div>
</form>     
     
<form class=" last inline"  style="width:36%;float:right">   
    <div style="padding-right: 5px;padding-left:5px;padding-top: 5px;padding-bottom:5px; border:1px solid #c9c9c9;">     
                        

  
    <!--li class="nav-header"></li-->  
    <h3>{$sc_title}</h3>
        <p>
        By 
        <select id="listdatasource" name="listdatasource" class="colorbox">
            {foreach from=$listdataopts key=i item=opt }
                <option value="{$i}" {if $listdatasrc == $i}Selected{/if}>{$opt}</option>
            {/foreach}
        </select>
        <p>
        <table>
          <tr>
              <th>
                  <a id="sclistcat" href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/guage={$guage}/showguage={$showguage}"><script type="text/javascript">$('#sclistcat').html($('#listdatasource option:selected').text());//Quick dirty fix to dsipaly actehory title (Same as list box) should be rewtitten to something a little less byzantine</script></a>
              </th>
              {if $sc_percent == 0}
                  <th style="text-align:right">
                      <a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/scorderby=Percent/guage={$guage}/showguage={$showguage}">%</a>
                  </th>
               {/if}
               <th  style="text-align:right">
                  <a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/scorderby=Count/guage={$guage}/showguage={$showguage}">Count</a>
              </th>
              {if $sc_percent != 0}
                  <th style="text-align:right">
                      <a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/scorderby=Percent/guage={$guage}/showguage={$showguage}">%</a>
                  </th>
               {/if}
          </tr>
          {if ({$listdatasrc} == 'sp')}
            {foreach $service_centre_list as $sc}
                {if $sc_percent == 0} {$sc_value = $sc.Count} {else} {$sc_value = $sc.Percent}{/if}
                <tr style="line-height:20px;">
                    <td style="vertical-align:middle;">
                        <a href="{$_subdomain}/Performance/JobsGauges/{$tab}/spid={$sc.ServiceProviderID}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/guage={$guage}/showguage={$showguage}">{$sc.Name|cat:' '|lower|capitalize|replace:'Tv ':'TV '|truncate:36:'...'}</a>
                    </td>
                    {if $sc_percent == 0}
                        <td style="vertical-align:middle;text-align:right">
                            <span class="label">{$sc.Percent|number_format:2:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Count > $sc_colour_settings.red}label-important{elseif $sc.Count > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="padding-bottom:2px;float:right;width:40px;text-align:center;">{$sc.Count}</span></a>
                        </td>
                    {else}
                        <td style="vertical-align:middle;text-align:right">
                            <span class="label">{$sc.Count|number_format:0:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Percent > $sc_colour_settings.red}label-important{elseif $sc.Percent > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="float:right;width:40px;text-align:center;">{$sc.Percent|number_format:2:".":","}%</span></a>
                        </td>
                    {/if}
                </tr>
            {/foreach}
          {/if}{* Service Centre List *}
              
          {* Manufacturer List *}    
          {if ({$listdatasrc} == 'make')}
            {foreach $service_centre_list as $sc}
                {if $sc_percent == 0} {$sc_value = $sc.Count} {else} {$sc_value = $sc.Percent}{/if}
                <tr style="line-height:20px;">
                    <td style="vertical-align:middle;">
                        <a href="{$_subdomain}/Performance/JobsGauges/{$tab}/mid={$sc.ManufacturerID}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/guage={$guage}/showguage={$showguage}">{$sc.Name|cat:' '|lower|capitalize|truncate:36:'...'}</a>
                    </td>
                    {if $sc_percent == 0}
                        <td style="vertical-align:middle;">
                            <span class="label">{$sc.Percent|number_format:2:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Count > $sc_colour_settings.red}label-important{elseif $sc.Count > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="padding-bottom:2px;float:right;width:40px;text-align:center;">{$sc.Count}</span></a>
                        </td>
                    {else}
                        <td style="vertical-align:middle;">
                            <span class="label">{$sc.Count|number_format:0:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Percent > $sc_colour_settings.red}label-important{elseif $sc.Percent > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="float:right;width:40px;text-align:center;">{$sc.Percent|number_format:2:".":","}%</span></a>
                        </td>
                    {/if}
                </tr>
            {/foreach}
          {/if}{* Manufacturer List *}
              
          {* Network List *}    {* Removed 02/04/2013 at Joe's request *}
          {*if ({$listdatasrc} == 'network')}
            {foreach $service_centre_list as $sc}
                {if $sc_percent == 0} {$sc_value = $sc.Count} {else} {$sc_value = $sc.Percent}{/if}
                <tr style="line-height:20px;">
                    <td style="vertical-align:middle;align:right">
                        <a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}/nid={$sc.NetworkID}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/guage={$guage}/showguage={$showguage}">{$sc.Name|cat:' '|lower|capitalize|truncate:36:'...'}</a>
                    </td>
                    {if $sc_percent == 0}
                        <td style="vertical-align:middle;text-align:right">
                            <span class="label">{$sc.Percent|number_format:2:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Count > $sc_colour_settings.red}label-important{elseif $sc.Count > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="padding-bottom:2px;float:right;width:40px;text-align:center;">{$sc.Count}</span></a>
                        </td>
                    {else}
                        <td style="vertical-align:middle;text-align:right">
                            <span class="label">{$sc.Count|number_format:0:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Percent > $sc_colour_settings.red}label-important{elseif $sc.Percent > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="float:right;width:40px;text-align:center;">{$sc.Percent|number_format:2:".":","}%</span></a>
                        </td>
                    {/if}
                </tr>
            {/foreach}
          {/if*}{* Network List *}
              
          {* Client List *}    
          {if ({$listdatasrc} == 'client')}
            {foreach $service_centre_list as $sc}
                {if $sc_percent == 0} {$sc_value = $sc.Count} {else} {$sc_value = $sc.Percent}{/if}
                <tr style="line-height:20px;">
                    <td style="vertical-align:middle;">
                        <a href="{$_subdomain}/Performance/JobsGauges/{$tab}{if $spid != 0}/spid={$spid}{/if}{if $nid != 0}/nid={$nid}{/if}{if $cid != 0}/cid={$cid}{/if}{if $bid != 0}/bid={$bid}{/if}/graph={$graphdatasrc}/guage={$guage}/showguage={$showguage}">{$sc.Name|cat:' '|lower|capitalize|truncate:36:'...'}</a>
                    </td>
                    {if $sc_percent == 0}
                        <td style="vertical-align:middle;text-align:right">
                            <span class="label">{$sc.Percent|number_format:2:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Count > $sc_colour_settings.red}label-important{elseif $sc.Count > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="padding-bottom:2px;float:right;width:40px;text-align:center;">{$sc.Count}</span></a>
                        </td>
                    {else}
                        <td style="vertical-align:middle;align:right">
                            <span class="label">{$sc.Count|number_format:0:".":","}</span>
                        </td>
                        <td style="padding-top:1px;padding-bottom:1px;vertical-align:middle;text-align:right">
                            <span class="label {if $sc.Percent > $sc_colour_settings.red}label-important{elseif $sc.Percent > $sc_colour_settings.yellow}label-warning{else}label-success{/if}" style="float:right;width:40px;text-align:center;">{$sc.Percent|number_format:2:".":","}%</span></a>
                        </td>
                    {/if}
                </tr>
            {/foreach}
          {/if}{* Client List *}
        </table>

    </ul> 
</div>              
</form>
        
<!-- Graph Data by box -->        
<form  class="span-15 inline" style="margin-left:0">
    <div style="background:#ffffff; border: 1px solid #c9c9c9;">
        <p style="margin-top:10px;text-align:center;">
        Display graph data by 
        <select id="graphdatasource" name="graphdatasource" class="colorbox">
            {foreach from=$graphdataopts key=i item=opt }
                <option value="{$i}" {if $graphdatasrc == $i}Selected{/if}>{$opt}</option>
            {/foreach}
        </select>&nbsp;&nbsp;
    </div>
</form>
        
                            
<!-- Column chart -->        
<form  class="span-15 inline" style="margin-left:0">
    <div style="background:#ffffff; border: 1px solid #c9c9c9;">
        <span style="float:right"><!-- a href="#" class="colorbox" id="graphfullscreen">Enlarge</a -->&nbsp;&nbsp;</span>
        <div id="graph"></div>
        {if $showguage == 1}<p align="center">Total {$sc_title|replace:'% ':''|replace:'Qty ':''} : {$graph_total_count}{/if}
    </div>
</form>



<p>

<div style="display:block">
    <!-- Table of data -->
    <form class="span-7 inline"  style="margin-left:0;float:left">
        <div style="background:#ffffff; border:none;">
            <p>
            <div id="graphtable" ></div>   
            <p>
        </div>
    </form>

    <!-- Pie Chart -->
    <form class="span-7 inline" style="float:right;margin-left:0;"  ><!--style="margin-left:0;float:right;"-->
        <div  style="background:#ffffff; border: 1px solid #c9c9c9;">
            <p>
            <div id="graphpie" style="width:100%;height:500px;"></div><!---->

            {if $showguage == 1}<p align="center">Total {$sc_title|replace:'% ':''|replace:'Qty ':''} : {/if}
        </div>
    </form>            
</div>

                




             
        <p>


</div>                                   
</div>

<div class="openJobsResultsPanel">

            <table id="openJobsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                <thead>
                        <tr>
                                <th title="Skyline job number" >SL No.</th>
                                <th title="Job booked date" >Booked</th>
                                <th title="Job booked date" >Days</th>
                                <th title="Service Centre"  >Service Centre</th>
                                <th title="Service Centre job number" >Job No.</th>
                                <th title="Unit Type" >Unit Type</th>
                                <th title="Manufacturer" >Manufacturer</th>
                                <th title="Service Type" >Service Type</th>
                                <th title="System Status" >Status</th>
                                <th title="Item Location" >Item Location</th>
                                <th title="Field Service" >Site</th>
                                <th></th>
                        </tr>
                </thead>
                <tbody>

                </tbody>
            </table>  

       </div>
</div>

<!-- Hidden divs (dialogues) -->

<div style="display:none;" >        
    <div id="biggraph"  class="SystemAdminFormPanel">
        <div id="enlargegraph"></div>
    </div>
</div>
              
<div style="display:none;">
    <div id="FilterForm" class="SystemAdminFormPanel">

        <form id="NetworkClientForm" name="NetworkClientForm" method="post" >
            <fieldset>
                <legend title="">Filter by :-</legend>


                {if $showNetworkDropDown}

                    <p>
                        <label for="NetworkID" >Network:<sup>*</sup></label>
                        &nbsp;&nbsp;  <select name="NetworkID" id="jncNetworkID" class="text" >

                                <option value="" {if $nid == 0}selected="selected"{/if}>Select from drop down</option>

                            {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nid == $network.NetworkID}selected="selected"{/if} >{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                    </p>
                {else}
                    <input type="hidden" name="NetworkID" id="jncNetworkID"  value="{$NetworkID|escape:'html'}" >
                {/if}    




                {if $ClientID}
                    <input type="hidden" name="ClientID" id="jncClientID"  value="{$ClientID|escape:'html'}" >
                {else}
                <p>
                    <label for="ClientID" >Client:<sup>*</sup></label>
                    &nbsp;&nbsp;  <select name="ClientID" id="jncClientID" class="text" >

                                    <option value="" {if $cid == 0}selected="selected"{/if}>Select from drop down</option>

                                    {foreach $clients as $client}

                                        <option value="{$client.ClientID}" {if $cid == $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                    {/foreach}
                                    </select>
                </p>
                {/if}



                <p>
                    <label for="BranchID" >Branch:<sup>*</sup></label>
                    &nbsp;&nbsp;  <select name="BranchID" id="jncBranchID" class="text" >

                                    <option value="" {if $bid == 0}selected="selected"{/if}>Select from drop down</option>

                                    {foreach $branches as $branch}

                                        <option value="{$branch.BranchID}" {if $bid == $branch.BranchID}selected="selected"{/if}>{$branch.BranchName|escape:'html'}</option>

                                    {/foreach}
                                    </select>
                </p>

                <p>
                    <label  >&nbsp;</label>

                    <input type="button" name="filter_btn" id="filter_btn" class="textSubmitButton"  value="Go" >


                </p>
            </fieldset>
        </form>
    </div>
</div> 
                                    
<div style="display:none;">
    <div id="DivGaugePreferences" class="SystemAdminFormPanel" >
        <form id="GaugePreferencesForm" name="GaugePreferencesForm" method="post"  action="#" class="inline">
            <style> 
                input{ width:30px; vertical-align: middle }
                th { text-align:center; } 
                td.r { text-align:right; width: 115px; } 
                td.c { text-align:center; vertical-align: middle  }  
                td.r span.label-success, td.r span.label-important { display:block; width: 40px; text-align:center; float:left; vertical-align: middle; margin-top: 5px; margin-left:5px;}
            </style>
            <fieldset>
                <legend title="" >Gauge Settings</legend>
                <p>
                <table>
                    <tr>
                        <th>
                            Section
                        </th>
                        <th>
                            %
                        </th>
                        <th>
                            Qty
                        </th>
                    </tr>
                    <tr>
                        <td class="c">
                            Under <input type="text" id="GaDialBoundaryLower" name="GaDialBoundaryLower" value="{$gaugePrefs.GaDialBoundaryLower}" onchange="$('#lower').text($(this).val())"> Days
                        </td>
                        <td class="r">
                            <span><span class="label label-success">Green</span> &lt; <input type="text" id="GaDial0Yellow" name="GaDial0Yellow" value="{$gauge0.yellow}"></span></span>
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial0Red" name="GaDial0Red" value="{$gauge0.red}">
                        </td>
                        <td class="r">
                            <span class="label label-success">Green</span> &lt; <input type="text" id="GaDial3Yellow" name="GaDial3Yellow" value="{$gauge3.yellow}">
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial3Red" name="GaDial3Red" value="{$gauge3.red}">
                            <br>
                            Maximum <input type="text" id="GaDial3Max" name="GaDial3Max" value="{$gaugePrefs.GaDial3Max}">
                        </td>
                    </tr>
                    <tr>
                        <td  class="c">
                            Between <span id='lower'>{$gaugePrefs.GaDialBoundaryLower}</span> and <span id='upper'>{$gaugePrefs.GaDialBoundaryUpper}</span> Days
                        </td>
                        <td class="r">
                            <span class="label label-success">Green</span> &lt; <input type="text" id="GaDial1Yellow" name="GaDial1Yellow" value="{$gauge1.yellow}">
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial1Red" name="GaDial1Red" value="{$gauge1.red}">
                        </td>
                        <td class="r">
                            <span class="label label-success">Green</span> &lt; <input type="text" id="GaDial4Yellow" name="GaDial4Yellow" value="{$gauge4.yellow}">
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial4Red" name="GaDial4Red" value="{$gauge4.red}">
                            <br>
                            Maximum <input type="text" id="GaDial4Max" name="GaDial4Max" value="{$gaugePrefs.GaDial4Max}">
                        </td>
                    </tr>
                    <tr>
                        <td  class="c">
                            Over <input type="text" id="GaDialBoundaryUpper" name="GaDialBoundaryUpper" value="{$gaugePrefs.GaDialBoundaryUpper}" onchange="$('#upper').text($(this).val())"> Days
                        </td>
                        <td class="r">
                            <span class="label label-success">Green</span> &lt; <input type="text" id="GaDial2Yellow" name="GaDial2Yellow" value="{$gauge2.yellow}">
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial2Red" name="GaDial2Red" value="{$gauge2.red}">
                        </td>
                        <td class="r">
                            <span class="label label-success">Green</span> &lt; <input type="text" id="GaDial5Yellow" name="GaDial5Yellow" value="{$gauge5.yellow}">
                            <br>
                            <span class="label label-important">Red</span> &gt;<input type="text" id="GaDial5Red" name="GaDial5Red" value="{$gauge5.red}">
                            <br>
                            Maximum <input type="text" id="GaDial5Max" name="GaDial5Max" value="{$gaugePrefs.GaDial5Max}">
                        </td>
                    </tr>
                </table>
                <br><br>
                <span class= "bottomButtons" >
                    <input type="submit" name="gauge_insert_save_btn" class="textSubmitButton" id="gauge_insert_save_btn"  value="Save" >
                    &nbsp;
                    <input type="submit" name="gauge_cancel_btn" class="textSubmitButton" id="gauge_cancel_btn" onclick="return false;"  value="Cancel" >
                    <span id="gauge_processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;Processing record</span>    
                </span>
                </p>
            </fieldset>    
        </form>        
    </div>
                
    <div style="display:none;">

         <div id="DivStatusPreferences" class="SystemAdminFormPanel">
    
	    <form id="StatusPreferencesForm" name="StatusPreferencesForm" method="post" action="#" class="inline">

                <fieldset>
		    
                    <legend title="">Status Preferences</legend>
		    
                    <p><label id="suggestText"></label></p>
		    
		    <p style="text-align:right;">
			<a id="StatusTagAll" href="#" style="text-decoration:underline">
			    Tag All
			</a>
			&nbsp;&nbsp;
			<a id="StatusClearAll" href="#" style="text-decoration:underline">
			    Clear All
			</a>
			&nbsp;&nbsp;
			<input type="checkbox" id="StatusDisplaySelected" name="StatusDisplaySelected" value="1" />
			&nbsp;&nbsp;
			Display Selected
			&nbsp;&nbsp;
			<br/><br/>
		    </p>

		    <div style="height:500px;overflow-y:scroll; padding:0px; margin:0px;">
			<p>     
			    {foreach $jobStatusList as $js}
				<span>
				    <input type="checkbox" style="width:30px;" class="text StatusCheckBox" name="StatusID[]" value="{$js.StatusID|escape:'html'}" {if $js.Exists eq true}checked="checked"{/if}>
				    &nbsp;&nbsp;
				    {$js.StatusName|escape:'html'}
				<br>
				</span>
			    {/foreach} 
			</p>
		    </div>    

		    <p>
			<br/><br/>
			<span class="bottomButtons">
			    <input type="submit" name="status_insert_save_btn" class="btnStandard" id="status_insert_save_btn" value="Save" />
			    &nbsp;
			    <input type="submit" name="status_cancel_btn" class="btnCancel" id="status_cancel_btn" onclick="return false;" value="Cancel" />
			    <span id="processDisplayText" style="color:red;display:none;">
				<img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" />
				&nbsp;&nbsp;
				Process Record
			    </span>    
			</span>
		    </p>

		</fieldset>    

	    </form>        

	</div>
                
</div> 
                                    
                                    
                                    
{/block}
