{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Job Update"}
    {$PageId = $HomePage}
{/block}


{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}


{block name=scripts}


<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>

<link rel="stylesheet" href="{$_subdomain}/css/tooltipster/tooltipster.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="{$_subdomain}/js/jquery.tooltipster.min.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/autoresize.jquery.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.autosize-min.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>

<script type="text/javascript">   
    
    var oContactHistoryTable;
    var oAppointmentHistoryTable;
    var oStatusTable;
    var partsTable;
    var engineerHistoryTable;
    var height = 0;
    var auditTrailTable;
    var raHistoryTable;
    
    var CustomerLocation='{$FullAddress|escape:'quotes'}';
    var CollectionLocation='{if $deliveryAddress["companyName"] != ""}{$deliveryAddress["companyName"]|escape:'quotes'},{/if}{if $deliveryAddress["buildingNameNumber"] != ""}{$deliveryAddress["buildingNameNumber"]|escape:'quotes'} {/if}{if $deliveryAddress["street"] != ""}{$deliveryAddress["street"]|escape:'quotes'},{/if}{if $deliveryAddress["localArea"] != ""}{$deliveryAddress["localArea"]|escape:'quotes'},{/if}{if $deliveryAddress["townCity"] != ""}{$deliveryAddress["townCity"]|escape:'quotes'},{/if}{if $deliveryAddress["postCode"] != ""}{$deliveryAddress["postCode"]|escape:'quotes'}{/if}';
    
    jQuery.validator.addMethod("NotEqualLabel", function(value, element, param) {
        return value != $(element).attr('title');
    }, "This field is required.");
    
    function setEqualHeight(columns)  {  
        var tallestcolumn = 0;  
        columns.each(  
            function()  {  
                currentHeight = $(this).height(); 
                if(currentHeight > tallestcolumn)  {  
                    tallestcolumn  = currentHeight;  
                }  
            }  
        );  
        columns.height(tallestcolumn); 
        return tallestcolumn;
    } 
    
    function resizeColumns() {
    
        $('#ProductDetailsPanel').height('auto');
        $('#ProductDetailsFormPanel').height('auto');
        $('#AdditionalPanels').height('auto');
        $('#AdditionalNotesFormPanel').height('auto');
        $('#IssueReportPanels').height('auto');
        $('#IssueReportFormPanel').height('auto');
        $('#AppointmentsPanel').height('auto');
        $('#InvoiceCostsPanel').height('auto');
        $('#ServiceReportPanel').height('auto');
            
        
        var $BookedByFormPanelHeight = $('#BookedByFormPanel').height();
        
        var colSubheight =   setEqualHeight($('#AdditionalInformationFormPanel, #BookedByFormPanel'));
        
        
        
        if($BookedByFormPanelHeight<colSubheight)
        {
                $('#BookedByFormPanel').height(colSubheight-30);
        }
            
        var colheight = setEqualHeight($('#ProductDetailsPanel, #AdditionalPanels, #IssueReportPanels'));
        
        $('#ProductDetailsFormPanel').height(colheight-57);
        $('#IssueReportFormPanel').height(colheight-$('#BookedBy').height()-80);
        $('#AdditionalNotesFormPanel').height(colheight-$('#AdditionalInformation').height()-80);
        
        //setEqualHeight($('#AppointmentsPanel, #InvoiceCostsPanel, #ServiceReportPanel'));
        setEqualHeight($('#AppointmentsPanel, #InvoiceCostsPanel'));
    }

    //jQuery.fn.log = function (msg) {
    //    console.log(msg);
    //    return this;
    //};


    function gotoAPTEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }
    
    
     /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveAPTRow(nRow, aData)
    {
        
        
          
        if (aData[6]=='Yes')
        {  
            $(nRow).addClass("inactive");

        }
        else
        {
            $(nRow).addClass("");
        }
        
    }
    
    
    


    function addFields(selected) {
     
        var selected_split = selected.split(","); 	

        $("#jbBuildingName").val(selected_split[0]).blur();	
        $("#jbStreet").val(selected_split[1]).blur();
        $("#jbArea").val(selected_split[2]).blur();
        $("#jbCity").val(selected_split[3]).blur();
        $("#selectOutput").slideUp("slow"); 

    }
    
    
    function mytrim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
    }


    $(document).ready(function() {  
	
	if ("{$pendingRA}" != "") {
	    setTimeout(function() { 
		modal("This job has an outstanding Authorisation Request. Before proceeding with any job updates this Request must be resolved.");
	    }, 2000);
	}
	
	$("input[name=ConsignmentDate]").datepicker({ dateFormat: "yy-mm-dd" });
	
	
	$("option").each(function() {
	    var $s = mytrim($(this).text());
	    $(this).text($s);
	});
	
	
	{*
	if count($AuthorisationTypes) > 1}
	    $(document).on("click", "input[name=ATType]", function() {
		$("#RAStatusElement, #RAHistoryNotesDiv").show();
	    });
	{/if
	*}
	
	/*ACCESSORIES*******************************************************************/
        
	var textCounter = {  
	    maxCharacterSize:	200,  
	    originalStyle:	"accessoriesTextcount",  
	    warningStyle:	"warningDisplayInfo",  
	    warningNumber:	10,  
	    displayFormat:	"#input Characters | #left Characters Left | #words Words"
	};
	$("#accessoriesTextarea").textareaCount(textCounter);
	
	$(".accessoriesTextcount").hide();
	
	$(document).on("click", "#accessoriesEdit", function() {
	    $("#accessoriesTextarea").val($("#accessoriesP").text());
	    $(".accessoriesTextcount").show();
	    $("#accessoriesP").hide();
	    $("#accessoriesTextarea").show();
	    $("#accessoriesEdit").hide();
	    $("#accessoriesSave").show();
	    $("#accessoriesCancel").show();
	    return false;
	});
	
	$(document).on("click", "#accessoriesCancel", function() {
	    $(".accessoriesTextcount").hide();
	    $("#accessoriesP").show();
	    $("#accessoriesTextarea").hide();
	    $("#accessoriesEdit").show();
	    $("#accessoriesSave").hide();
	    $("#accessoriesCancel").hide();
	    return false;
	});
	
	$(document).on("click", "#accessoriesSave", function() {
	    $.post("{$_subdomain}/Job/editAccessories", { text: $("#accessoriesTextarea").val(), jobID: {$JobID} }, function(response) {
		$("#accessoriesP").text($("#accessoriesTextarea").val().toUpperCase());
		$(".accessoriesTextcount").hide();
		$("#accessoriesP").show();
		$("#accessoriesTextarea").hide();
		$("#accessoriesEdit").show();
		$("#accessoriesSave").hide();
		$("#accessoriesCancel").hide();
		return false;
	    });
	    return false;
	});
	
	
	/*CONDITION*******************************************************************/
        
	var textCounter = {  
	    maxCharacterSize:	150,  
	    originalStyle:	"conditionTextcount",  
	    warningStyle:	"warningDisplayInfo",  
	    warningNumber:	10,  
	    displayFormat:	"#input Characters | #left Characters Left | #words Words"
	};
	$("#conditionTextarea").textareaCount(textCounter);

	$(".conditionTextcount").hide();
	    
	$(document).on("click", "#conditionEdit", function() {
	    $("#conditionTextarea").val($("#conditionP").text());
	    $(".conditionTextcount").show();
	    $("#conditionP").hide();
	    $("#conditionTextarea").show();
	    $("#conditionEdit").hide();
	    $("#conditionSave").show();
	    $("#conditionCancel").show();
	    return false;
	});
    
	$(document).on("click", "#conditionCancel", function() {
	    $(".conditionTextcount").hide();
	    $("#conditionP").show();
	    $("#conditionTextarea").hide();
	    $("#conditionEdit").show();
	    $("#conditionSave").hide();
	    $("#conditionCancel").hide();
	    return false;
	});

	$(document).on("click", "#conditionSave", function() {
	    $.post("{$_subdomain}/Job/editCondition", { text: $("#conditionTextarea").val(), jobID: {$JobID} }, function(response) {
		$("#conditionP").text($("#conditionTextarea").val().toUpperCase());
		$(".conditionTextcount").hide();
		$("#conditionP").show();
		$("#conditionTextarea").hide();
		$("#conditionEdit").show();
		$("#conditionSave").hide();
		$("#conditionCancel").hide();
		return false;
	    });
	    return false;
	});
	
	/**************************************************************************************/
        	
        $('.combobox').combobox({
	    change: function() {
		if($("#RAStatusID option:selected").attr("statusCode") == 3) {
		    $("#authNoTr").show();
		    $("#raNotesTr").hide();
		} else {
		    $("#authNoTr").hide();
		    $("#raNotesTr").show();
		}
	    }
	});
 
 
        {if $AP7035}

	    raHistoryTable = $("#RAHistory").dataTable({
		bAutoWidth:	    false,
		aoColumns: [
                    { bVisible: false },
                    { sWidth: "80px" },
                    { sWidth: "46px" },
                    { sWidth: "131px" },
                    { sWidth: "150px" },
                    { sWidth: "130px" },
                    null,
		    { sWidth: "50px" }
		],
		bDestroy:           true,
		bServerSide:        false,
		bProcessing:        false,
		htmlTableId:        "RAHistory",
		sDom:               "ft<'#dataTables_child'>rpli",
		sPaginationType:    "full_numbers",
		bPaginate:          true,
		bSearch:            false,
		iDisplayLength:     10,
		sAjaxSource:        "{$_subdomain}/Data/RAhistory/{$JobID}/",
		aaSorting:	    [[1, "desc"], [2, "desc"]],
		oLanguage: {
		    sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
		},
		fnInitComplete: function() {
		    var html = '<a href="#" class="delSearch" style="top:14px; right:4px; position:absolute;">\
				    <img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
				</a>';
		    $(html).insertAfter("#RAHistoryNotesDiv .dataTables_filter input");
		    $(document).on("click", "#RAHistoryNotesDiv .delSearch", function() {
			$("#RAHistoryNotesDiv .dataTables_filter input").val("");
			raHistoryTable.fnFilter("");
			return false;
		    });
		},
		fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		    if (aData[7]) {
			var html = "<a href='#' class='showRAImage' img='" + aData[7] + "'><img src='{$_subdomain}/images/remote_engineer_history_icon.png' /></a>";
			$($(nRow).find("td").toArray()[6]).css("text-align", "center");
			$($(nRow).find("td").toArray()[6]).html(html);
		    }
		}
	    });
	    
	    
	    $(document).on("click", ".showRAImage", function() {
		var html = "<img id='cboxImg' src='{$_subdomain}/images/ra/" + $(this).attr("img") + "' />";
		$.colorbox({ 
		    opacity:    0.75,
		    html:	html,
		    onComplete: function() {
			$.colorbox.resize({ innerWidth: $("#cboxImg").width() });
		    }
		});
		$.colorbox.resize();
		return false;
	    });
	    
	    
	    $(document).on("click", "input[name=raTypeFilter]", function() {
		var source = "{$_subdomain}/Data/RAhistory/{$JobID}/";
		if($(this).val() == "current") {
		    source += "?type=" + $("input[name=ATType]:checked").val();
		}
		raHistoryTable.fnReloadAjax(source);
		return false;
	    });
	    
	    
	    //Click handler for Save New Status button.
	    $(document).on("click", "#SaveNewStatus", function() { 

		if($("#RAStatusID option:selected").val() == "") {
		    modal("You have to select new RA status first.");
		    return false;
		}

		if($("#RANotes").val() == "" && $("#RAStatusID option:selected").attr("statusCode") != 3) {
		
		    modal("Please enter notes.");  
		    $("#RANotes").focus();
		    return false;
		    
		} else {

		    if($("#RAStatusID option:selected").attr("statusCode") == 3 && $("#authNo").val() == "") {
			alert("Please enter Authorisation No.");  
			$("#authNo").focus();
			return false;
		    }


		    var options = {
			url:	"{$_subdomain}/Job/updateRAJobType/{$JobID}",
			success: function(responseText, statusText, xhr, $form) {
			    document.location.href = "{$_subdomain}/index/jobupdate/{$JobID}/{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";
			}
		    };
		    $("#raJobsForm").ajaxSubmit(options);


		    /*
		    $.post("{$_subdomain}/Job/updateRAJobType/{$JobID}", $("#raJobsForm").serialize(), function(data) {
                        
                        document.location.href = "{$_subdomain}/index/jobupdate/{$JobID}/{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";

			var p = eval("(" + data + ")");

                        if(p == "OK") {
			    {*document.location.href = "{$_subdomain}/index/jobupdate/{$JobID}/#EQ7{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";*}
			    {*document.location.href = "{$_subdomain}/index/jobupdate/{$JobID}/{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";*}
                        } else {
			    $("#centerInfoText").html("Sorry there is a problem, your changes have not been done.").css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  
			}
			
		    });
		    */
		 
		} 
		
		return false;
		
	    });
        
	
	    function modal(text) {
		$("#modP").remove();
		var html = "<p id='modP'>" + text + "</p>";
		$(html).appendTo("body");
		$("#modP").dialog({
		    resizable:  false,
		    draggable:  false,
		    height:	"auto",
		    modal:	true,
		    title:	"Alert",
		    buttons: {
			Close: {
			    click:	function() {
					$(this).dialog("destroy"); 
				    },
			    "class":	"btnCancel",
			    text:	"Close"
			}
		    }
		});	
	    }
	
        
	    //Click handler for RA Type checkboxes.
	    $(document).on("click", "[name='ATType']", function() { 

		document.body.style.cursor = "wait";

		var the = this;
		var allowed = false;
	    
		$.ajax({
		    type:   "POST",
		    url:    "{$_subdomain}/Job/getAuthTypeAllowed",
		    data:   { type: $(the).val(), jobID: {$JobID} },
		    async:  false
		}).done(function(response) {
	    
		    document.body.style.cursor = "default";
		    
		    //check response if user is permitted to change auth type
		    if(!response || response == 0 || response == "0") {
			modal("This record has an active RA Status that is not resolved. You must resolve that RA Status before you can assign another RA Group.");
			allowed = false;
			return false;
		    } else {
			allowed = true;
		    }
		    
		    //load RA status dropdown for current auth type
		    document.body.style.cursor = "wait";
		    $.ajax({
			type:   "POST",
			url:    "{$_subdomain}/JobUpdate/getRAStatuses",
			data:   { type: $(the).val(), jobID: {$JobID} },
			async:  false
		    }).done(function(response) {
			var data = JSON.parse(response);
			var html = "<option value=''></option>";
			for(var i = 0; i < data.statuses.length; i++) {
			    html += "<option value='" + data.statuses[i]["RAStatusID"] + "'>" + data.statuses[i]["RAStatusName"] + "</option>"
			}
			$("#RAStatusID").html(html);
			if(data.status) {
			    $("#currentRAStatus").show();
			    $("#JobStatusText").text(data.status.RAStatusName);
			} else {
			    $("#currentRAStatus").hide();
			}
			document.body.style.cursor = "default";
		    });
		    
		    
		    if(!$("input[name=raTypeFilter]").is(":checked")) {
			$("input[name=raTypeFilter][value=all]").attr("checked", true);
		    }

		    if($(the).is(":checked")) {
			$sltValue = $(the).val();
		    } else {
			$sltValue = 0;
		    } 

		    if($sltValue) {

			$alreadySeletecdFlag = false;

			$("[name='ATType']").each(function() {
			    if($(this).is(':checked')) {
				$sltValue1 = $(this).val();
			    } else {
				$sltValue1 = 0;
			    } 
			    if($sltValue1 && $sltValue1!=$sltValue) {
				$alreadySeletecdFlag = true;
			    }
			});


			if($alreadySeletecdFlag) {    

			    $(the).removeAttr("checked");

			    $.colorbox({   
				inline:		true,
				href:		"#MultipleRATypeSelection",
				title:		"",
				opacity:	0.75,
				height:		240,
				width:		700,
				overlayClose:   false,
				escKey:		false,
				onLoad: function() {
				    $('#cboxClose').remove();
				},
				onClosed: function() {
				    location.href = "#EQ7{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";
				}
			    }); 

			} else if($(the).val() == "on hold") {

			    $.colorbox({   
				inline:	    true,
				href:	    "#ApplyNewRATypeConfirmation",
				title:	    '',
				opacity:	    0.75,
				height:	    240,
				width:	    700,
				overlayClose:   false,
				escKey:	    false,
				onLoad: function() {
				    $('#cboxClose').remove();
				},
				onClosed: function() {
				    //Setting existing latest status id for selected RA Type from history table. 
				    setRAStatusID($sltValue);
				    location.href = "#EQ7{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";
				}
			    }); 
			}

		    } else {

			$.colorbox({   
			    inline:		true,
			    href:		"#RATypeNoSelection",
			    title:		'',
			    opacity:	0.75,
			    height:		220,
			    width:		700,
			    overlayClose:	false,
			    escKey:		false,
			    onLoad: function() {
				$('#cboxClose').remove();
			    },
			    onClosed: function() {
				location.href = "#EQ7{if isset($ref)}?ref={$ref}{/if}{if isset($params)}&params={$params}{/if}";
			    }
			}); 

			$(the).attr("checked", "checked");        

		    }

		    $("#RAStatusElement, #RAHistoryNotesDiv").show();

		    if($("input[name=raTypeFilter]:checked").val() == "current") {
			var source = "{$_subdomain}/Data/RAhistory/{$JobID}/";
			source += "?type=" + $(the).val();
			raHistoryTable.fnReloadAjax(source);
		    }
		    
		});
		
		if(allowed) {
		    return true;
		} else {
		    return false;
		}

            });            



            //Click handler for RAType No Selection Yes button.
	    $(document).on("click", "#RATypeNoSelectionYes", function() { 
		$.colorbox.close();
		$("#NumRequestAuthorisation").html("0");
		$("[name='ATType[]']").each(function() {
		   $(this).removeAttr("checked");    
		});
	    });
             
             
	    //Click handler for RAType No Selection No button.
            $(document).on("click", "#RATypeNoSelectionNo", function() { 
                 $.colorbox.close();
	    });
        
        
            //Click handler for Multiple RA Type Selection close button.
            $(document).on("click", "#MultipleRATypeSelectionClose", function() { 
                 $.colorbox.close();
	    });
        
        
            //Click handler for RA Type Confirm Continue button.
            $(document).on("click", "#RATConfirmContinue", function() { 
                 $.colorbox.close();
                 $("#NumRequestAuthorisation").html("1");
	    });
        
        
            //Click handler for RA Type Cancel Selection button.
            $(document).on("click", "#RATCancelSelection", function() { 
		$.colorbox.close();
		$( "[name='ATType[]']" ).each(function(){
		   $(this).removeAttr("checked");
		});
                $("#NumRequestAuthorisation").html("0");
	    });
             
            
	    function setRAStatusID($sltValue) {
		if($sltValue) {
                    $.get(
			"{$_subdomain}/Data/getLatestRAHisotryStatusID/{$JobID}/" + urlencode($sltValue),
			'',      
			function(data) {
			    var p = eval("(" + data + ")");
			    if(p) {
				$("#RAStatusID").val(p);
				setValue("#RAStatusID");
			    } else {
				$("#RAStatusID").val("{$RAStatusTypesList.0.RAStatusID|default: 0}");
				setValue("#RAStatusID");
			    }

			}
		    );
		}
	    }
        
        {/if}

    
        {if $AP11005}
            var $displayButtonsApp = 'AUD';
        {else}
            var $displayButtonsApp = '';
        {/if}    
        
        $('#AppointmentsDT').PCCSDataTable( {
	    aoColumns: [
                {  bVisible:    false },
		null,
		null,
		null,
		null,
                null,
                {  bVisible:    false }
	    ],
            displayButtons:  $displayButtonsApp,
            sDom:               't<"#dataTables_command_dt">rp',    
            bottomButtonsDivId:	'dataTables_command_dt',
            htmlTableId:        'AppointmentsDT',
            fetchDataUrl:       '{$_subdomain}/Data/appointments/{$JobID}/',
	    searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
            hidePaginationNorows:  true,
            iDisplayLength:  8,
            aaSorting:    [[ 0, "asc" ], [ 1, "asc" ]],
            addButtonId:     'addButtonId',
            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
            createFormTitle: '{$page['Text']['insert_app_page_legend']|escape:'html'}',
            createAppUrl:    '{$_subdomain}/Job/appointmentsJob/insert/{$JobID}'+"/",
            createDataUrl:   '{$_subdomain}/Job/appointmentsJob/',
            createFormFocusElementId:'AppointmentDate',
            formInsertButton:'insert_save_btn_apt',
            dblclickCallbackMethod: 'gotoAPTEditPage',
            fnRowCallback:	    'inactiveAPTRow',
            

            frmErrorRules:   {
                                    APT_AppointmentDate:
                                        {
                                            required: true,
                                            dateITA: true
                                        },
                                    APT_AppointmentTime:
                                        {
                                            required: true
                                        },
                                    APT_AppointmentType:
                                        {
                                            required: true
                                        },
                                    APT_Notes:
                                        {
                                            required: true
                                        },
                                    APT_ServiceProviderID:
                                        {
                                            required: true
                                        },
                                    APT_ServiceProviderEngineerID:
                                        {
                                            required: true
                                        }      

                             },

           frmErrorMessages: {

                                    APT_AppointmentDate:
                                        {
                                           required: "{$page['Errors']['appointment_date']|escape:'html'}",
                                           dateITA: "{$page['Errors']['appointment_date_format']|escape:'html'}"
                                        },
                                    APT_AppointmentTime:
                                        {
                                            required: "{$page['Errors']['appointment_time']|escape:'html'}"
                                        },
                                    APT_AppointmentType:
                                        {
                                            required: "{$page['Errors']['appointment_type']|escape:'html'}"
                                        },
                                    APT_Notes:
                                        {
                                            required: "{$page['Errors']['notes']|escape:'html'}"
                                        },
                                    APT_ServiceProviderID:
                                        {
                                            required: "{$page['Errors']['service_provider']|escape:'html'}"
                                        },
                                    APT_ServiceProviderEngineerID:
                                        {
                                            required: "{$page['Errors']['sp_engineer']|escape:'html'}"
                                        }    


                              },                     

            popUpFormWidth:  750,
            popUpFormHeight: 430,


            updateButtonId:  'updateButtonId',
            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
            updateFormTitle: '{$page['Text']['update_apt_page_legend']|escape:'html'}',
            updateAppUrl:    '{$_subdomain}/Job/appointmentsJob/update/',
            updateDataUrl:   '{$_subdomain}/Job/appointmentsJob/',
            formUpdateButton:'update_save_btn_apt',
            updateFormFocusElementId: 'AppointmentDate',
            
            formCancelButton:   'cancel_btn_apt',

            
            
            deleteButtonId:  'deleteButtonId',
            deleteButtonText:'{$page['Buttons']['cancel']|escape:'html'}',
            deleteFormTitle: '{$page['Text']['delete_apt_page_legend']|escape:'html'}',
            deleteAppUrl:    '{$_subdomain}/Job/appointmentsJob/delete/',
            deleteDataUrl:   '{$_subdomain}/Job/appointmentsJob/',
            formDeleteButton:'delete_save_btn_apt',
            


            colorboxFormId:  "AppointmentForm",
            frmErrorMsgClass:"fieldError",
            frmErrorElement: "label",
            htmlTablePageId: 'AppointmentResultsPanel'
            
            
            
            
        });  


	
	
	//Engineer History Table ***********************************************************************
        
	var engineerHistoryTableObj = {
            aoColumns: [ 
                { sWidth: "9%", sType: "date-eu" },
                { sWidth: "6%"	},
                { sWidth: "15%" }, 
                { sWidth: "20%" }, 
                { sWidth: "39%"	},
                { sWidth: "14%", sClass: "alignCentre",  
                                mRender: function(data, type, full) { 
                                                 var html = '';
                                                 if (data !== null) { 
                                                     html += '<a href="#" class="rehi" data-img="' + data + '" data-caption="'+full[4]+'"  title="View Image"><img src="{$_subdomain}/images/remote_engineer_history_icon.png" /></a>';
                                                  }
                                                  if (full[3] === 'CONFIRM ARRIVAL') {
                                                      if (html!=='') {
                                                          html += '&nbsp;&nbsp;';
                                                      }
                                                      var ref = '52.2389613302674,-0.881384006739781';
                                                      html += '<a href="#" class="rehm" data-ref="'+ref+'" title="View Map"><img src="{$_subdomain}/images/icon_map_35px.png" /></a>';
                                                  }
                                                  return html;
                                          } 
                                 }
            ],
            bAutoWidth:		false,
            bDestroy:           true,
            bStateSave:         true,
            bServerSide:        false,
            bProcessing:        false,
            htmlTableId:        "engineerHistory",
            sDom:               "ft<'#dataTables_child'>rpli",
            sPaginationType:    "full_numbers",
            bPaginate:          true,
            bSearch:            false,
            aaSorting:		[[ 0, 'asc' ], [ 1, 'asc' ]],
            iDisplayLength:     10,
            sAjaxSource:        "{$_subdomain}/Job/getRemoteEngineerHistory/?jobID={$JobID}",
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
	    },
	    fnInitComplete: function() {
		var html = '<a href="#" class="delSearch" style="top:14px; right:4px; position:absolute;">\
				<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			    </a>';
		$(html).insertAfter("#EQ10 .dataTables_filter input");
		$(document).on("click", "#EQ10 .delSearch", function() {
		    $(".dataTables_filter input").val("");
		    engineerHistoryTable.fnFilter("");
		    return false;
		});
	    } //,
	    /*fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		if(aData[5]) {              
		    var html = '<a href="#" class="rehi" img="' + aData[5] + '" caption="'+aData[4]+'"><img src="{$_subdomain}/images/remote_engineer_history_icon.png" /></a>';
		    $($(nRow).find("td").toArray()[5]).css("text-align", "center");
		    $($(nRow).find("td").toArray()[5]).html(html);
		}
	    } */
	};

	engineerHistoryTable = $("#engineerHistory").dataTable(engineerHistoryTableObj);

        $(document).on("click", ".rehm", function() {
            //var maplink = 'http://maps.google.com/maps?layer=c&cbll=52.2389613302674,-0.881384006739781&cbp=12,0,0,0,0';
            var maplink = 'http://maps.google.com/maps?q='+$(this).attr("data-ref")+'&z=18';
            $.colorbox({ 
		opacity: 0.75,
		href: maplink,
                width: $(window).width(),
                height: $(window).height(),
                iframe: true
            });
	    return false;
        });
        
	$(document).on("click", ".rehi", function() {
	    /*var html = "<img id='cboxImg' src='{$_subdomain}" + $(this).attr("img") + "' />";
	    $.colorbox({ 
		opacity:    0.75,
		html:	    html,
		onComplete: function() {
		    console.log($("#cboxImg").width());
		    $.colorbox.resize({ innerWidth: $("#cboxImg").width() });
		}
	    });
	    $.colorbox.resize();*/
            //console.log("{$_subdomain}/ImageServer/index/"+$(this).attr("img"));
            var caption = $(this).attr("data-caption");
            if (caption == 'null') caption = '';
            $.colorbox({ 
		opacity:    0.75,
		href:	    "{$_subdomain}/ImageServer/index/"+$(this).attr("data-img"),
                title: caption,
                maxWidth: $(window).width(),
                maxHeight: $(window).height()
            }).resize();
	    return false;
	});


	//**************************************************************************************

        $('#ContactHistory').PCCSDataTable( {
            aoColumns: [ 
                { sWidth: "9%", sType: "date-eu" },    
                { sWidth: "6%" },    
                { sWidth: "15%" },    
                { sWidth: "20%" },
                { bVisible: false },
                null,
                { sWidth: "7%" }
            ],
            displayButtons:	'A',
            //bServerSide:        false,
            sDom:               'ft<"#dataTables_command3">rp',    
            bottomButtonsDivId:	'dataTables_command3',
            htmlTableId:        'ContactHistory',
            fetchDataUrl:       '{$_subdomain}/Data/contacthistory/{$JobID}/',
            searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
            createAppUrl:       '{$_subdomain}/Job/AddContactNote/{$JobID}/',
            createDataUrl:      '{$_subdomain}/Job/AddContactNote/',
            colorboxFormId:     'JobUpdateContactNoteForm',
            formInsertButton:   'insert_save_btn_notes',
            formCancelButton:   'cancel_btn_notes',
            addButtonId:        'addContactHistory',
            addButtonText:      '{$page['Buttons']['add_contact_note']|escape:'html'}',
            createFormTitle:    '{$page['Text']['add_contact_notes_legend']|escape:'html'}',
            sPaginationType:    'two_button',
            hidePaginationNorows: true,
            iDisplayLength:	12,
            aaSorting:		[[ 0, 'desc' ], [ 1, 'desc' ]],
            frmErrorMsgClass:   'fieldError',
            frmErrorElement:    'label',
            formDataErrorMsgId: 'suggestText',
            frmErrorSugMsgClass:'formCommonError',
            frmErrorRules:   {
                 ContactHistoryActionID:   { required: true },
                 Note:                     { NotEqualLabel: true}   
               },
            frmErrorMessages: {
                 ContactHistoryActionID:   { required: "{$page['Errors']['contact_action_error']|escape:'html'}" },
                 Note:                     { NotEqualLabel: "{$page['Errors']['contact_note_error']|escape:'html'}" }   
            },
            onSuccess: function(mode, data) {
		$('#NumContactHistoryRows').text(data['count']);
	    }
        }); 


	auditTrailTable = $("#AuditTrail").dataTable({
            bAutoWidth:	false,
            aoColumns: [
                { sWidth: "9%", sType: "date-eu" },
                { sWidth: "6%"	},
                { sWidth: "15%" }, 
                { sWidth: "14%" }, 
                { sWidth: "28%"	},
                { sWidth: "28%"	}
            ],
            bDestroy:           true,
            bStateSave:         true,
            bServerSide:        false,
            bProcessing:        false,
            htmlTableId:        "AuditTrail",
            sDom:               "ft<'#dataTables_child'>rpli",
            sPaginationType:    "full_numbers",
            bPaginate:          true,
            bSearch:            false,
            iDisplayLength:     10,
            sAjaxSource:        "{$_subdomain}/Job/getAuditTrailByJobID/?jobID={$JobID}",
	    aaSorting:		[[0, "desc"], [1, "desc"]],
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:10px; position:relative;'>Search within results:</span>&nbsp;"
	    },
	    fnInitComplete: function() {
		var html = '<a href="#" class="delSearch" style="top:14px; right:4px; position:absolute;">\
				<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			    </a>';
		$(html).insertAfter("#EQ9 .dataTables_filter input");
		$(document).on("click", "#EQ9 .delSearch", function() {
		    $(".dataTables_filter input").val("");
		    auditTrailTable.fnFilter("");
		    return false;
		});
	    },
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		if(aData[4] == "") {
		    $('td:eq(4)', nRow).html("Previous entry was blank");
		}
	    }
	});


        $('#StatusChanges').PCCSDataTable( {
            //bServerSide:        false,
            "aoColumns": [ 
                /* Date */	{ sWidth : "9%", sType: "date-eu" },   
                /* Time */	{ sWidth : "6%" },   
                /* User */	{ sWidth : "15%" },   
                /* Status */	{ sWidth : "20%" }, 
                /* Additional Information */  null
            ],
            sDom:               'ft<"#dataTables_command4">rp',    
            bottomButtonsDivId:	'dataTables_command4',
            htmlTableId:        'StatusChanges',
            fetchDataUrl:       '{$_subdomain}/Data/statuschanges/{$JobID}/',
            searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
            sPaginationType: 'two_button',
            hidePaginationNorows:  true,
            iDisplayLength:  12,
            aaSorting:    [[ 0, "desc" ], [ 1, "desc" ]]
        });
            
        $("#ManufacturerID").change(function () {
            var value = $("#ManufacturerID option:selected").text();
            $("#ServiceBaseManufacturer").val(value);
        });
        
        $("#ServiceBaseManufacturer").change(function () {
            //remove selected ManufacturerID 
            //if free form text entered into input box
            $('#ManufacturerID option:selected').remove();
        });

        $("#ProductTypeID").change(function () {
            var value = $("#ProductTypeID option:selected").text();
            $("#ServiceBaseUnitType").val(value);
        });
    
        resizeColumns();
            
        //accordion
        $('.main .head').click(function() {
            $(this).children('.icon_c').toggleClass('icon_e');
            $('html, body').animate({ scrollTop: $('#jobdetails').height() }, 800);
            $(this).next().slideToggle('slow');
            return false;
        }).next().hide();
            
        $( 'input[name="PurchaseDate_1"], input[name="PurchaseDate"]' ).datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: "button",
                    buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/date-picker.gif",
                    buttonImageOnly: true
        });
            
        $('.ui-datepicker-trigger').hide();
            
	    
        $("a.colorbox").click(function() {
 
	    $.colorbox({ 
		href: $(this).attr('href'),
		title: $(this).attr('title'),
		opacity: 0.75,
		width: 800,
		overlayClose: false,
		escKey: false
	    });
		
            return false;
	    
        });
                   
        /************************************************
        *                                               *
        * Functions for handling inline editing on form *
        *                                               *
        ************************************************/
        $(".inline-edit").click(function(){
            
            var fieldset = $(this).attr('href' );
                
            switch(fieldset.substr(1)){
                case'InvoiceCosts':
                    $(fieldset + ' #discount-type').show();
                    break;
                }
                                
            $(fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput,' + fieldset + ' .memoInput').show();
               
            $('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').hide();
            
            if(fieldset=="#InvoiceCosts")   
            {
                 $('#Gross').hide();
                 $('#VAT').hide();
                 $('#TotalNet').hide();
            }
            
                
	    
	    if(fieldset == "#IssueReport") {
		var textCounter = {  
		    maxCharacterSize:	2000,  
		    originalStyle:	"jobUpdateTextCount",  
		    warningStyle:	"warningDisplayInfo",  
		    warningNumber:	1700,  
		    displayFormat:	"#input Characters | #left Characters Left | #words Words"
		};
		$("textarea[name=IssueReport]").textareaCount(textCounter, function() {
		    if($("textarea[name=IssueReport]").val().length > 300) {
			$("textarea[name=IssueReport]").css("color", "red");
		    } else {
			$("textarea[name=IssueReport]").css("color", "inherit");
		    }
		});  
		$(".jobUpdateTextCount").css("font-size", "11px");
	    }
	    

            if(fieldset=="#AdditionalInformation")
            {
                $('#AdditionalInformationFormPanel').height(280);
            }

            resizeColumns();
            
            return false;
        });


        $(".inline-cancel").click(function(){
            
            var fieldset = $(this).attr('href' );
	    
	    if(fieldset == "#IssueReport") {
		$(".jobUpdateTextCount").remove();
	    }
                
            switch(fieldset.substr(1)) {
                case 'InvoiceCosts':
                    $(fieldset + ' #discount-type').hide();
                    break;
            }                             
                                
            $('.error-highlight').removeClass('error-highlight');
                
            $( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + 
                    fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput, ' + fieldset + ' .memoInput, ' + fieldset + ' .error').hide();
                
            $('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').show(); 
                
            resizeColumns();
            
            if(fieldset=="#InvoiceCosts" || fieldset=="#AdditionalInformation")
            {
               location.reload();
            }

        });
                  	    
        $('input').change(function(){
            $('.error').hide();
            $('.error-highlight').removeClass( 'error-highlight' );                
        });
            
	    
        $(".inline-save").click(function() {           
	
            var fieldset = $(this).attr('href'); 
                
	    if(fieldset == "#IssueReport") {
		$(".jobUpdateTextCount").remove();
	    }
		
            /*switch(fieldset.substr(1)) {
                case 'InvoiceCosts': {
                    $(fieldset + ' .discount-type').hide();
                    break;
		}
            } */
                
            $(fieldset).validate({
                errorPlacement: function(error, element) {
                    error.insertAfter(element);
                }            
            });
                
            $('html, body').css('cursor', 'wait');
            $( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel ').attr('disabled', true);
	    
	    //console.log("{$_subdomain}/Data/update" + fieldset.substr(1) + '/{$JobID}/{$ClientID}');
	    
            $.post("{$_subdomain}/Data/update" + fieldset.substr(1) + '/{$JobID}/{$ClientID}', 
		    $(fieldset).serializeArray(),
		    function(data) { 
		    
			$('html, body').css('cursor', 'auto');
			$( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel ').removeAttr('disabled');
			if(data !== null) {
			    var server = jQuery.parseJSON(data);

			    if(server.status == 'error') {

				$(fieldset + ' .error').show();
				$(fieldset + ' .error').text(server.message);
				if (server.field != '') {
				    $(fieldset + ' [name="' + server.field +'"]').addClass('error-highlight');
				} 
                                resizeColumns();
			    } else {   

                            if ('ModelNumber' in server.data) $( 'ModelNumber' ).text( server.data['ModelNumber'] );
                            if ('UnitTypeID' in server.data) $( 'UnitTypeID' ).text( server.data['UnitTypeID']);

				$(fieldset + ' .memoInput').each(function() {
				    elt = $(this);                               
				    elt.parent().children('.memoValue').text(elt.children('textarea').val());
				});

				$(fieldset + ' .fieldInput').each(function() {

				    elt = $(this);

				    if(elt.has("textarea").length) {
					if($(this).children('textarea').attr('id') == 'ModelDescription') {
					    if(server.data['ModelDescription'] == '' || server.data['ModelDescription'] == null) {
						$(this).parent().children('.fieldValue').text(server.data['ModelNumber']);
						$(this).children('textarea').val(server.data['ModelNumber']);                                       
					    } else {
						$(this).parent().children('.fieldValue').text(server.data['ModelDescription']);
						$(this).children('textarea').val(server.data['ModelDescription']);
					    }
					} else {
					    $(this).parent().children('.fieldValue').text($(this).children('textarea').val());
					}
				    } else if(elt.has("select").length) {
					//alert('select: '+elt.find('option:selected').text());
					if ($(this).children('select').attr('id') == 'ManufacturerID') {
					    $(this).parent().children('.fieldValue').text(server.data['ManufacturerName']);
					    $("#ServiceBaseManufacturer").val(server.data['ManufacturerName']);
					    //$('#ManufacturerID option[value="'+server.data['ManufacturerID']+'"').attr('selected','selected');
					} else if($(this).children('select').attr('id') == 'ProductTypeID') {
					    $(this).parent().children('.fieldValue').text(server.data['UnitTypeName']);
					    $("#ServiceBaseUnitType").val(server.data['UnitTypeName']);
					} else {
					    $(this).parent().children('.fieldValue').text($(this).find('option:selected').text());
					}
				    } else if(elt.has("input").length) {
					$(this).parent().children('.fieldValue').text($(this).children('input').val());
				    } 

				});

				$('.error-highlight').removeClass('error-highlight');

				$(fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + 
				  fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput, ' + 
				  fieldset + ' .memoInput, ' + fieldset + ' .error').hide();

				$('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').show(); 

                                resizeColumns();

			    }
			}

                         if(fieldset=="#InvoiceCosts" && server.status != 'error' || fieldset=="#AdditionalInformation")
                         {
                            location.reload();
                         }

		    }
		    
	    );

            return false;
	    
        });
            
	    
        // DiscountType
        $('input[name="DiscountType"]').click(function(){
            //$.fn.log('clicked ' + $(this).val());
            if($(this).val() == 'value'){
                $('#DiscountAllowedRate').attr({ 'disabled' : 'disabled' });
            $('input[name="DiscountAllowed"]').removeAttr('disabled');
            }else{
                $('#DiscountAllowedRate').removeAttr('disabled');
                $('input[name="DiscountAllowed"]').attr({ 'disabled' : 'disabled' });
            }
        });
            
        $('#DiscountAllowedRate').keyup(function(){
            //console.log( $('#DiscountAllowedLabel').text().substr(0, -2) );
            $('#DiscountAllowedLabel').text('Discount Allowed @' + $(this).val() + '%' );
        });


	if(getUrlVars()["ref"] == "responseRequired" || getUrlVars()["ref"] == "authRequired" || getUrlVars()["ref"] == "raJobs") {
	    $('#EQ7 .show').show();
	    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
	}
	
	
	$(document).on("click", "#editStatus", function() {
	    $.post("{$_subdomain}/Job/getEntityStatusList", { jobID: {$JobID} }, function(response) {
		$.colorbox({
		    html:	response, 
		    width:	"350px",
		    scrolling:  false,
		    onClosed: function() {
			$("#save").die();
		    }
		});
		return false;
	    });
	    return false;
	});
        	
    });	// end document ready


    // following function to be assigned to popup buttons
    $(document).on('click', '#quick_find_btn', function() {
        //log.info($('#PostalCode').val());
            $.ajax( { type:'POST',
                    dataType:'html',
                    data:'postcode=' + $('#PostalCode').val(),
                    success:function(data, textStatus) { 
                              
                        if(data=='EM1011' || data.indexOf("EM1011")!=-1) {
                            $('#selectOutput').html("<label class='fieldError'>{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                            $("#selectOutput").slideDown("slow");
                          //  $("#postcode_not_listed_btn").slideUp("slow");

                            $("#BuildingName").val('');	
                            $("#Street").val('');
                            $("#Area").val('');
                            $("#City").val('');
                            $('#PostalCode').focus();

                        } else {

                            $('#selectOutput').html(data); 
                            $("#selectOutput").slideDown("slow");
                            $("#postSelect").focus();

                      //      $("#postcode_not_listed_btn").slideDown("slow");

                        }

                        //$(this).colorbox.resize({ height: ($('#EditCustomerDetails').height()+120)+"px" });
			
			$.colorbox.resize();
                          
                    },
                    beforeSend:function(XMLHttpRequest){ 
                            $('#fetch').fadeIn('medium'); 
                    },
                    complete:function(XMLHttpRequest, textStatus) { 
                            $('#fetch').fadeOut('medium') 
                            setTimeout("$('#quickButton').fadeIn('medium')",500);
                    },
                    url:'{$_subdomain}/index/addresslookup' 
                } ); 
            return false;                 
    });

    $(document).on('click', '.cancel', function() {
        $(this).colorbox.close();
	return false;
    });

    $(document).on('click', '#cancel_btn', function() {
        $(this).colorbox.close();
	return false;
    });
    
    
    $(document).on('blur', '.ValueCorrection', function() {
       
      var $boxValue = $(this).val(); 
      
      if($boxValue)
      {
              
              if($boxValue.indexOf(".") == -1)
              {
                    if($boxValue.length>=2)
                    {   
                        position = $boxValue.length-2;
                        $(this).val( $boxValue.substr(0, position) + '.' + $boxValue.substr(position) );   
                    }
                    else
                    {
                        $(this).val( $boxValue );       
                        
                        $(this).val("0.0"+$(this).val());    
                        
                    }
              }    
      }    
       
    });
    


    /*Add a click handler to the no mail checkbox - starts here*/
    $(document).on('click', '#jbNoContactEmail', 
        function() {
            if($("#jbNoContactEmail").is(':checked')) {
                $("#jbContactEmailSup").fadeOut();  
                $(".fieldError[for='jbContactEmail']").fadeOut(); 
            } else {
                $("#jbContactEmailSup").fadeIn();  
                if(!$("#jbContactEmail").val()) {
                    $(".fieldError[for='jbContactEmail']").fadeIn();
                }
            }
        }
    );  
    /*Add a click handler to the no mail checkbox - ends here*/


    /* Add a change handler to the county dropdown of job booking page - strats here*/
    $(document).on('change', '#jbCounty', 
        function() {

            $("#jbCounty option:selected").each(function () {
                $selected_cc_id =  $(this).attr('id');
                $country_id_array = $selected_cc_id.split('_');
                if($country_id_array[1]!='0') {
                    $("#jbCountry").val($country_id_array[1]);
                    $("#jbCountry").blur();
                    $("#jbCountry").attr("disabled","disabled").addClass('disabledField');
                } else {
                    $("#jbCountry").val('');
                    $("#jbCountry").removeAttr("disabled").removeClass('disabledField');
                }
            });
        }      
    );
    /* Add a change handler to the county dropdown of job booking page - ends here*/



    $(document).on('click', '#EditCustomerDetailsSave', function() {
                                      
        $('.auto-hint').each(function() {
                $this = $(this);
                if ($this.val() == $this.attr('title')) {
                    $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                    if ($this.hasClass('auto-pwd')) {
                        $this.prop('type','password');
                    }
                }
        } );
    
	$('#EditCustomerDetails').validate({

	    ignore: '',

	    rules:  {
		ContactTitle: { required: true },
		ContactLastName: {  required: true },
		PostalCode: { required: true },
		postSelect: { required: true },
		BuildingName: { required: true },
		Street: { required: true },
		City: { required: true },

		Country: {
		    required: function (element) {
			if($("#jbCountryMandatory").val()=='1' && $("#jbBrandCountry").val()=='1') {
			    return true;
			} else {
			    return false;
			}
		    }
		},
		ContactEmail: {
		    required: function (element) {
			if($("#jbNoContactEmail").is(':checked')) {
			    $("#jbContactEmailSup").fadeOut();  
			    $(".formError [for='jbContactEmail']").fadeOut(); 
			    return false;
			} else {
			    $("#jbContactEmailSup").fadeIn();  
			    $(".formError [for='jbContactEmail']").fadeIn(); 
			    return true;
			}
		    },    
		    email: true
		},
		ContactHomePhone: {
		    digits: true,
		    required: function() {
			if($("#jbContactMobile").val() == "") {
			    return true;
			} else {
			    return false;
			}
		    }
		},
		ContactMobile: {
		    digits: true,
		    required: function() {
			if($("#jbContactHomePhone").val() == "" && $("#jbContactMobile").val() == "") {
			    return false;
			} else if($("#jbContactHomePhone").val() == "") {
			    return true;
			} else {
			    return false;
			}
		    }
		}

	    },
	    messages: {
		ContactTitle: {
		    required: "{$page['Errors']['contact_title']|escape:'html'}"
		},
		ContactLastName: {
		    required: "{$page['Errors']['contact_last_name']|escape:'html'}"
		},
		PostalCode: {
		    required: "{$page['Errors']['postal_code']|escape:'html'}"
		},
		postSelect: {
		    required: "{$page['Errors']['select_address']|escape:'html'}"
		},
		BuildingName: {
		    required: "{$page['Errors']['building_name']|escape:'html'}"
		},
		Street: {
		    required: "{$page['Errors']['street']|escape:'html'}"
		},
		City: {
		    required: "{$page['Errors']['city']|escape:'html'}"
		},
		Country: {
		    required: "{$page['Errors']['country']|escape:'html'}"
		},
		ContactEmail: {
		    required: "{$page['Errors']['email']|escape:'html'}",
		    email: "{$page['Errors']['valid_email']|escape:'html'}"
		},
		ContactHomePhone: {
		    required: "{$page['Errors']['daytime_phone']|escape:'html'}",
		    digits: "{$page['Errors']['valid_daytime_phone']|escape:'html'}"
		},
		ContactMobile: {
		    digits: "{$page['Errors']['valid_mobile_no']|escape:'html'}"
		}
	    },
		
	    errorPlacement: function(error, element) {
		error.insertAfter( element );
	    },
	    errorClass: 'fieldError',
	    onkeyup: false,
	    onblur: false,
	    errorElement: 'label',
	    
	    submitHandler: function() {
		$.post('{$_subdomain}/Data/updateCustomerDetails/{$CustomerID}/{$JobID}',        
		$("#EditCustomerDetails").serializeArray(),           
		function(data){
		    
		    var p = eval("(" + data + ")");                
		    if(p['Status']=="OK") {    
			$infoMsg = $("#dataUpdatedMsg").html();
		    } else {
			$infoMsg =   $("#accessDeniedMsg").html();
		    }

		    $.colorbox( { 
			title:		'Edit Customer Details',
			html:		$infoMsg,
			opacity:	0.75,
			height:		210,
			width:		600,
			overlayClose:	false,
			escKey:		false,
			onCleanup:	reloadPage 

		    });                      
		});
	    }
	    
	});
           
	function reloadPage() {
	    location.reload(true); 
	}

	//return false;
	
    });
    
    //Customer Address Google Map -----------------------------------------------
    
    $(document).on("click", ".customermap", function() {
            var maplink = 'http://maps.google.com/maps?q='+CustomerLocation+'&z=18';
            $.colorbox({ 
		opacity: 0.75,
		href: maplink,
                width: $(window).width(),
                height: $(window).height(),
                iframe: true
            });
	    return false;
        });
        
    //Collection Address Google Map -----------------------------------------------
    
    $(document).on("click", ".collectionmap", function() {
            var maplink = 'http://maps.google.com/maps?q='+CollectionLocation+'&z=18';
            $.colorbox({ 
		opacity: 0.75,
		href: maplink,
                width: $(window).width(),
                height: $(window).height(),
                iframe: true
            });
	    return false;
        });



    //Edit delivery details -----------------------------------------------------------------------------------------------------------

    $(document).on('click', '#deliveryDetailsSave', function() {
        
        $('.auto-hint').each(function() {
	    $this = $(this);
	    if ($this.val() == $this.attr('title')) {
		$this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
		if ($this.hasClass('auto-pwd')) {
		    $this.prop('type','password');
		}
	    }
        });

	$('#editDeliveryDetails').validate({

	    debug : true,

	    onsubmit : function() { alert("submitted"); },

	    ignore: '',

	    rules:  {
		PostalCode:	{ required: true },
		postSelect:	{ required: true },
		BuildingName:	{ required: true },
		Street:		{ required: true },
		City:		{ required: true },
		ColAddEmail: { email: true }
		
		/*
		Country: {
		    required: function (element) {
			if($("#jbCountryMandatory").val()=='1' && $("#jbBrandCountry").val()=='1') {
			    return true;
			} else {
			    return false;
			}
		    }
		}
		*/
		
		/*
		ContactEmail: {
		    required: function (element) {
			if($("#jbNoContactEmail").is(':checked')) {
			    $("#jbContactEmailSup").fadeOut();  
			    $(".formError [for='jbContactEmail']").fadeOut(); 
			    return false;
			} else {
			    $("#jbContactEmailSup").fadeIn();  
			    $(".formError [for='jbContactEmail']").fadeIn(); 
			    return true;
			}
		    },    
		    email: true
		},
		ContactHomePhone: {
		    required: true,
		    digits: true
		},
		ContactMobile: {
		    digits: true
		}
		*/
		
	    },
	
	    messages: {
		PostalCode: {
		    required: "{$page['Errors']['postal_code']|escape:'html'}"
		},
		postSelect: {
		    required: "{$page['Errors']['select_address']|escape:'html'}"
		},
		BuildingName: {
		    required: "{$page['Errors']['building_name']|escape:'html'}"
		},
		Street: {
		    required: "{$page['Errors']['street']|escape:'html'}"
		},
		City: {
		    required: "{$page['Errors']['city']|escape:'html'}"
		},
		Country: {
		    required: "{$page['Errors']['country']|escape:'html'}"
		},
                ColAddEmail: {
		    email: "{$page['Errors']['valid_email']|escape:'html'}"
		}
	    },
	    
	    
	    errorPlacement: function(error, element) {
		error.insertAfter(element);
	    },
	    errorClass: 'fieldError',
	    onkeyup: false,
	    onblur: false,
	    errorElement: 'label',
	    
	    submitHandler: function() {
		$.post('{$_subdomain}/Data/updateDeliveryDetails/{$CustomerID}/{$JobID}',        
			
			$("#editDeliveryDetails").serializeArray(),           
			
			function(data) {
			    
			    var p = eval("(" + data + ")");                
			    if(p['Status']=="OK") {    
				$infoMsg = $("#dataUpdatedMsg").html();
			    } else {
				$infoMsg =   $("#accessDeniedMsg").html();
			    }

			    $.colorbox( { 
				title :	    'Edit Collection/Delivery Details',
				html :	    $infoMsg,
				opacity :   0.75,
				height :    210,
				width :	    600,
				overlayClose: false,
				escKey :    false,
				onCleanup : reloadPage 
			    } );                      
			}
			
		);
	    }
	    
	} );
        
        function reloadPage() {
	    location.reload(true); 
	}

	//return false;
        
    } );
    
    // Edit Service Provider.....
    $(document).on('click', '#update_save_btn', function() {
        
        $('.auto-hint').each(function() {
            $this = $(this);
            if ($this.val() == $this.attr('title')) {
                $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                if ($this.hasClass('auto-pwd')) {
                    $this.prop('type','password');
                }
            }
        });
        
        $('#ServiceProvidersForm').validate({
        
            rules: {
                CompanyName:     { required: true },
                PostalCode:      { required: true },
                Street:          { required: true },
                TownCity:        { required: true },    
                ContactEmail:    { required: true,
                                   email:    true },
                ReplyEmail:      { email:    true },     
                ContactPhone:    { required: true },     
                Platform:        { required: true },  
                IPAddress:       { required: function (element) {
                                            if($("#Platform").val()=='ServiceBase') {
                                                $("#IPAddressSup").show();
                                                return true;
                                            } else {
                                                $("#IPAddressSup").hide();
                                                return false;
                                            }
                                        }
                                  },  
                Port:             { required: function (element) {
                                            if($("#Platform").val()=='ServiceBase') {
                                                $("#PortSup").show();
                                                return true;
                                            } else {
                                                $("#PortSup").hide();
                                                return false;
                                            }
                                        },
                                        digits: true
                                   },
               WeekdaysOpenTime:   { time: true },
               WeekdaysCloseTime:  { time: true },
               SaturdayOpenTime:   { time: true },
               SaturdayCloseTime:  { time: true },
               DefaultTravelTime:  { required: true,
                                     digits: true },
               DefaultTravelSpeed: { required: true,
                                     digits: true,
                                     max: 200,
                                     min:50 }                                                   
   
            } ,
            messages: {
               CompanyName:        { required: "{$service_providers_page['Errors']['company_name']|escape:'html'}" },
               PostalCode:         { required: "{$service_providers_page['Errors']['post_code']|escape:'html'}" },
               Street:             { required: "{$service_providers_page['Errors']['street']|escape:'html'}" },
               TownCity:           { required: "{$service_providers_page['Errors']['town']|escape:'html'}" },       
               ContactEmail:       { required: "{$service_providers_page['Errors']['email']|escape:'html'}",
                                     email:    "{$service_providers_page['Errors']['valid_email']|escape:'html'}" },
               ReplyEmail:         { email:    "{$service_providers_page['Errors']['valid_email']|escape:'html'}" },   
               ContactPhone:       { required: "{$service_providers_page['Errors']['phone']|escape:'html'}" },     
               Platform:           { required: "{$service_providers_page['Errors']['platform']|escape:'html'}" },  
               IPAddress:          { required: "{$service_providers_page['Errors']['ip_address']|escape:'html'}" },  
               Port:               { required: "{$service_providers_page['Errors']['port']|escape:'html'}",
                                     digits:   "{$service_providers_page['Errors']['digits']|escape:'html'}" },
               WeekdaysOpenTime:   { time: "{$service_providers_page['Errors']['weekdays_open_time_invalid']|escape:'html'}" },
               WeekdaysCloseTime:  { time: "{$service_providers_page['Errors']['weekdays_close_time_invalid']|escape:'html'}" },
               SaturdayOpenTime:   { time: "{$service_providers_page['Errors']['saturday_open_time_invalid']|escape:'html'}" },
               SaturdayCloseTime:  { time: "{$service_providers_page['Errors']['saturday_close_time_invalid']|escape:'html'}" },
               DefaultTravelTime:  { required: "{$service_providers_page['Errors']['default_travel_time']|escape:'html'}",
                                     digits: "{$service_providers_page['Errors']['digits']|escape:'html'}" },
               DefaultTravelSpeed: { required: "{$service_providers_page['Errors']['default_travel_speed']|escape:'html'}",
                                     digits: "{$service_providers_page['Errors']['digits']|escape:'html'}" }
            },               

            errorClass: 'fieldError',
            onkeyup: false,
            onblur: false,
            errorElement: 'label',

            submitHandler: function() {
                                                
                $.post( '{$_subdomain}/OrganisationSetup/ProcessData/ServiceProviders/',        
                        $('#ServiceProvidersForm').serialize(),      
                        function(data) {
                             // DATA NEXT SENT TO COLORBOX
                             var p = eval("(" + data + ")");
                             if(p['Status']=="OK") {    
				$infoMsg = $("#dataUpdatedMsg").html();
			     } else {
				//$infoMsg =   $("#accessDeniedMsg").html();
                                $infoMsg = p['message'];
			     }

			     $.colorbox( { 
				title :	    '{$service_providers_page['Text']['update_page_legend']|escape:'html'}',
				html :	    $infoMsg,
				opacity :   0.75,
				height :    210,
				width :	    600,
                                fixed:true,
				overlayClose: false,
				escKey :    false,
				onCleanup : reloadPage 
			     } );
 
                         }

                );//Post ends here..
                                     
            } // submit handler

        });
        
        function reloadPage() {
	    location.reload(true); 
	}

	//return false;
        
    });
    
    $(document).on('click', '#AddClientDetails', 
                     function() { 
                       if($("#ClientID").val()!='' && ($("#ClientContactPhone").val()!='' || $("#ClientContactEmail").val()!='')) {
                          var newRow = "<tr id='C_"+$("#ClientID").val()+"'  ><td><input type='hidden' name='ClientIDs[]' value='"+$("#ClientID").val()+"' >"+$('#ClientID option:selected').html()+"</td><td><input type='hidden' name='ClientContactPhones[]' value='"+$("#ClientContactPhone").val()+"' >"+$("#ClientContactPhone").val()+"</td><td><input type='hidden' name='ClientContactEmails[]' value='"+$("#ClientContactEmail").val()+"' >"+$("#ClientContactEmail").val()+"</td><td><span><img style='cursor:pointer;' class='CloseImage'  src='{$_subdomain}/css/Skins/{$_theme}/images/close.png' width='17' height='16' ></span></td></tr>";
                          $("#ClientsList").append(newRow);
                          $("#ClientID option[value='"+$("#ClientID").val()+"']").hide();
                          $("#ClientID").val('');
                          $("#ClientContactPhone").val('');
                          $("#ClientContactEmail").val('');
                       } else {
                          alert('{$service_providers_page['Errors']['client_contact']|escape:'html'}');
                       }
                    }
    );
             
    $(document).on('click', '.CloseImage', 
                     function() {
                         if(confirm('{$service_providers_page['Errors']['delete_client_contact']|escape:'html'}')) {    
                            $row_id = $(this).closest('tr').attr("id");
                            $row_id =  $row_id.replace("C_",""); 
                            $("#ClientID option[value='"+$row_id+"']").show();      
                            $(this).closest('tr').remove();   
                         }
                     }
   ); 
   
   {********************** Book Collection Handling *****************}
   function BookCollectionFormData() {

        this.CollectionDate = '';
        this.PreferredCourier = '';
        this.ConsignmentNo = '';
        this.Name = '';
        this.Postcode = '';
        this.BuildingName = '';
        this.Street = '';
        this.Area = '';
        this.Town = '';
        this.County = '';
        this.Country = '';
        this.Email = '';
        this.Phone = ''; 
        this.PreferredCourierID = null;
        this.CountyID = null;
        this.CountryID = null;
    
        this.update = function() {
        
            var address = '';
            var map_address = '';
            if (this.Street==='' && this.Town==='' && this.Postcode==='') {
                address = '<span style="color:black;">{$page['Text']['address_is_blank']|escape:'html'}</span>';
            } else {
                if (this.Name!=='') {
                    address += this.Name.toUpperCase();
                    address += ', ';
                }
                if (this.BuildingName!=='') {
                    address += this.BuildingName.toUpperCase();
                    address += ' ';
                    map_address += this.BuildingName;
                    map_address += ' ';                    
                }
                if (this.Street!=='') {
                    address += this.Street.toUpperCase();
                    address += ', ';
                    map_address += this.Street;
                    map_address += ', ';
                }
                if (this.Area!=='') {
                    address += this.Area.toUpperCase();
                    address += ', ';
                    map_address += this.Area;
                    map_address += ', ';
                }
                if (this.Town!=='') {
                    address += this.Town.toUpperCase();
                    address += ', ';
                    map_address += this.Town;
                    map_address += ', ';
                }
                if (this.Postcode!=='') {
                    address += this.Postcode.toUpperCase();
                    map_address += this.Postcode;
                }
            }
            var tel = '<strong>(T)</strong> '+this.Phone.toUpperCase();
            var email = '<strong>(E)</strong> ' + this.Email.toUpperCase();
            
            // save the google map address in global CollectionLocation variable
            CollectionLocation = map_address;
            $('#collectionmaplink').removeClass('hidden');
            if (CollectionLocation=='') {
                $('#collectionmaplink').addClass('hidden');
            }
            
            $('#CollectionAddress').html(address);
            $('#CollectionTelephone').html(tel);
            $('#CollectionEmail').html(email);
            $('#ConsignmentDate').html(this.CollectionDate);
            $('#CourierName').html(this.PreferredCourier);
             if (this.PreferredCourier == 'DPD') {
                $('#ConsignmentNo').html('<a href="{$DPDTrackerURL|escape:'html'}'+this.ConsignmentNo+'" target="_blank">'+this.ConsignmentNo+'</a>');
            } else {
                $('#ConsignmentNo').html(this.ConsignmentNo);
            }

        };
   }
   
  {* $(document).on('click', '#collectionDetailsEdit', 
            function() {
                $.colorbox({
                    href :	'{$_subdomain}/Courier/BookCollectionForm/job={$JobID}', 
                    width:	'700px',
                    scrolling:	false,
                    overlayClose: false,
                    fixed: true
                });
                return false;
            }
    );
    *}
    
    {********************** Book Collection End **********************}


    $(document).on("click", "#printRoutines", function() {
	openPrintModal();
    });


    function openPrintModal() {
    
	var html = "<fieldset>\
			<legend align='center'>Print Options</legend>\
			<br/>\
			<input name='printRadio' type='radio' value ='2' /> Print Customer Receipt\
			<br/>\
			<input name='printRadio' type='radio' value ='1' /> Print Job Card\
                        <br/>\
			<input name='printRadio' type='radio' value ='3' /> Print Service Report\
			<br/>\
			<br/>\
			<div style='width:100%; text-align:center;'>\
			    <a href='#' id='print' style='width:50px;' class='btnStandard'>Print</a>&nbsp;\
			    <a href='#' id='cancel' style='width:50px;' class='btnCancel'>Cancel</a>\
			</div>\
		    </fieldset>\
		   ";

	$.colorbox({
	    html :	html, 
	    width:	"320px",
	    scrolling:	false
	});

	$(document).on("click", "#print", function() {
	    var printType = $("input[name=printRadio]:radio:checked").val();
	    $.colorbox.close();
            var url = "{$_subdomain}/Job/printJobRecord/" + {$JobID} + "/" + printType + "/?pdf=1";
	    $("#print").die();
            window.open(url, "_blank");
            window.focus();
            return false;
	});
	
    }
    
    function replicateJobs() {
    
        var html = "<fieldset>\
                <legend align='center'>Replicate Job</legend>\
                <br/>\
                You have requested to replicate the details on this job to a new job.  If you continue a new job will be created.<br/>\<br/>\
                Replicate Type:<br/>\<br/>\
                <div style='padding-left: 80px;'><input name='rType' type='radio' value ='Customer' checked='true' /> Customer Details Only</div>\
                <div style='padding-left: 80px;'><input name='rType' type='radio' value ='Both' style='padding-left: 20px;'/> Customer & Job Details</div>\
                <br/>\
                <div style='width:100%; text-align:center;'>\
                    <a href='#' id='print' style='width:60px;' class='btnStandard'>Continue</a>&nbsp;\
                    <a href='#' id='cancel' style='width:60px;' class='btnCancel'>Cancel</a>\
                </div>\
            </fieldset>\
               ";
        $.colorbox({
            html :	html, 
            width:	"500px",
            scrolling:	false
        });
        $(document).on("click", "#cancel", function() {
            $.colorbox.close();
            return false;
        });
        $(document).on("click", "#print", function() {
            var rType = $("input[name=rType]:radio:checked").val();
            $.colorbox.close();
            var url = "{$_subdomain}/Job/index/?rJobId=" + {$JobID} + "&rType=" + rType;
            $("#print").die();
            location.href = url;
            window.focus();
            return false;
        });
    }
</script>

<style>
fieldset { position: relative; }
.top-right { position: absolute !important; top: 17px; right:5px; }
.top-right a { vertical-align: top; }
.bottom-right { position: absolute !important; bottom: 17px; right:5px; }
input[type="text"] { text-transform: uppercase; }
.hidden{ display:none !important;}
#cboxTitle{ position:absolute; top: auto; bottom:25px; left:0; text-align:center; width:100%; color:#949494; }
.alignCentre{ text-align: center !important; }
</style>

{* hide from Internet Explorer... *}
<!--[if !IE]><!-->
<style>
.AndrewsComboBox { width:25px !important; }
</style>
<!--<![endif]-->

{* hide from IE 6... *}
<!--[if gte IE 7]>
<style>
.AndrewsComboBox { width:25px !important; }
</style>
<![endif]-->

{* include for IE 6 only... *}
<!--[if IE 6]>
<style>
.AndrewsComboBox {}
</style>
<![endif]-->

{/block}


{block name=body}

<div class="breadcrumb">
    <div>
	{if isset($ref) && $ref != ""}
            {if $ref == "inStore" || $ref == "withSupplier" || $ref == "awaitingCollection" || $ref == "customerNotified" || $ref == "authRequired" || $ref == "responseRequired" || $ref == "appraisalRequired"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Open Jobs</a> / &nbsp;Job Update
            {else if $ref == "raJobs"}
		{if isset($params)}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs/{$params}">RA Jobs</a> / &nbsp;Job Update
		{else}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs">RA Jobs</a> / &nbsp;Job Update
		{/if}
	    {else if $ref == "openJobs"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/openJobs{if $smarty.session.openjob.ojvBy == 'd'}/ojvBy=d{/if}">Open Jobs</a> / &nbsp;Job Update
	    {else if $ref == "overdueJobs"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/overdueJobs{if $smarty.session.overduejob.ojvBy == 'd'}/ojvBy=d{/if}">Overdue Jobs</a> / &nbsp;Job Update
	    {else if $ref == "closedJobs"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/closedJobs{if $smarty.session.closedjobs.ojvBy == 'd'}/ojvBy=d{/if}">Closed Jobs</a> / &nbsp;Job Update
	    {else if $ref == "jobSearch"}
		<a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/jobsearch{$searchArgs}">Job Search</a> / &nbsp;Job Update
	    {else if $ref == "wallboard"}
		<a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/AppointmentDiary/wallboard/{$params}">Wallboard</a> / &nbsp;Job Update
	    {else}    
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Overdue Jobs</a> / &nbsp;Job Update
            {/if}
	{else}
	    <a href="{$_subdomain}/index/index">Home Page</a> / &nbsp;Job Update
	{/if}
    </div>
</div>
    
<div id="menu">
    
    {if $superadmin}
    
	{menu menu_items = [
	    [$OutstandingJobsPage, "Add&nbsp;Contact&nbsp;Note", "ONCLICK", "first", "$('#addContactHistory').trigger('click')"],
	    [$OverdueJobsPage, "Send&nbsp;email", "/index/sendemail/$JobID", ""],
	    [$PerformancePage, "Send&nbsp;SMS", "/index/sendsms/$JobID", ""],
	    [$LinksPage, "Cancel&nbsp;Job", "/index/canceljob/$JobID", ""],
	    [$HomePage, "Other&nbsp;Actions", "/index/otheractions/$JobID", ""]
	]}

    {else}
	
        {foreach $jobMenuItems as $jmkey => $jmvalue}
            
            {if $jobMenuItems.$jmkey.2 == "ONCLICK"}
              {$jobMenuItems.$jmkey.3 = "onclick="|cat:$jobMenuItems.$jmkey.4}
            {else}
              {$jobMenuItems.$jmkey.2 = $jobMenuItems.$jmkey.2|cat:"/"|cat:$JobID}
            {/if}
              
        {/foreach}
	
	{menu menu_items = $jobMenuItems}
	
    {/if}
			
    <button type="button" id="printRoutines">
	<span class="label">Print Routines</span>
    </button>
    
</div> 
     

<div class="main" id="jobdetails">
                                    
    <h2 class="span-8" style="margin-bottom:0px;">Date Booked: {$DateBooked|date_format: "%d/%m/%Y"}&nbsp;&nbsp;&nbsp;&nbsp;{$TimeBooked|date_format: "%H:%M"}</h2>
    <h2 class="span-8" style="margin-bottom:0px; text-align: center;">{$ContactLastName}, {$ContactTitle} {$ContactFirstName}</h2>
    <h2 class="span-8 last" style="text-align:right; margin-bottom:0px;"><span style="font-size:small;">Skyline No:</span> {$JobID}<br />
                                                       <span style="font-size:small;">RMA Number:</span> {$RMANumber}</h2>
    
    <div class="span-24">
        <form id="CustomerDetails" name="CustomerDetails">
        <fieldset>
            <legend>Customer Details</legend>
            <span class="top-right" style="width:88px;" >
                {*<a class="hide save" title="" href="#CustomerDetails">Save</a>*}
                {if $FullAddress neq ''}
                <a href="#" class="customermap" title="View Map"><img src="{$_subdomain}/images/icon_map_35px.png" /></a>
                {/if}
                
                <a class="colorbox" title="Edit Customer Details" href="{$_subdomain}/Popup/editCustomerDetails/{$CustomerID}">Edit</a>{*#CustomerDetails*}
            </span>
            <p class="address"><strong>Home Address:</strong> {$FullAddress|strtoupper}</p>
            <p class="contact-info">
                
                {if $ContactHomePhone != ""}
                   <span style="margin-right:50px;"> <strong>(T)</strong> {$ContactHomePhone}&nbsp;&nbsp;{if $ContactWorkPhoneExt != ""}<strong>(EX)</strong> {$ContactWorkPhoneExt}{/if}</span>
                {/if}
                
                {if $ContactMobile != ""}
                   <span style="margin-right:50px;"> <strong>(M)</strong> {$ContactMobile} </span>
                {/if}
                
                {if $ContactAltMobile != "" && $SendServiceCompletionText == "ALTERNATIVEMOBILENO"}
                    <span style="margin-right:50px;"> <strong>(SMS)</strong> {$ContactAltMobile} </span>
                {/if}
                
                {if $ContactEmail != ""}
                     <span style="margin-right:50px;"> <strong>(E)</strong> {$ContactEmail} </span>
                {/if}
		
            </p>            
        </fieldset>
        </form>
    </div>

	
    {if isset($deliveryAddress)}
	<div class="span-24">
	    <form id="deliveryAddress" name="deliveryAddress">
		<fieldset>
		    <legend>
			{*<img src="{$_subdomain}/css/Skins/{$_theme}/images/icons_skyline/icons_skyline_truck.png" />*}
			Collection/Delivery Address Details
		    </legend>
                    <span class="top-right" style="width:88px" >
                        {if $deliveryAddress["street"] == "" && $deliveryAddress["townCity"] == "" && $deliveryAddress["postCode"] == ""}
                        <a id="collectionmaplink" href="#" class="collectionmap hidden" title="View Map"><img src="{$_subdomain}/images/icon_map_35px.png" /></a>
                        {else}
                        <a id="collectionmaplink" href="#" class="collectionmap" title="View Map"><img src="{$_subdomain}/images/icon_map_35px.png" /></a>
                        {/if}
                        {* DO NOT UNDER ANY CIRCUMSTANCERS use the colorbox class on the Anchor tag !!!!!
                          For whatever reason someone in the past has used the colorbox class as JQuery selector to invoke the colorbox 
                          Overlay on this tag uing the href as content.
                          ******************************************************************************************************************
                          *** Using a class called colorbox as both a css style class AND a JQuery selector is a REALLY REALLY BAD IDEA ****
                          ******************************************************************************************************************
                          I discussed this with Aida when I made the change but the conversation seems to have got lost.
                          The JQuery selector to invoke colorbox for this tag is #collectionDetailsEdit
                          DO NOT CHANGE THIS code. DO NOT USE THE CSS colorbox CLASS HERE *}
                        <a class="colorbox" id="collectionDetailsEdit" title="Edit Collection Details" href="{$_subdomain}/Courier/BookCollectionForm/job={$JobID}" >Edit</a>   {* removed app-button class and added colorbox class by thirumal *}
                    </span>
                    {*<span>
                        <a class="hide save" title="" href="#deliveryAddress">Save</a>
                        <a class="top-right colorbox" id="deliveryAddressEdit" title="Edit Customer Details" href="{$_subdomain}/Popup/editDeliveryDetails/{$JobID}">Edit</a>
                    </span>*}
		    <p id="CollectionAddress" class="address">
			{if $deliveryAddress["street"] == "" && $deliveryAddress["townCity"] == "" && $deliveryAddress["postCode"] == ""}
			    <span style="color:black;">
				{$page["Text"]["address_is_blank"]|escape:"html"}
			    </span>
			{else}
			    {if $deliveryAddress["companyName"] != ""}{$deliveryAddress["companyName"]|strtoupper}, {/if}
			    {if $deliveryAddress["buildingNameNumber"] != ""}{$deliveryAddress["buildingNameNumber"]|strtoupper} {/if}
			    {if $deliveryAddress["street"] != ""}{$deliveryAddress["street"]|strtoupper}, {/if}
			    {if $deliveryAddress["localArea"] != ""}{$deliveryAddress["localArea"]|strtoupper}, {/if}
			    {if $deliveryAddress["townCity"] != ""}{$deliveryAddress["townCity"]|strtoupper}, {/if}
			    {if $deliveryAddress["postCode"] != ""}{$deliveryAddress["postCode"]|strtoupper}{/if}
			{/if}
		    </p>                    
		    <p class="span-22 last contact-info">
			<span id="CollectionTelephone" class="span-8"><strong>(T)</strong> {$deliveryAddress["ColAddPhone"]}   {if $deliveryAddress["ColAddPhoneExt"] neq ''}&nbsp;&nbsp;<strong>(EX)</strong> {$deliveryAddress["ColAddPhoneExt"]} {/if}</span> 			 
			<span id="CollectionEmail" class="span-8 last"><strong>(E)</strong> {$deliveryAddress["ColAddEmail"]}</span>   
		    </p>
                                       
                    <br />
                    
                    <div class="span-22 last">
                        <div class="span-7">
                            <strong>{$page['Labels']['courier']|escape:'html'}</strong> <span id="CourierName">{$CourierName|upper|escape:'html'}</span>
                        </div>
                        <div class="span-7">
                            <strong>{$page['Labels']['consignment_date']|escape:'html'}</strong> <span id="ConsignmentDate">{$ConsignmentDate|upper|escape:'html'}</span>
                        </div>
                        <div class="span-8 last">
                             <strong>{$page['Labels']['consignment_no']|escape:'html'}</strong>
                            {if $CourierName eq 'DPD'}
                            <span id="ConsignmentNo"><a href="{$DPDTrackerURL|escape:'html'}{$ConsignmentNo|upper|escape:'html'}" target="_blank">{$ConsignmentNo|upper|escape:'html'}</a></span>
                            {else}
                            <span id="ConsignmentNo">{$ConsignmentNo|upper|escape:'html'}</span>
                            {/if}
                        </div>
                    </div>
		    
		</fieldset>
	    </form>
	</div>
    {/if}

	    
    {*<p>&nbsp;</p>*}

    {* This block of code is for to display message after data updation *} 

                                    
    <div id="dataUpdatedMsg" class="SystemAdminFormPane" style="display: none;" >   
    
        <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > Edit Customer Details </legend>
                <p>
                 
                    <b>{$page['Text']['customer_data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
        </form>            
    </div> 
                        
                        
                        
    <div id="accessDeniedMsg" class="SystemAdminFormPanel" style="display: none;"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$page['Errors']['access_denied_legend']|escape:'html'} </legend>
                <p>

                    <b>{$page['Errors']['access_denied_legend']|escape:'html'}</b><br><br>

                </p>

                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>


            </fieldset>   
        </form>            
    </div>                         

    {*<img src="{$_subdomain}/images/job_status.png" style="margin-bottom: 20px;" />*}

       {if $Timeline|@count gt 0}

    <div id="timeline_container" class="clearfix">
        
	{*<form id="test"></form>*}
        
	{if $ClosedDate != null}
	    <fieldset>
	    <legend>Service Provider Keynote Timeline - Customer Perception</legend>
        {else}
	    <fieldset style="padding-top: 0;">
	    <legend>Service Provider Keynote Timeline - Customer Perception</legend>
	    <div style="text-align: right; padding-right: 5px; margin-bottom: 10px; margin-top: -10px;">
                <span style="padding-right:20px;">CURRENT SYSTEM STATUS: {$StatusName|strtoupper}</span>
		{if $loggedin_user->UserType == "Admin" || isset($loggedin_user->Permissions["AP12001"])}
		    <a href="#" class="DTTT_button" id="editStatus">Edit</a>
		{/if}
	    </div>
        {/if}
        
	{foreach $Timeline as $stage}{* need to add code for first and last node*}
                 
            {if $stage@first}
            <div class="first-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage" style="margin-bottom:0px;" >{$stage['Name']}&nbsp;</p>
                <p class="date"  >{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {elseif $stage@last}
            <div class="last-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage">{$stage['Name']}&nbsp;</p>
                <p class="date">{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {else}
            <div class="node-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage">{$stage['Name']}&nbsp;</p>
                <p class="date">{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {/if}
            
        {/foreach}

        <div class="standardTAT">
                <div class="left">{$DaysFromBooking} Day(s) From Booking</div>
                <div class="centre">Standard Job Turnaround Time = {$JobTurnaroundTime} Day(s)</div>
                <div class="right">
                        
                    {if $DaysRemaining >=0}
                      {$DaysRemaining} Day(s) Remaining
                    {else}
                       {math equation="x * y" x=-1 y=$DaysRemaining} Day(s) Overdue
                    {/if}
                    
                </div>
        </div>
        <div>
            
            <div class="left turnaround-timeline{$TurnaroundTimeType}">
                <div class="value" style="width:{$TurnaroundTimeValue};">&nbsp;</div>
            </div>
           
        </div>  
        
        </fieldset>
        </form>
    </div>           
   {/if} 
   
<!-- 
    nivas start <br>
   {if $Timeline|@count gt 0}

    <div id="timeline_container" class="clearfix">
        
	{*<form id="test"></form>*}
        
	{if $ClosedDate != null}
	    <fieldset>
	    <legend>Branch Keynote Timeline - Customer Perception</legend>
        {else}
	    <fieldset style="padding-top: 0;">
	    <legend>Branch Keynote Timeline - Customer Perception</legend>
	    <div style="text-align: right; padding-right: 5px; margin-bottom: 10px; margin-top: -10px;">
                <span style="padding-right:20px;">CURRENT OPEN JOBS STATUS: {$StatusName|strtoupper}</span>
		{if $loggedin_user->UserType == "Admin" || isset($loggedin_user->Permissions["AP12001"])}
		    <a href="#" class="DTTT_button" id="editStatus">Edit</a>
		{/if}
	    </div>
        {/if}
        
	{foreach $Timeline as $stage}{* need to add code for first and last node*}
                 
            {if $stage@first}
            <div class="first-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage" style="margin-bottom:0px;" >{$stage['Name']}&nbsp;</p>
                <p class="date"  >{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {elseif $stage@last}
            <div class="last-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage">{$stage['Name']}&nbsp;</p>
                <p class="date">{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {else}
            <div class="node-{$stage@total} {$stage['Tense']}{$TurnaroundTimeType}">
                <p class="stage">{$stage['Name']}&nbsp;</p>
                <p class="date">{$stage['Time']}<br />{$stage['Date']} &nbsp;</p>     
            </div> 
            {/if}
            
        {/foreach}

        <div class="standardTAT">
                <div class="left">{$DaysFromBooking} Day(s) From Booking</div>
                <div class="centre">Standard Job Turnaround Time = {$JobTurnaroundTime} Day(s)</div>
                <div class="right">
                        
                    {if $DaysRemaining >=0}
                      {$DaysRemaining} Day(s) Remaining
                    {else}
                       {math equation="x * y" x=-1 y=$DaysRemaining} Day(s) Overdue
                    {/if}
                    
                </div>
        </div>
        <div>
            
            <div class="left turnaround-timeline{$TurnaroundTimeType}">
                <div class="value" style="width:{$TurnaroundTimeValue};">&nbsp;</div>
            </div>
           
        </div>  
        
        </fieldset>
        </form>
    </div>           
   {/if}        
    nivas end -->         
    <div id="EQ1" class="span-24">
        
        <div class="span-8">
            <div id="ProductDetailsPanel" >
            <form id="ProductDetails" name="ProductDetails" method="post" action="#">
            <fieldset>
                <legend>Product Details</legend>
                <div id="ProductDetailsFormPanel" style="margin-bottom: 20px;">
                    <div style="height: 100%;">
                        <p class="error" style="display:none;"></p>
                        
                        <input type="hidden" id="ModelNumber" name="ModelNumber" value="{$ModelNumber}">
                        <input type="hidden" id="UnitTypeID" name="UnitTypeID" value="{$UnitTypeID}">
                        <input type="hidden" id="ModelID" name="ModelID" value="{$ProductDetails.ModelID}">
                
                        {if $EnableProductDatabase|upper =='YES'}
                        <p>
                            <label>Catalogue No:</label>
                            <span class="fieldValue">{$ProductNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="ProductNo" value="{$ProductNo|escape:'html'}" /></span>
                        </p> 
                        {/if}
                
                        <p>
                            <label>{$page["Labels"]["model_no"]}:</label>
                            <span class="fieldValue" id="JobModelNumberSpan">{$ModelNumber|default:$service_base_model|upper|escape:'html'}</span>
                            {* <span class="fieldInput"><textarea id="ModelDescription" name="ModelDescription">{$ModelDescription|default: $service_base_model}</textarea></span> *}
                            <span class="fieldInput">
                                <input id="ServiceBaseModel" name="ServiceBaseModel" value="{$ModelNumber|default:$service_base_model|upper|escape:'html'}" />
                            </span>
                        </p>
                                
                        <p>
                            <label>Manufacturer:</label>
                            <span class="fieldValue">{$ManufacturerName|default:$service_base_manufacturer|upper|escape:'html'}</span>
                            <span class="fieldInput">
                                <input id="ServiceBaseManufacturer" name="ServiceBaseManufacturer" type="text" value="{$ManufacturerName|default:$service_base_manufacturer|upper|escape:'html'}" style="width:94px;">
                                 <select name="ManufacturerID" id="ManufacturerID" class="text AndrewsComboBox">
                                     <option value=""></option>
                                    {foreach $manufacturerList as $manufacturer}
                                    {if $ProductDetails.ManufacturerID eq $manufacturer.ManufacturerID}
                                    <option value="{$manufacturer.ManufacturerID}" selected="selected">{$manufacturer.ManufacturerName|upper|escape:'html'}</option>
                                    {else}
                                    <option value="{$manufacturer.ManufacturerID}">{$manufacturer.ManufacturerName|upper|escape:'html'}</option>
                                    {/if}
                                    {/foreach}
                                </select> 
                            </span> 
                        </p>                
                
                        <p>
                            <label>Product Type:</label>
{*                            <span class="fieldValue">{$service_base_unit_type}</span>*}
                            <span class="fieldValue">{$UnitTypeName}</span>
                            <span class="fieldInput">
{*                                <input id="ServiceBaseUnitType" name="ServiceBaseUnitType" id="ServiceBaseUnitType" type="text" value="{$service_base_unit_type}" style="width:94px;">*}
                                <input id="ServiceBaseUnitType" name="ServiceBaseUnitType" id="ServiceBaseUnitType" type="text" value="{$UnitTypeName}" style="width:94px;">
                                <select name="ProductTypeID" id="ProductTypeID" class="text AndrewsComboBox">
                                    <option value="1" {if $UnitTypeID eq ''}selected="selected"{/if}>UNKNOWN</option>
                                    {foreach $productTypeList as $unit_type}
					{if $unit_type.UnitTypeID eq $UnitTypeID}
					    <option value="{$unit_type.UnitTypeID}" selected="selected">{$unit_type.UnitTypeName|escape:'html'}</option>
					{else}
					    <option value="{$unit_type.UnitTypeID}">{$unit_type.UnitTypeName|escape:'html'}</option>
					{/if}
                                    {/foreach}
                                </select>                     
                            </span>     
                        </p>               
                
                        <!--input id="ServiceBaseManufacturer" name="ServiceBaseManufacturer" type="hidden" value="{$ServiceBaseManufacturer}"-->
                        <p>
                            <label>Job Type:</label>
                            <span class="fieldValue">{$ProductDetails.RepairType|strtoupper}</span>
                            <span class="fieldInput">
                                <select name="RepairType" id="RepairType" class="text combobox" >
                                    <option value=""></option>
                                    <option value="customer" {if $ProductDetails.RepairType eq "customer"} selected="selected"{/if}>CUSTOMER</option>
                                    <option value="stock" {if $ProductDetails.RepairType eq "stock"} selected="selected"{/if}>STOCK</option>
                                </select>
                            </span> 
                        </p>

                        <p>
                            <label>Service Type:</label>
                            <span class="fieldValue">{$ProductDetails.ServiceTypeName|trim|strtoupper}</span>
                            
                            
                            
                            
                            <span id="ServiceTypeInput" class="fieldInput"   >
                                <select name="ServiceType" id="ServiceType" class="text combobox">
                                    
                                    {if $ProductDetails.Mask}
                                        
                                        <option value="-1" >{$ProductDetails.ServiceTypeName|trim|strtoupper}&nbsp;</option>
                                    
                                    {else}
                                    
                                        <option value="" ></option>
                                        
                                    {/if}
                                    
				    {foreach $serviceTypes as $serviceType}
					<option value="{$serviceType["ServiceTypeID"]}" 
					    {if $ProductDetails.ServiceTypeName|trim|strtoupper==$serviceType["Name"]|strtoupper}
						selected="selected"
					    {/if}>
					    {$serviceType["Name"]|strtoupper}
					</option>
				    {/foreach}
                                </select><br>
                                <span style="float:right;margin-right:16px;" >
                                <input style="padding:0px;margin:0px;width:10px;" type="checkbox" name="ServiceTypeEditable" value="1" > Edit
                                </span>
                            </span> 
                                
                        </p>

			{if $imeiRequired}
			    <p>
				<label>IMEI No:</label>
				<span class="fieldValue">{$ImeiNo|escape:'html'}</span>
				<span class="fieldInput"><input type="text" name="ImeiNo" value="{$ImeiNo|escape:'html'}" /></span>
			    </p> 
			{/if}
			
                        <p>
                            <label>Serial No:</label>
                            <span class="fieldValue">{$SerialNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="SerialNo" value="{$SerialNo|escape:'html'}" /></span>
                        </p> 
                
                        <p>
                            <label>Item Location:</label>
                            <span class="fieldValue">{if $ProductLocation=="Branch" || $ProductLocation=="BRANCH"}BRANCH ADDRESS{else}{$ProductLocation}{/if}</span>
                            <span class="fieldInput">
                                <select name="PhysicalLocation" id="PhysicalLocation" class="text combobox" >
                                    <option value=""></option>
                                    <option value="Customer" {if $ProductLocation=="CUSTOMER"} selected="selected"{/if}>CUSTOMER</option>
                                    <option value="Branch" {if $ProductLocation=="BRANCH"} selected="selected"{/if}>BRANCH ADDRESS</option>
                                    <option value="Service Provider" {if $ProductLocation=="SERVICE PROVIDER"} selected="selected"{/if}>SERVICE PROVIDER</option>
                                    <option value="Supplier" {if $ProductLocation=="STORE"} selected="selected"{/if}>SUPPLIER</option>
                                </select>
                            </span> 
                        </p>
                
                        <p>
                            <label>Purchase Date:</label>
                            <span class="fieldValue">{$DateOfPurchase|date_format:"%d/%m/%Y"}</span>
                            <span class="fieldInput"><input type="text" id="PurchaseDate_1" name="PurchaseDate_1" value="{$DateOfPurchase|date_format:'%d/%m/%Y'}" /></span>
                        </p>               

                        <p>
                            <label>Original Retailer:</label>
                            <span class="fieldValue">{$OriginalRetailer|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="OriginalRetailer" value="{$OriginalRetailer|escape:'html'}" /></span>
                        </p> 
                
                        <p>
                            <label>Sales Receipt No:</label>
                            <span class="fieldValue">{$ReceiptNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="ReceiptNo" value="{$ReceiptNo|escape:'html'}" /></span>
                        </p>   
                    </div>
                        
                    <p style="text-align: right; margin-bottom: 0;">
                        <a class="hide inline-save" title="" href="#ProductDetails">Save</a>
                        <a class="hide inline-cancel" title="" href="#ProductDetails">Cancel</a>
                        <a class="inline-edit" title="" id="productDetailsEdit" href="#ProductDetails">Edit</a>
                    </p>
                    
                </div>
            </fieldset>
            </form>
            </div>
        </div>
                        
        <div class="span-8">
            <div id="AdditionalPanels"> 
            <form id="AdditionalInformation" name="AdditionalInformation" method="post" action="#">
            <fieldset>
                <legend>Additional Information</legend>
                <div id="AdditionalInformationFormPanel">
                    
                        <p class="error" style="display:none;"></p>
                        <p>
                            <label>Insurer:</label>
                            <span class="fieldValue">{$AdditionalInformation.Insurer|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="Insurer" value="{$AdditionalInformation.Insurer}" /></span>
                        </p>
                        <p>
                            <label>{$page['Labels']['policy_no']|escape:'html'}:</label>
                            <span class="fieldValue">{$AdditionalInformation.PolicyNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="PolicyNo" value="{$AdditionalInformation.PolicyNo}" /></span>
                        </p>
			
			<p>
			    <label>{$page['Labels']['authorisation_no']|escape:'html'}:</label>
			    <span class="fieldValue">{$AdditionalInformation.AuthorisationNo|escape:'html'}</span>
			    {if isset($loggedin_user->Permissions["AP7036"]) || $loggedin_user->UserType == "Admin"}
				<span class="fieldInput"><input type="text" name="AuthorisationNo" value="{$AdditionalInformation.AuthorisationNo}" /></span>
			    {/if}
			</p>
			
                        <p>
                            <label>{$page['Labels']['referral_no']|escape:'html'}:</label>
                            <span class="fieldValue">{$AdditionalInformation.BrandReferralNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="BrandReferralNo" value="{$AdditionalInformation.BrandReferralNo}" /></span>
                        </p>
                        {if $AdditionalInformation.childJob neq ""}
                        <p>
                            <label>{$page['Labels']['replicate_job_to']|escape:'html'}:</label>
                            <span class="fieldValue">{$AdditionalInformation.childJob}</span>
                        </p>
                        {/if}
                        {if $AdditionalInformation.parentJob neq ""}
                        <p>
                            <label style="width: 145px;">{$page['Labels']['replicate_job_from']|escape:'html'}:</label>
                            <span class="fieldValue">{$AdditionalInformation.parentJob}</span>
                        </p>
                        {/if}
<!-- 
                        <p>
                            <label>{$page['Labels']['courier']|escape:'html'}:</label>
                            <span class="fieldValue">{$CourierName|escape:'html'}</span>
                            {*<span class="fieldInput"><input type="text" name="CourierID" value="{$CourierID}" /></span>*}
                            <span class="fieldInput">
                                <select name="CourierID" id="CourierID" class="text combobox" >
                                    <option value=""></option>
				    {foreach $couriers as $courier}
					<option value="{$courier.CourierID}" {if $courier.CourierID == $CourierID}selected="selected"{/if}>{$courier.CourierName}</option>
				    {/foreach}
                                </select>
                            </span> 
                        </p>
			
                        <p>
                            <label>{$page['Labels']['consignment_no']|escape:'html'}:</label>
                            <span class="fieldValue">{$ConsignmentNo|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="ConsignmentNo" value="{$ConsignmentNo}" /></span>
                        </p>
			
                        <p>
                            <label>{$page['Labels']['consignment_date']|escape:'html'}:</label>
                            <span class="fieldValue">{$ConsignmentDate|escape:'html'}</span>
                            <span class="fieldInput"><input type="text" name="ConsignmentDate" value="{$ConsignmentDate}" /></span>
                        </p> 
-->
			
                        <p>&nbsp;</p>
                        
                      <p style="text-align: right; margin-bottom: 0px; margin-top: 10px;">
                        <a class="hide inline-save" title="" href="#AdditionalInformation">Save</a>
                        <a class="hide inline-cancel" title="" href="#AdditionalInformation">Cancel</a>
                        <a class="inline-edit" title="" href="#AdditionalInformation">Edit</a>
                    </p>
                </div>
            </fieldset>
            </form>
              
            <br />
                        
            <form id="AdditionalNotes" name="AdditionalNotes" method="post" action="#">
            <fieldset>
                <legend>Additional Notes</legend>
                <div id="AdditionalNotesFormPanel" style="margin-bottom: 20px;">
                    <div style="height: 100%;">
                        <p class="error" style="display:none;"></p>
                        <p>
                            <span class="memoValue">{$Notes|escape:'html'}</span>
                            <span class="memoInput"><textarea name="Notes">{$Notes|escape:'html'}</textarea></span>
                        </p>
                    </div>
                    <p style="text-align: right; margin-bottom: 0;">
                        <a class="inline-save hide" title="" href="#AdditionalNotes">Save</a>
                        <a class="inline-cancel hide" title="" href="#AdditionalNotes">Cancel</a>
                        <a class="inline-edit" title="" href="#AdditionalNotes">Edit</a>
                    </p>
                </div>
            </fieldset>
            </form>                    
            </div>
                        
        </div>
                        
        <div class="span-8 last">
            <div id="IssueReportPanels">
                
            <form id="BookedBy" name="BookedBy" method="post" action="#">
            <fieldset>
                <legend>Booked By</legend>
                <div id="BookedByFormPanel" >
                        <p>
                            <label>Network:</label>
                            <span class="fieldValue">{$Network|default: 'Not Assigned'|escape:'html'}</span>
                        </p>
                        {if $Client ne $Network}
                        <p>
                            <label>Client:</label>
                            <span class="fieldValue">{$Client|default: 'Not Assigned'|escape:'html'}</span>
                        </p>
                        {/if}
                        {if $Brand ne $Client}
                        <p>
                            <label>Brand:</label>
                            <span class="fieldValue">{$Brand|default: 'Not Assigned'|escape:'html'}</span>
                        </p>
                        {/if}
                        {if $Branch ne $Brand}
                        <p>
                            <label>Branch:</label>
                            <span class="fieldValue">{$Branch|default: 'Not Assigned'|escape:'html'}</span>
                        </p>
                        {/if}
                        {if $UserServiceProviderID eq null}
                        <p>
                            <label>User:</label>
                            {* 20/03/2013 this change requested by Joe. In his opinion this is a better solution than changing the username *}
                            {if $User eq 'SW4NN'}
                            <span class="fieldValue">Swann</span>
                            {else}
                            <span class="fieldValue">{$UserFullName|default: 'Not Assigned'|escape:'html'}</span>
                            {/if}
                        </p>
                        {/if}
                        
                        
                </div>
                   <p class="clear" style="text-align: right; margin-bottom: 0;">
                            <a class="colorbox" href="{$_subdomain}/Popup/viewBookedBy/{$JobID}">View</a>
                   </p>
            </fieldset>
            </form>
              
            <br />
            
            <form id="IssueReport" name="IssueReport_form" method="post" action="#">
            <fieldset>
                <legend>Issue Report</legend>
                <div id="IssueReportFormPanel" style="margin-bottom: 20px;">
                    <div style="height: 100%;">
                        <p class="error" style="display:none;"></p>
                        <p>
                            <span class="memoValue">{$ReportedFault|escape:'html'}</span>
                            <span class="memoInput"><textarea name="IssueReport">{$ReportedFault|escape:'html'}</textarea></span>
                        </p>
                    </div>
                    <p style="text-align: right; margin-bottom: 0;">
                        <a class="inline-save hide" title="" href="#IssueReport">Save</a>
                        <a class="inline-cancel hide" title="" href="#IssueReport">Cancel</a>
                        <a class="inline-edit" title="" href="#IssueReport">Edit</a>
                    </p>
                </div>
            </fieldset>
            </form>
            </div>
        </div>
                  
    </div>
    
                        
    {if $ProductLocation eq 'BRANCH' || $ProductLocation eq 'SERVICE PROVIDER'}
        
    <div id="accessoriesWrapper" style="width:100%; margin:0px 0px 5px 0px; padding:0; display:block; position:relative; float:left; clear:both;">
	<fieldset>
	    <legend>{$page["Labels"]["accessories"]|escape:'html'}</legend>
	    <p id="accessoriesP" style="margin:0px;">{$Accessories}</p>
	    <textarea id="accessoriesTextarea" style="display:none; width:100%; height:55px; margin:10px 0px 5px 0px;">{$Accessories}</textarea>
	  {*  <div id="accessoriesButtons">
		<a href="#" id="accessoriesCancel" class="btnCancel" style="width:38px; display:none; float:right; margin-right:0;">Cancel</a>
		<a href="#" id="accessoriesSave" class="btnConfirm" style="width:38px; display:none; float:right;margin-right: 10px;">Save</a>
		<a href="#" id="accessoriesEdit" class="btnStandard" style="width:38px; display:block; float:right; margin-right:0;">Edit</a>
	    </div> *}
	</fieldset>
    </div>
	    
    <div id="conditionWrapper" style="width:100%; margin:0px 0px 5px 0px; padding:0; display:block; position:relative; float:left; clear:both;">
	<fieldset>
	    <legend>{$page["Labels"]["condition"]|escape:'html'}</legend>
	    <p id="conditionP" style="margin:0px;">{$UnitCondition}</p>
	    <textarea id="conditionTextarea" style="display:none; width:100%; height:55px; margin:10px 0px 5px 0px;">{$UnitCondition}</textarea>
	    <div id="accessoriesButtons">
		<a href="#" id="conditionCancel" class="btnCancel" style="width:38px; display:none; float:right; margin-right:0;">Cancel</a>
		<a href="#" id="conditionSave" class="btnConfirm" style="width:38px; display:none; float:right;margin-right: 10px;">Save</a>
		<a href="#" id="conditionEdit" class="btnStandard" style="width:38px; display:block; float:right; margin-right:0;">Edit</a>
	    </div>
	</fieldset>
    </div>
            
    {/if}        

    {*
    <div id="conditionWrapper" style="width:100%; margin:0px 0px 5px 0px; padding:0; display:block; position:relative; float:left; clear:both;">
	<fieldset>
	    <legend>{$page["Labels"]["condition"]|escape:'html'}</legend>
	    <p id="conditionP">{$UnitCondition}</p>
	</fieldset>
    </div>
    *}
			
    <div class="span-24" style="margin-top:10px;">
        <h2 class="span-8">Job Details</h2>
        <h2 class="span-8" style="text-align: center;">{$ServiceCentreName}</h2>
        <h2 class="span-8 last" style="text-align: right;">Job No : {$ServiceCentreJobNo}</h2>
    </div>
                                    
    <div class="span-24" >
        <form id="ServiceCentre" name="ServiceCentre">
        <fieldset>
            <legend >Service Centre</legend>
            {if $ServiceCentreAddress ne ''} 
            <a class="top-right colorbox" href="{$_subdomain}/Popup/viewServiceCentre/{$ServiceProviderID}">View</a>
            <p class="span-22 last address"> 
                {$ServiceCentreAddress} 
            </p>
            <p class="span-22 last contact-info">
                <span class="span-3"> <strong>(T)</strong> {$ServiceCentreTelephone} </span>
                <span class="span-9"> <strong>(E)</strong> {$ServiceCentreEmail} </span>  
                <span class="span-10 last">
                    <span class="span-2" style="text-align:right;padding:0;">Contact{if $ServiceCentreContacts|@count gt 1}s{/if}:</span>
                    <span class="span-8 last" style="vertical-align:top;padding:0;">
                   {foreach $ServiceCentreContacts as $contact}
                   {$contact.Title|strtoupper}&nbsp;{$contact.Name|strtoupper}<br />
                   {/foreach}
                   </span>
                </span>
            </p>
           
            {else}
            <p style="color: #0189CD; font-size: 16px; font-weight: bold;">SERVICE PROVIDER NOT ALLOCATED</p>    
            {/if}
        </fieldset>
        </form>
    </div>
        
      <div class="span-24">
            <form id="ServiceReport">
            <fieldset>
                <legend title="">Service Report</legend>
                <div id="ServiceReportPanel" >
                    {if $AP7025}
                    <div style="height: 100%;">
                        <p class="error" style="display:none;"></p>
                        <p>
                            <span class="memoValue">{$RepairDescription}</span>
                            <span class="memoInput"><textarea name="ServiceReport">{$RepairDescription}</textarea></span>
                        </p>
                    </div>
                    <p  style="text-align: right; margin-bottom: 5px; margin-top: 5px; vertical-align:bottom;" >
                        <a class="hide inline-save" title="" href="#ServiceReport">Save</a>
                        <a class="inline-cancel hide" title="" href="#ServiceReport">Cancel</a>
                        <a class="inline-edit" title="" href="#ServiceReport">Edit</a>                
                    </p>
                    {/if}
                </div>
            </fieldset>
            </form>
        </div>   
        
        
    
    <div id="EQ2" class="span-24">

        <div class="span-16">
            <form id="Appointments">
            <fieldset style="margin-left: 0; margin-right: 0; padding-left: 5px; padding-right: 5px;">     
                <legend>Appointments</legend>               
                <div id="AppointmentsPanel" style="margin-bottom: 20px;">
                {if $AP7023}
                <table id="AppointmentsDT" class="browse" style="margin-bottom: 5px;">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="date" >Date</th>
                            <th class="time" >Timeslot</th>
                            <th style="width:80px;" > Appointment Type</th>
                            <th >Appointment Notes </th>
                            <th class="user">Engineer</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>               
                </table> 
                
                {if $AP11005}
                    <div class="bottomButtonsPanel" >

                    </div>
                {/if}    
                
                {/if}
                </div>
                {* 
                <p class="clear" style="text-align: right; margin-bottom: 0;">
                    <a class="colorbox" id="addAppointment" title="Add Appointment" href="{$_subdomain}/Popup/addappointment/{$JobID}">Add</a>
                </p>
                *}
            </fieldset>
            </form>
        </div>
            
            
        <div class="span-8 last">
            <form id="InvoiceCosts">
            <fieldset>
                <legend>Invoice Costs</legend>
                <div id="InvoiceCostsPanel" style="margin-bottom: 20px;">  
                    {if $AP7024}
                    {if $ServiceTypeCode == 2}
                    <div style="height: 100%;">
                        <p>
                            This Job has been booked as a Manufacturer's Warranty claim, please check all information is correct before proceeding.
                        </p>
                    </div>
                    {else}
                    <div style="height: 100%;">
                        <p class="error" style="display:none;"></p>
                        <p id="Parts">
                            <label>Parts:</label>
                            <span class="fieldValue">{$InvoiceCosts.Parts}</span>    
                            <span class="fieldInput"> <input type="text" class="ValueCorrection" name="Parts" value="{$InvoiceCosts.Parts}" /></span>                    
                        </p> 
                        <p id="Labour">
                            <label>Labour:</label>
                            <span class="fieldValue">{$InvoiceCosts.Labour}</span>   
                            <span class="fieldInput"> <input type="text" class="ValueCorrection"  name="Labour" value="{$InvoiceCosts.Labour}" /></span>                     
                        </p> 
                        <p id="Carriage">
                            <label>Carriage:</label>
                            <span class="fieldValue">{$InvoiceCosts.Carriage}</span>    
                            <span class="fieldInput"> <input type="text" class="ValueCorrection"  name="Carriage" value="{$InvoiceCosts.Carriage}" /></span>                    
                        </p> 
                        <p id="TotalNet">
                            <label>Total Net:</label>
                            <span class="fieldValue">{$InvoiceCosts.TotalNet}</span>   
                            {*<span class="fieldInput"> <input type="text" class="" name="TotalNet" value="{$InvoiceCosts.TotalNet}" /></span>*}                     
                        </p> 
                        <p id="VAT">
                            <label>VAT @ {$InvoiceCosts.VATRate}%:</label>
                            <span class="fieldValue">{$InvoiceCosts.VAT}</span>    
                            {* <span class="fieldInput"> 
                                <input type="text" class="" name="VAT" value="{$InvoiceCosts.VAT}" />     
                            </span>   *}                 
                        </p> 
                        <p id="Gross" >
                            <label>Gross:</label>
                            <span class="fieldValue">{$InvoiceCosts.Gross}</span>   
                            {* <span class="fieldInput"> <input type="text" class="" name="Gross" value="{$InvoiceCosts.Gross}" /></span> *}                  
                        </p> 
                        {* <p id="DepositPaid">
                            <label>Deposit Paid:</label>
                            <span class="fieldValue">{$InvoiceCosts.DepositPaid}</span>    
                            <span class="fieldInput"> <input type="text" class="" name="DepositPaid" value="{$InvoiceCosts.DepositPaid}" /></span>                    
                        </p> 
                        <p id="BalanceDue">
                            <label>Balance Due:</label>
                            <span class="fieldValue">{$InvoiceCosts.BalanceDue}</span>   
                            <span class="fieldInput"> <input type="text" class="" name="BalanceDue" value="{$InvoiceCosts.BalanceDue}" /></span>                     
                        </p> 
                        <p id="DiscountAllowed">
                            <label>Discount Allowed @ {$InvoiceCosts.DiscountRate}%:</label>
                            <span class="fieldValue">{$InvoiceCosts.DiscountAllowed}</span>    
                            <span class="fieldInput"><input type="text" class="" disabled="disabled" name="DiscountAllowed" value="{$InvoiceCosts.DiscountAllowed}" /></span>                    
                        </p> 
                        <p id="discount-type">
                            <label>Discount Type:</label>
                            <input class="radio" type="radio" name="DiscountType" value="value" />Value 
                            <input class="radio" type="radio" name="DiscountType" value="pecentage" checked="checked" />
                            <input type="text" id="DiscountAllowedRate" name="DiscountAllowedRate" value="{$InvoiceCosts.DiscountRate}" size="2" />%
                        </p>
                        <p id="RevisedBalanceDue">
                            <label>Revised Balance Due:</label>
                            <span class="fieldValue">{$InvoiceCosts.RevisedBalanceDue}</span>   
                            <span class="fieldInput"> <input type="text" class="" name="BalanceDue" value="{$InvoiceCosts.RevisedBalanceDue}" /></span>                     
                        </p>                     
                        <p id="PaymentType">
                            <label>Payment Type:</label>
                            <span class="fieldValue">{$InvoiceCosts.PaymentType}</span>   
                            <span class="fieldInput">
                                <select name="PaymentType" id="PaymentType" class="text combobox">
                                    {foreach $InvoiceCosts.PaymentTypeList as $type}
                                    <option value="{$type['PaymentTypeID']}" {if $InvoiceCosts.PaymentType == $type['PaymentTypeID']}checked="checked"{/if}>{$type['PaymentTypeName']}</option>
                                    {/foreach}
                                </select>
                            </span>                    
                        </p> *}             
                    </div>
                    <p style="text-align: right; margin-bottom: 0;">
                        <a class="hide inline-save" title="" href="#InvoiceCosts">Save</a>
                        <a class="inline-cancel hide" title="" href="#InvoiceCosts">Cancel</a>
                        <a class="inline-edit" title="" href="#InvoiceCosts">Edit</a>
                    </p>
                </div>
                {/if}
                {/if}
            </fieldset>
            </form>
            
        </div>  
            
            
                

    </div>
      
		
      {*include file="include/partsused.tpl" *}

    
    {if $AP7027}
    <div id="EQ4" class="span-24">
        <div class="head"><div class="icon_c">&nbsp;</div>Contact History Notes (<span id="NumContactHistoryRows">{$NumContactHistory}</span>)</div>
        <div class="show">
            <table id="ContactHistory" class="browse">
                <thead>
                    <tr>
                        <th class="date">Date</th>
                        <th class="time">Time</th>
                        <th class="user">User</th>
                        <th class="type">Type</th>
                        <th class="subject">Subject</th>
                        <th class="note">Note</th>
			<th>Private</th>
                    </tr>
                </thead>
                <tbody>
                    <tr> <td colspan="7"> No records found </td> </tr>
                </tbody>
            </table>
            {* <div id="dataTables_command"><button id="addContactHistory"></button></div> *}
        </div>
    </div>
    {/if}

    
   
    
    
    {if $AP7028}
    <div id="EQ5" class="span-24">
        <div class="head"><div class="icon_c">&nbsp;</div>Status Changes ({$NumStatusChanges})</div>
        <div class="show">
            <table id="StatusChanges" class="browse">
                <thead>
                    <tr>
                        <th class="date">Date</th>
                        <th class="time">Time</th>
                        <th class="user">User</th>
                        <th class="status">Status</th>
                        <th>Additional information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr> <td colspan="5"> No records found </td> </tr>
                </tbody>
            </table>         
        </div>
    </div>   
    {/if} 
    
    
     {if isset($loggedin_user->Permissions["AP7042"]) || $loggedin_user->Username == "sa"}
	<div id="EQ9" class="span-24" style="margin-top:5px;">
	    <div class="head">
		<div class="icon_c">&nbsp;</div>
		Audit Trail (<span id="NumAuditTrailRows">{$NumAuditTrail}</span>)
	    </div>
	    <div class="show">
		<table id="AuditTrail" class="browse">
		    <thead>
			<tr>
			    <th class="date">Date</th>
			    <th class="time">Time</th>
			    <th class="user">User</th>
			    <th class="type">Field Name</th>
			    <th class="subject">Previous Entry</th>
			    <th class="subject">New Entry</th>
			</tr>
		    </thead>
		    <tbody>
			<tr>
			    <td colspan="5">No records found</td>
			</tr>
		    </tbody>
		</table>
	    </div>
	</div>
    {/if}
       
    {if isset($loggedin_user->Permissions["AP12007"]) || $loggedin_user->Username == "sa"}
	<div id="EQ10" class="span-24" style="margin-top:5px;">
	    <div class="head"><div class="icon_c">&nbsp;</div>Remote Engineer Visit History (<span id="engineerHistoryCount">{$engineerHistoryCount}</span>)</div>
	    <div class="show">
		<table id="engineerHistory" class="browse">
		    <thead>
			<tr>
			    <th>Date</th>
			    <th>Time</th>
			    <th>Engineer</th>
			    <th>Action</th>
			    <th>Additional information</th>
			    <th>Images</th>
			</tr>
		    </thead>
		    <tbody>
			<tr><td colspan="9">No records found</td></tr>
		    </tbody>
		</table> 
	    </div>
	</div>
    {/if}
    
    
    {if $AP7035}

    <div id="EQ7" class="span-24">
        <div class="head">
	    <div class="icon_c">&nbsp;</div>
	    Request Authorisation (<span id="NumRequestAuthorisation">{$NumRequestAuthorisation}</span>)
	</div>
        <div class="show">
                
	    <div width="100%" style="text-align:center">
            
		<div class="centerInfoText" id="centerInfoText" style="display:none;padding-top:10px;"></div>
                
		<form id="raJobsForm" name="raJobsForm" method="post" enctype="multipart/form-data" action="#" class="inline">
       
		    <fieldset>
			<legend title="">Authorisation Type</legend>
			<p>
			    <div class="innerFieldSet">
				{foreach $AuthorisationTypes as $at}
				    <span class="leftAlign">
				       {*<input type="radio" name="ATType" {if $RAType && $RAType == $at.Name}checked="checked"{/if} value="{$at.Name|escape:'html'}"> {$at.Value|escape:'html'}*}
				       <input type="radio" name="ATType" {if count($AuthorisationTypes) == 1}checked="checked"{/if} value="{$at.Name|escape:'html'}" /> {$at.Value|escape:'html'}
				    </span>
				{/foreach}
			    </div>
			</p>
			
			<p>&nbsp;</p>
			
			<table id="RAStatusElement" {if count($AuthorisationTypes) > 1}style="display:none;"{/if}>
			    
			    {*if isset($RAStatusName) && $RAStatusName != ""*}
				<tr id="currentRAStatus" style="{if !isset($RAStatusName) || $RAStatusName == ""}display:none;{/if}">
				    <td style="width:40%; text-align:right">Current RA Status:</td>
				    <td style="width:30%; text-align:left" id="JobStatusText">{$RAStatusName|escape:'html'}</td>
				    <td style="width:30%; text-align:left"></td>
				</tr>
			    {*/if*}
			    
			    <tr>
				<td style="width:40%; text-align:right; background-color:transparent;">Select New RA Status:</td>
				<td style="width:30%; text-align:left; background-color:transparent;" id="RAStatusIDElement">
				    <select name="RAStatusID" id="RAStatusID" class=" text combobox" style="width:200px;">
					<option value=""></option>
					{foreach $RAStatusTypesList as $rastl}
					    <option value="{$rastl.RAStatusID}" statusCode="{$rastl.RAStatusCode}">{$rastl.RAStatusName|escape:'html'}</option>
					{/foreach}
				    </select>
				</td>
				<td style="width:30%; text-align:left; background-color:transparent;"></td>
			    </tr>

			    {if isset($loggedin_user->Permissions["AP7036"]) || $loggedin_user->UserType == "Admin"}
				{if $authRequired}
				    <tr style="display:none;" id="authNoTr">
					<td style="width:40%; text-align:right; background-color:transparent;">Authorisation No:</td>
					<td style="width:30%; text-align:left; background-color:transparent;" id="authNoTd">
					    <input type="text" class="text" style="width:390px;" name="authNo" value="" id="authNo">
					</td>
				    </tr>
				{/if}
			    {/if}
			    <tr id="raNotesTr">
				<td style="width:40%;text-align:right;background-color:transparent;">Notes:</td>
				<td style="width:30%;text-align:left;background-color:transparent;" id="Notes">
				    {*<input type="text" class="text" style="width:390px;" name="RANotes" value="" id="RANotes">*}
				    <textarea class="text" style="width:400px; height:30px; min-height:30px; max-height:200px; resize:vertical;" name="RANotes" value="" id="RANotes"></textarea>
				</td>
			    </tr>

			    <tr id="raImageTr">
				<td style="width:40%;text-align:right;background-color:transparent;">Image:</td>
				<td style="width:30%;text-align:left;background-color:transparent;" id="raImageTd">
				    <input type="file" name="raImage" id="raImage">
				</td>
			    </tr>

			    <tr>
				<td style="background-color:transparent;"></td>
				<td style="width:30%; text-align:left; background-color:transparent;">
				    <a class="btnStandard" title="Save New Status" href="#EQ7" id="SaveNewStatus">Apply New RA Status</a>
				</td>
			    </tr>

			    <tr>
				<td style="width:30%; text-align:left"></td>
				<td style="width:30%; text-align:right">Current Job Status:</td>
				<td style="width:40%; text-align:left" id="JobStatusText">{$StatusName|escape:'html'}</td>
			    </tr>
					
			</table>
			    
			<input name="OldStatusID" id="OldStatusID" type="hidden" value="{$RAStatusID|escape:'html'}" />

		    </fieldset> 

		</form>
				   
		<br/>
		
		<div id="RAHistoryNotesDiv" style="{if count($AuthorisationTypes) > 1}display:none;{/if} position:relative;">
		
		    <span class="RAHistoryNotes">Request Authorisation History ({$RAHistoryNotesCount})</span>

		    <div style="position:absolute; top:10px; right:300px;">
			<input type="radio" name="raTypeFilter" value="current" /> Current
			<input type="radio" name="raTypeFilter" value="all" /> All
		    </div>
		    
		    <table id="RAHistory" class="browse">
			<thead>
			    <tr>
				<th></th>
				<th class="date">Date</th>
				<th class="time">Time</th>
				<th class="user">User</th>
				<th class="type">RA Status</th>
				<th class="note">Authorisation Type</th>
				<th class="note">Notes</th>
				<th class="note">Image</th>
			    </tr>
			</thead>
			<tbody>
			    <tr>
				<td colspan="6"> No records found </td> 
			    </tr>
			</tbody>
		    </table> 
                    
		</div>
		
                
                    <div style="display:none;">
                        <div id="ApplyNewRATypeConfirmation">

                            <p style="text-align:center">

                                If you continue to set this flag this job will be Suspended from the repair process until the Request has be Authorised  <br><br>

                                <a href="#" id="RATConfirmContinue" class="btnConfirm">Continue</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#" id="RATCancelSelection" class="btnCancel">Cancel Selection</a><br>

                            </p>

                        </div>
                        
                        
                         <div id="MultipleRATypeSelection">

                            <p style="text-align:center">

                                A job can only be allocated one Request Authorisation at a time. The current RA must be completed before your selection can be accepted.  <br><br>

                                <a href="#" id="MultipleRATypeSelectionClose" class="form-button"   >Close</a>


                            </p>

                        </div>
                        
                        
                         <div id="RATypeNoSelection"  >

                            <p style="text-align:center" >

                               This job is currently undergoing a RA Process Review.  Are you sure you wish to close this review.  <br><br>

                                <a href="#" id="RATypeNoSelectionYes" class="form-button"   >Yes</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#" id="RATypeNoSelectionNo" class="form-button"   >No</a><br>


                            </p>

                        </div>
                        
                        
                   </div>                            
                                                
            </div>
             
                      
                 
        </div>
    </div> 
                                   
                                   
                                   
                                           
                                   
                                   
    {/if}

    <div class="breadcrumb" style="margin-top:10px;">
	<div>
	    {if isset($ref) && $ref != ""}
		{if $ref == "inStore" || $ref == "withSupplier" || $ref == "awaitingCollection" || $ref == "customerNotified" || $ref == "authRequired" || $ref == "responseRequired" || $ref == "appraisalRequired"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Open Jobs</a> / &nbsp;Job Update
		{else if $ref == "raJobs"}
		    {if isset($params)}
			<a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs/{$params}">RA Jobs</a> / &nbsp;Job Update
		    {else}
			<a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs">RA Jobs</a> / &nbsp;Job Update
		    {/if}
		{else if $ref == "openJobs"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/openJobs">Open Jobs</a> / &nbsp;Job Update
		{else if $ref == "overdueJobs"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/overdueJobs">Overdue Jobs</a> / &nbsp;Job Update
		{else if $ref == "closedJobs"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/closedJobs">Closed Jobs</a> / &nbsp;Job Update
		{else if $ref == "jobSearch"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/jobsearch{$searchArgs}">Job Search</a> / &nbsp;Job Update
		{else if $ref == "wallboard"}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/AppointmentDiary/wallboard/{$params}">Wallboard</a> / &nbsp;Job Update
		{else}    
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Overdue Jobs</a> / &nbsp;Job Update
		{/if}
	    {else}
		<a href="{$_subdomain}/index/index">Home Page</a> / &nbsp;Job Update
	    {/if}
	</div>
    </div>
    
    
</div>
            <div id="ViaDiv2" style="display:none;font-size: 14px;width:300px;margin-bottom: 0px;height:150px">
     
         
        </div>  
{/block}
