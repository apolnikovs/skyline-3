<script type="text/javascript">   
$(document).ready(function(){

/* =======================================================
*
* Initialise input auto-hint functions...
*
* ======================================================= */

$('.auto-hint').focus(function() {
$this = $(this);
if ($this.val() == $this.attr('title')) {
$this.val('').removeClass('auto-hint');
if ($this.hasClass('auto-pwd')) {
    $this.prop('type','password');
}
}
} ).blur(function() {
$this = $(this);
if ($this.val() == '' && $this.attr('title') != '')  {
$this.val($this.attr('title')).addClass('auto-hint');
if ($this.hasClass('auto-pwd')) {
    $this.prop('type','text');
}
}         
} ).each(function(){
$this = $(this);
if ($this.attr('title') == '') { return; }
if ($this.val() == '') { 
if ($this.attr('type') == 'password') {
    $this.addClass('auto-pwd').prop('type','text');
}
$this.val($this.attr('title')); 
} else { 
$this.removeClass('auto-hint'); 
}
$this.attr('autocomplete','off');
} );



/* =======================================================
*
* set tab on return for input elements with form submit on auto-submit class...
*
* ======================================================= */

$('input[type=text],input[type=password]').keypress( function( e ) {
if (e.which == 13) {
$(this).blur();
if ($(this).hasClass('auto-submit')) {
    $('.auto-hint').each(function() {
        $this = $(this);
        if ($this.val() == $this.attr('title')) {
            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
            if ($this.hasClass('auto-pwd')) {
                $this.prop('type','password');
            }
        }
    } );
    $(this).get(0).form.onsubmit();
} else {
    $next = $(this).attr('tabIndex') + 1;
    $('[tabIndex="'+$next+'"]').focus();
}
return false;
}
} ); 

});
</script>



<form action="" id="editDeliveryDetails" name="editDeliveryDetails" method="post">
    
    <fieldset>      
        
	<legend title="AP7111">Edit Delivery Details</legend>
        <p class="error" style="display:none;"></p>
        
	

        <p>
	    <label for="CompanyName">{$page['Labels']['company_name']|escape:'html'}:</label>&nbsp;&nbsp;
	    <input  class="text" type="text" name="CompanyName" id="CompanyName" value="{$CompanyName|escape:'html'}" >&nbsp;
	</p>
	
        <p>
	    <label for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
	    <input  class="text uppercaseText" style="width: 130px;" type="text" name="PostalCode" id="PostalCode" value="{$PostalCode|escape:'html'}" >&nbsp;
	    <input type="button" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn" value="Click to find Address" />
	    <span id="fetch" style="display:none" class="blueText" >({$page['Text']['fetching']|escape:'html'})</span>
	</p>

        <p id="selectOutput"></p>

        <p id="jb_p_house_name"  >
            <label for="jbBuildingName" >{$page['Labels']['building_name']|escape:'html'}:</label>&nbsp;&nbsp;
             <input type="text" class="text"  name="BuildingNameNumber" value="{$BuildingNameNumber|escape:'html'}" id="jbBuildingName" >          
        </p>

        <p id="jb_p_street"  >
            <label for="jbStreet" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input type="text" class="text"  name="Street" value="{$Street|escape:'html'}" id="jbStreet" >          
        </p>

        <p id="jb_p_area"  >
            <label for="jbArea" >{$page['Labels']['area']|escape:'html'}:</label>&nbsp;&nbsp;
             <input type="text" class="text" name="LocalArea" value="{$Area|escape:'html'}" id="jbArea" >          
        </p>


        <p id="jb_p_town"  >
            <label for="jbCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
             <input type="text" class="text" name="City" value="{$City|escape:'html'}" id="jbCity" >          
        </p>

        <p id="jb_p_county"  >
            <label for="jbCounty" >{$page['Labels']['county']|escape:'html'}:</label>&nbsp;&nbsp;
            <select name="CountyID" id="jbCounty"  >
                <option value="" id="country_0_county_0" {if $CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                {foreach $Skyline->getCountriesCounties() as $country}
		    <optgroup label="{$country.Name}"  >
			{foreach $country.Counties as $county}
			    <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
			{/foreach}
		    </optgroup>
                {/foreach}
            </select>
        </p>

	
	 
            
            <p id="jb_p_country" >
               <label for="jbCountry" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
               &nbsp;&nbsp;<select name="CountryID" id="jbCountry" >
                   <option value="" {if $CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                   {foreach $Skyline->getCountriesCounties() as $country}

                       <option value="{$country.CountryID}" {if $CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                   {/foreach}

               </select>
           </p>
            
            
        
	
        
	
        <p>
           <label for="ColAddEmail">{$page['Labels']['col_email']|escape:'html'}:</label>
             &nbsp;&nbsp;<input  type="text" name="ColAddEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$ColAddEmail|escape:'html'}" id="ColAddEmail" >
            <br>
             
        </p>
        
        <p>
            <label for="ColAddPhone" >{$page['Labels']['col_phone']|escape:'html'}:</label>
            &nbsp;&nbsp;<input  type="text" class="text" style="width:184px"  name="ColAddPhone" value="{$ColAddPhone|escape:'html'}" id="ColAddPhone" >
            <input  type="text" class="text extField auto-hint" style="width:100px"  title="{$page['Text']['extension_no']|escape:'html'}" name="ColAddPhoneExt" value="{$ColAddPhoneExt|escape:'html'}" id="ColAddPhoneExt" >
            
        </p>
        
        
        
        <p class="right" >
            <button id="deliveryDetailsSave" class="gplus-blue"><span class="label">Save</span></button>
            <button class="gplus-red cancel last"><span class="label">Cancel</span></button>
        </p>
	
    </fieldset>
	    
</form>