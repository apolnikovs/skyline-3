{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']|escape:'html'}
{$PageId = $DiaryPerformancePage}
{/block}


{block name=afterJqueryUI}
   
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script type="text/javascript" src="{$_subdomain}/js/jquery.scrollTo.min.js"></script>
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
   
   <style type="text/css" >
      
    .ui-combobox-input {
         width:270px;
     }  
     
   </style>
   
{/block}



{block name=scripts}

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>   
      
    <script type="text/javascript">
      
      function updateFn($sRow) { 
            if(!isNaN($sRow[2])) {     
                document.location = '{$_subdomain}/index/jobupdate/'+ $sRow[2]+'/?ref=wallboard&params=spID={$spID}/aType={$aType}/dType={$dType}/dataSPID={$dataSPID}/openJobs={$openJobs}/appsDate={$appsDate}' ;
            }
        }
        
        
        
        /**
        *  This is used to change color of the days column.
        */
        function changeColor(nRow, aData)
        {
            
            if (parseInt(aData[1]) > parseInt(aData[10]))
            {  
                

                $('td:eq(1)', nRow).css( { "color": "#FF0000", "font-weight":"bold" } );
            }
            else
            {
                
                $('td:eq(1)', nRow).css( { "font-weight":"bold" } );
            }


        }
      
      
      
      $(document).ready(function() {
      
      
     
      $.colorbox({ 
                                                
                        html:$('#waitDiv').html(), 
                        title:"",
                        escKey: false,
                        overlayClose: false,
                        onLoad: function() {
                            $('#cboxClose').hide();
                        }

                }); 
                
      $(document).delay(0).queue(function() {
      
                
                
      
             {*   
                 //Diary Performance data table starts here...   
                $('#diaryPerformanceResults').PCCSDataTable( {

                        displayButtons: "",
                        bServerSide: false,
                        htmlTablePageId:    'diaryPerformanceResultsPanel',
                        htmlTableId:        'diaryPerformanceResults',
                        fetchDataUrl:       '{$_subdomain}/AppointmentDiary/wallboardResult',
                        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                        colorboxForceClose:false,
                        hidePaginationNorows:  true,
                        sDom:               't<"#dataTables_command">rpli',
                        aaSorting:    [[ 0, "asc" ]],
                        aoColumns: [
			{ bVisible: false, bSortable:false },
                         null
                       {foreach $dateRange as $dr}
                           
                        ,null,
                        null
                        
                       {/foreach}        
                          
                        ]

                    });
               //Diary Performance data table ends here...  
            
              *}  
                  
            
                  
              
               $(document).on('click', ".aTypeRadioClass", 
                   function() {

                        document.location.href = "{$_subdomain}/AppointmentDiary/wallboard/spID="+$("#ServiceProviderID").val()+"/aType="+urlencode($(this).val())+"/dType={$dType}"; 
                        
                     });  
                     
              
              $(document).on('click', ".dtRadioClass", 
                   function() {

                        document.location.href = "{$_subdomain}/AppointmentDiary/wallboard/spID="+$("#ServiceProviderID").val()+"/aType={$aType}/dType="+urlencode($(this).val()); 
                        
                     }); 
                    
                    
             $( "#ServiceProviderID" ).combobox( { change: function() { document.location.href="{$_subdomain}/AppointmentDiary/wallboard/spID="+$("#ServiceProviderID").val()+"/aType={$aType}/dType={$dType}"; } } );    
       
       
       
       
             {if $SuperAdmin}
         
              $(document).on('click', ".SPAutoLoginLink", 
                   function() {
                       
                        if($(this).attr('id')!='')
                        {    
                            $.post("{$_subdomain}/Login/index/",        

                                                                 { name: $(this).attr('id'), password:'5kyl1ne2002' },

                                                                 function(data){

                                                                 document.location.href = "{$_subdomain}/index";

                                                                 });
                        }
                                
                        return false;
                       
                     });
             {/if}     
     
     
     
           {if $dataSPID}     
                      
                      var $fetchUrl = '';
                      var $dataSorting  = [[ 1, "desc" ]];
                      
                      {if $openJobs}
                          
                           $fetchUrl     = '{$_subdomain}/Data/jobs/serviceProviderOpenJobs/{$dataSPID}/{$NetworkID}/sType={$sType}/aType={$aType}/dType={$dType}';
                           

                      {/if}
                        
                      {if $appsDate}
                          
                          
                          $fetchUrl = '{$_subdomain}/Data/jobs/serviceProviderAppJobs/{$dataSPID}/{$NetworkID}/{$appsDate}/sType={$sType}/aType={$aType}/dType={$dType}';
                          
                          
                      {/if}
                          
                          
                    

                             $('#WallboardJobResults').PCCSDataTable( {
                                 bAutoWidth:	    false,
                                 displayButtons:     '',
                                 permission:         'R',
                                 aoColumns: [
                                     { sWidth: '8%', sType: 'date-eu' },
                                     { sWidth: '7%',
                                       sClass: 'centre'/*,
                                       mRender: function(data, type, full) {
                                                    if(data === null || parseInt(data) < 0) {
                                                        return '&nbsp;';
                                                    }
                                                    if(parseInt(full[1]) > parseInt(full[10])) {
                                                        
                                                        if(data<10)
                                                        {
                                                            data = '0'+data;
                                                        }
                                                        
                                                        return '<b style="color: red;">' + data + '</b>';
                                                    } else {
                                                      
                                                        if(data<10)
                                                        {
                                                            data = '0'+data;
                                                        }
                                                        return '<b>' + data + '</b>';
                                                    }
                                                }*/
                                     },        
                                     { sWidth: '9%' }, 
                                     { sWidth: '14%'  },  
                                     { sWidth: '14%' }, 
                                     { sWidth: '9%' }, 
                                     { sWidth: '16%' },
                                     { sWidth: '13%' },
                                     { sWidth: '10%' },
                                     { bVisible: false },
                                     { bVisible: false }            
                                 ],
                                 htmlTableId:        'WallboardJobResults',
                                 dblclickCallbackMethod: 'updateFn',
                                 fetchDataUrl:       $fetchUrl,
                                 fnRowCallback:	    'changeColor',
                                 parentURL:          '',
                                 searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                                 passParamsType:	    '1', //1 'means' parameters in query string. Default 0; 
                                 pickDataIdList:	    new Array('#JobID'),
                                 bServerSide:false,
                                 aaSorting: $dataSorting,
                                 tooltipTitle: '{$page['Text']['table_tooltip']|escape:'html'}'
                             } );      

                      
       {/if}
     
     
        $(this).dequeue();
                                                                    
        });//set delay ends here...  
     
         $.colorbox.close();  
         
         
         $(document).on('click', '.wallboardLink', 
                                function() {
         
                        $('*').css('cursor','wait');
         
         });
         
      });
      
      
      
      
      
     {if $dataSPID}
 
        //Open jobs chart starts here..
        google.load("visualization", "1", { packages:["corechart"] });
        google.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = new google.visualization.DataTable();
            var gridLines = 0;

            var $statsResult = [
                {foreach $statsResult as $sname}  
                   ["{$sname[0]}", {$sname[1]}, {$sname[2]}, "{$sname[3]}"]{if not $sname@last},{/if} 
                {/foreach}
            ];

            data.addColumn('string', "{$page['Text']['status_types']|escape:'html'}");
            $total_rows = 0;
            $data_rows  = new Array();
            $data_rows[0] = new Array();
            $data_rows[0][0] = '';
            maxrows = 0;
            $colorsData = new Array();

            $cCnt = 1;

            for(var $i = 0; $i < $statsResult.length; $i++) {
                data.addColumn('number', $statsResult[$i][0], $statsResult[$i][2]);
                data.addColumn({ type: 'string', role: 'tooltip' }); // tooltip col.
                $total_rows += $statsResult[$i][1];       

                //$data_rows[0][$i+1] = $statsResult[$i][1];
                $data_rows[0][$cCnt++] = $statsResult[$i][1];
                $data_rows[0][$cCnt++] = $statsResult[$i][0];

                if(gridLines <  $statsResult[$i][1]) {
                    gridLines =  $statsResult[$i][1] + 1;
                    maxrows = $statsResult[$i][1];
                }

                $colorsData[$i] = "#" + $statsResult[$i][3];
            }

            data.addRows($data_rows);

            var options = {
                'chartArea': { 
                    'width': '620', 
                    'height': '80%',
                    'left' : '70' 
                },
                'legend': { 
                    'position' : 'right',
                    'textStyle' : { 'fontSize' : '9' } 
                },
                'vAxis': { 
                    title: "{$page['Text']['quantity']|escape:'html'}", 
                    titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
                    slantedText: false,
                    minValue: 0,
                    maxValue: maxrows,
                    textColor: '#ffffff',
                    gridlines: { count: gridLines }
                },
                'hAxis': { 
                    title: "{$graph_title}",
                    titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
                    slantedText: false
                },
                'colors': $colorsData      
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);

            //a click handler which grabs some values then redirects the page
            google.visualization.events.addListener(chart, 'select', function() {
                //grab a few details before redirecting
                    var selection = chart.getSelection();

                //var col = selection[0].column;
                //alert(chart.getColumnId(selection[0].column));

                var col = data.getColumnId(selection[0].column);
                //alert('The user selected ' + topping);

                document.location.href = '{$_subdomain}/AppointmentDiary/wallboard/spID={$spID}/aType={$aType}/dType={$dType}/dataSPID={$dataSPID}/openJobs={$openJobs}/appsDate={$appsDate}/sType=' + col;

                });
            }

            //Open jobs chart ends here..
        
      {/if}
     
      $(document).ready(function() {
            $.scrollTo($('.errorElement'),800);
            setInterval("location.reload(true)", 60000);
            endless();
           
        });
          function endless(){
              $('.blinker').effect("highlight",{ color: 'white'},200,function(){
              endless();
         
        });
             
                }
</script>

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map_title']|escape:'html'}</a> /
        
        {$page['Text']['page_title']|escape:'html'}
    </div>
</div>

    
    
<div class="main" id="home" >
                               
    
   
      <div class="diaryPerformancePanel" >
         <form id="diaryPerformanceForm" name="diaryPerformanceForm" method="post"  action="#" class="inline">
       
             
            <fieldset>
            <legend title="" >{$page['Text']['table_title']|escape:'html'}</legend>
             
            <p>
            
                {$page['Text']['page_text']|escape:'html'}
                
            </p>       
                
          
             </fieldset> 

        
        </form>
    </div>   
    
                                    
     <br><br>                               
     
          
      <span style="float:right;" > 
         
          {if $SuperAdmin}
      <span style="margin-right:30px;"  >      
    
            <input type="radio" class="dtRadioClass" name="dType" value="all" {if $dType eq 'all' || $dType eq ''} checked="checked" {/if} > ALL    
            <input type="radio" class="dtRadioClass" name="dType" value="via" {if $dType eq 'via'} checked="checked" {/if} > VIA
            <input type="radio" class="dtRadioClass" name="dType" value="sa" {if $dType eq 'sa'} checked="checked" {/if} > SA
            <input type="radio" class="dtRadioClass" name="dType" value="ao" {if $dType eq 'ao'} checked="checked" {/if} > AO

            
     </span>     
          {else}
          
              <input type="hidden"  name="dType" value="all" >
         
              
          {/if}   
         
     <select  name="ServiceProviderID" id="ServiceProviderID"  class="text"  >
        <option value="-1" {if $spID eq '-1'}selected="selected"{/if} >{$page['Text']['select_service_provider']|escape:'html'}</option>
         
         {foreach from=$ServiceProvidersList item=sp}
         <option value="{$sp.ServiceProviderID}" {if $spID eq $sp.ServiceProviderID}selected="selected"{/if} >{$sp.CompanyName|escape:'html'}</option>
         {/foreach}

     </select>
      <br><br>
    </span>
         
   
     
    <div class="diaryPerformanceResultsPanel" >
        
    <table id="diaryPerformanceResults" border="0" cellpadding="0" cellspacing="0" class="browse dataTable diaryPerformanceResults" >
        <thead>
                <tr>
                       
                       <th width="35%"  >
                       
                           <input type="radio" class="aTypeRadioClass" name="aType" value="all" {if $aType eq 'all' || $aType eq ''} checked="checked" {/if} > {$page['Labels']['all']|escape:'html'}
                           <input type="radio" class="aTypeRadioClass" name="aType" value="av" {if $aType eq 'av'} checked="checked" {/if} > {$page['Labels']['av_text']|escape:'html'}
                           <input type="radio" class="aTypeRadioClass" name="aType" value="ha" {if $aType eq 'ha'} checked="checked" {/if} > {$page['Labels']['ha_text']|escape:'html'}
                          
                           {if $SuperAdmin}
                            <input type="radio" class="aTypeRadioClass" name="aType" value="os" {if $aType eq 'os'} checked="checked" {/if} > {$page['Labels']['os_text']|escape:'html'}
                           {/if}
                       </th>
                       
                       {if $SuperAdmin}
                           
                         <th class="mediumBoarderth" >&nbsp;</th>
                       {else}
                        <th class="mediumBoarderth" style="text-align:center;font-weight:bold;" title="{$page['Text']['all_text']|escape:'html'}" >{$page['Text']['all_text']|escape:'html'}</th>
                       {/if}
                       
                       {assign var="cnt" value=-1}
                       {assign var="scnt" value=-1}
                       {foreach $dateRange as $dr}
                          
                        {assign var=scnt value=$scnt+1}
                        
                        {if !$SundayFlag && $scnt==$Sunday}
                            {continue}
                        {/if}
                        
                        {assign var=cnt value=$cnt+1}
                        
                        {if $cnt>4}
                              {break}  
                        {/if}
                        <th colspan="3" class="thickBoarderth" style="text-align:center;font-weight:bold;"  title="{$dr|escape:'html'}" >{$dr|escape:'html'}</th>
                        
                       {/foreach}  
                </tr>
                 <tr>
                       
                      
                       <th title="{$page['Text']['service_provider']|escape:'html'}" >{$page['Text']['service_provider']|escape:'html'}</th>
                       
                       {if $SuperAdmin}
                        <th width="6%" class="mediumBoarderth" style="text-align:center;" title="{$page['Labels']['type']|escape:'html'}" >{$page['Labels']['type']|escape:'html'}</th>
                       {else}
                       <th width="8%" class="mediumBoarderth" style="text-align:center;"  title="{$page['Text']['open_jobs']|escape:'html'}" >{$page['Text']['open_jobs']|escape:'html'}</th>
                       
                       {/if}
                       
                       {assign var="cnt" value=-1}
                       {assign var="scnt" value=-1}
                       {foreach $dateRange as $dr}
                           
                       
                        {assign var=scnt value=$scnt+1}
                        
                        {if !$SundayFlag && $scnt==$Sunday}
                            {continue}
                        {/if}   
                            
                        {assign var=cnt value=$cnt+1}
                        {if $cnt>4}
                              {break}  
                        {/if}



                        {if $SuperAdmin}
                          <th width="5%" class="thickBoarderth" style="text-align:center;"  title="{$page['Text']['total_capital']|escape:'html'}" >{$page['Text']['total_capital']|escape:'html'}</th>    
                          <th width="3%" style="text-align:center;"  title="S" >(S)</th>    
                          <th width="4%" style="text-align:center;"  title="{$page['Text']['avg_capital']|escape:'html'}" >{$page['Text']['avg_capital']|escape:'html'}</th>
                        {else}
                          <th width="11%" class="thickBoarderth" colspan="3" style="text-align:center;" title="{$page['Text']['apps']|escape:'html'}" >{$page['Text']['apps']|escape:'html'}</th>  
                        {/if}
                            
                        
                           
                       {/foreach}  
                </tr>
              </thead>
              
                
                {foreach from=$data key=dtk item=dt}
                    
                    <tr {if $dtk%2 eq 1} class="odd" {else} class="even" {/if} >  
                        {assign var="cntk" value=-1}
                        
                        {foreach from=$dt key=k item=v}
                            {if $k neq 0}
                                
                              
                                
                              {if !$SundayFlag && ($SundayData eq $k || $SundayData+1 eq $k || $SundayData+2 eq $k)} 
                                  {continue}
                              {/if}
                              
                              {assign var=cntk value=$cntk+1}
                              
                              {if $cntk>=17}
                              {break}  
                              {/if}
                              
                              {if !$SuperAdmin && $k>=3 &&  ($k%3==2 || $k%3==1)}
                              {continue}  
                              {/if}
                              
                              
                              {if $data|@count eq ($dtk+1)}
                                  
                                   
                                     <th  {if ($k%3 eq 0)} class="thickBoarderth"  {else if $k eq 2 } class="mediumBoarderth" {/if}   {if !$SuperAdmin && $k>=3 && ($k%3 eq 0)} colspan="3" {/if}   {if $k>1} style="text-align:center;" {/if}  >

                                        {if $k eq 1}

                                            {$v.0}

                                        {else}     
                                            
                                                 {if $v eq 0 or $SuperAdmin}
                                                 
                                                   {$v} 

                                                 {else if $k eq 2 && !$SuperAdmin}

                                                    <a class="wallboardLink" href="{$_subdomain}/AppointmentDiary/wallboard/spID={$spID}/aType={$aType}/dType={$dType}/dataSPID=-1/openJobs=1/#openJobsForm" title="{$page['Text']['click_open_jobs']|escape:'html'}"  >{$v}</a>
                                                 
                                                 {else}
                                                    
                                                    <a class="wallboardLink" href="{$_subdomain}/AppointmentDiary/wallboard/spID={$spID}/aType={$aType}/dType={$dType}/dataSPID=-1/appsDate={$sqlDateRange[(int)(($k/3)-1)]}/#openJobsForm" title="{$page['Text']['click_appointments']|escape:'html'}" >{$v}</a>
                                                
                                                 {/if}   

                                        {/if}    
                                     </th>
                                   
                              {else}
                                  
                                  
                                    
                                    <td  {if ($k%3 eq 0)}  {if $k gt 2 && $error[$dtk][($k/3)-1] && $SuperAdmin} class="errorElement thickBoardertd" {else} class="thickBoardertd" {/if}   {else if $k eq 2 } class="mediumBoardertd" {/if}   {if !$SuperAdmin && $k>=3 && ($k%3 eq 0)} colspan="3" {/if}   {if $k>1} style="text-align:center;" {/if}  >

                                         {if $k eq 1}

                                             {if $SuperAdmin}
                                                 <a href="#" class="SPAutoLoginLink wallboardLink"  id="{$v.1}" >{$v.0}</a>
                                             {else}    
                                                 {$v.0}
                                             {/if}    

                                         {else}

                                             {if $SuperAdmin || $v eq 0}

                                                 {assign var="bracket_at" value=$v|strpos:"("}
                                                  {assign var="Sbracket_at" value=$v|strpos:"["}
                                                 {if $bracket_at}
                                                     {$class=""}
                                                     <!--setting custom colors depending on time in sec sinc unreached-->
                                                     {$secs=$v|substr:$Sbracket_at}
                                                     {$secs=$secs|regex_replace:"/\[/":""}
                                                     {$secs=$secs|regex_replace:"/\]/":""}
                                                     {if $secs<=15*60}
                                                         {$backcolour="#FFE538"}
                                                      {elseif $secs<=30*60&&$secs>15*60}
                                                         {$backcolour="#FE7A15"}
                                                      {elseif $secs<=60*60&&$secs>30*60}
                                                         {$backcolour="#E42019"}
                                                      {elseif $secs>=60*60}
                                                         {$backcolour="#E42019"}
                                                         {$class="blinker"}
                                                         {/if}
                                                 
                                                         {$v|substr:0:$bracket_at} <span class="{$class}" style="background-color:{$backcolour};color:#000000;" >{$v|substr:$bracket_at|regex_replace:"/[[0-9]*]/":""}</span>

                                                 {else}

                                                    {$v}

                                                 {/if}

                                             {else}
                                                 
                                                 {if $k eq 2}
                                                     <a class="wallboardLink" href="{$_subdomain}/AppointmentDiary/wallboard/spID={$spID}/aType={$aType}/dType={$dType}/dataSPID={$dt.0}/openJobs=1/#openJobsForm" title="{$page['Text']['click_open_jobs']|escape:'html'}"  >{$v}</a>
                                                 {else}

                                                     <a class="wallboardLink" href="{$_subdomain}/AppointmentDiary/wallboard/spID={$spID}/aType={$aType}/dType={$dType}/dataSPID={$dt.0}/appsDate={$sqlDateRange[(int)(($k/3)-1)]}/#openJobsForm" title="{$page['Text']['click_appointments']|escape:'html'}" >{$v}</a>
                                                 
                                                  {/if}    
                                                 

                                             {/if} 

                                         {/if}
                                     </td>
                                    
                                     
                                  
                              {/if}
                              
                             
                              
                            {/if}
                        {/foreach}    
                            
                    </tr>        
                {/foreach}  
                
                
        
           
    </table>  
                
                
     {if $dataSPID}
         <br><br>
          

             <div class="openJobsPanel">
	
                <form id="openJobsForm" name="openJobsForm" method="post"  action="#" class="inline">
                    <p>
		    <div id="chart_div"></div>
		    
                    </p>
                </form>
              </div>
         
              
              <div id="WallboardJobResultsPanel" >

                  <table id="WallboardJobResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                      <thead>
                              <tr>
                                      <th title="Date" >Date</th>
                                      <th title="Days" >Days</th>
                                      <th title="Skyline No." >Skyline No.</th>
                                      <th title="Product Type" >Service Provider</th>
                                      <th title="Consumer" >Consumer</th>
                                      <th title="Post Code" >Post Code</th>
                                      <th title="Product Type" >Product Type</th>
                                      <th title="Status" >Status</th>
                                      <th title="App Date" >App Date</th>

                              </tr>
                      </thead>
                      <tbody>

                      </tbody>
                  </table>  

              </div>  
             
             
         
         
         
        
     {/if}                
   </div>
        
                        
    
                                    
    
                                    
</div>

<div id="waitDiv" style="display:none">
    <img src="{$_subdomain}/images/processing.gif"><br>
</div>

{/block}
