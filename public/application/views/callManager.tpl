{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $CallManagerPage}
{/block}

{block name=afterJqueryUI}
   <link rel="stylesheet" href="{$_subdomain}/css/Base/jquery.countup.css" />
   <script type="text/javascript" src="{$_subdomain}/js/jquery.contup.js"></script>
{/block}

{block name=scripts}





<script type="text/javascript" >
    
    //It opens Job update page.
        function openJob($rowData){
            
            if($rowData[0])
            document.location.href = "{$_subdomain}/index/jobupdate/"+urlencode($rowData[0]); 

        }

     
    
    $(document).ready(function() {
    
            
             $('#callDuration').countup();
             $('#ContactHistoryResultsPanel').hide();
             
             $("#CallerResults tr:odd").addClass("odd");
             $("#CallerResults tr:even").addClass("even");
            
             function ContactHistoryResults($JobID)
             {
                $('#ContactHistoryResults').PCCSDataTable( {
                       "aoColumns": [ 
                           /* Contact Date */  { sWidth : "10%" },    
                           /* Contact Time */  { sWidth : "15%" },   
                           /* UserCode */	    { bVisible: false },
                           /* Action */	    { sWidth : "15%" },
                           /* Subject */	    { sWidth : "15%" },
                           /* Note */	    null
                       ],
                       sDom:               't<"#dataTables_command3">rp',    
                       bottomButtonsDivId:	'dataTables_command3',
                       htmlTableId:        'ContactHistoryResults',
                       fetchDataUrl:       '{$_subdomain}/Data/contacthistory/'+$JobID+'/',
                       searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                       hidePaginationNorows:  true,
                       iDisplayLength:  5,
                       aaSorting:    [[ 0, "asc" ], [ 1, "asc" ]],
                       bDestroy: true

                   }); 
             }
    
    
    
              //Click handler for Previous JobID Radio starts here.
               $(document).on('click', ".PreviousJobIDRadio", 
                   function() {
                   
                           if($("input[name='JobID']:radio:checked").val())
                           {
                                   $('#ContactHistoryResultsHead').html("{$page['Text']['contact_history']|escape:'html'} ( {$page['Text']['sl_no']|escape:'html'}: "+$("input[name='JobID']:radio:checked").val()+" )");
                           }
                           else
                           {    
                                 $('#ContactHistoryResultsHead').html("{$page['Text']['contact_history']|escape:'html'}");
                           }
                           
                           $('#ContactHistoryResultsPanel').show();
                           
                           
                                
                            ContactHistoryResults($("input[name='JobID']:radio:checked").val());     
                                
                             
                              
                     });  
                     
                     
                     
                //Click handler for Customer ID Radio button starts here.
               $(document).on('click', ".CustomerIDRadio", 
                   function() {
                   
                           if($("input[name='CustomerID']:radio:checked").val())
                           {
                                   
                               $.post("{$_subdomain}/CallManager/fetchCallerDetails/cuID="+$("input[name='CustomerID']:radio:checked").val()+"/n={$number|escape:'html'}"+"/",         
                                                 '',      
                                                 function(data){
                                                         var $customerDetails  = eval("(" + data + ")");

                                                         if($customerDetails.length>0)
                                                         {
                                                            $CustomerAddress = '';
                                                            
                                                            for($i=2;$i<=6;$i++)
                                                            {    
                                                                if($customerDetails[$i]!='' && $customerDetails[$i]!=null) { $CustomerAddress  = $CustomerAddress+$customerDetails[$i]+'<br>'; }
                                                            }
                                                            $CustomerContact = '';
                                                            
                                                            
                                                            $CustomerContact = '<label style="font-size:1em;width:60px;margin:0px;text-align:left;" >'+"{$page['Text']['phone_number']|escape:'html'}</label>"; 
                                                            if($customerDetails[7]!='' && $customerDetails[7]!=null) 
                                                            {
                                                                $checked = '';
                                                                if ($customerDetails[7]=="{$number}") { $checked = ' checked="checked" '; }
                                                                
                                                                  $CustomerContact =  $CustomerContact+'<input type="radio" class="CustomerPhoneRadio"  value="'+$customerDetails[7]+'"  '+$checked+'  name="CustomerPhone" > '+$customerDetails[7]+' '; 
                                                                  
                                                                  if($customerDetails[8]!='' && $customerDetails[8]!=null) {
                                                                      
                                                                       $CustomerContact =  $CustomerContact+"&nbsp;&nbsp;({$page['Text']['phone_extension']|escape:'html'}) "+ $customerDetails[8]; 
                                                                          
                                                                  }
                                                                   $CustomerContact =  $CustomerContact+' <br>';
                                                                     

                                                            } 
                                                            
                                                            
                                                            $CustomerContact =  $CustomerContact+'<label style="font-size:1em;width:60px;margin:0px;text-align:left;" >'+"{$page['Text']['mobile_number']|escape:'html'}</label>";
                                                            if($customerDetails[9]!='' && $customerDetails[9]!=null) {
                                                                $checked = '';
                                                                if ($customerDetails[9]=="{$number}") { $checked = ' checked="checked" '; }
                                                            
                                                                 $CustomerContact =  $CustomerContact+'<input type="radio" class="CustomerPhoneRadio" '+$checked+'  value="'+$customerDetails[9]+'" name="CustomerPhone" > '+$customerDetails[9];
                                                            }    
                                
                                                            
                                                            
                                                            
                                                            $("#CustomerAddressColumn").html($CustomerAddress);
                                                            $("#CustomerContactColumn").html($CustomerContact);
                                                         }
                                                         else
                                                         {
                                                             alert("ssssss");
                                                         }

                                                 });     
                               
                           }
                           
                           
                            
                     });      
                     
                //Click handler for Customer ID Radio button ends here.      
                     
                     
                     
    
    
              //Click handler for Open Existing Job starts here.
               $(document).on('click', ".OpenExistingJob", 
                   function() {
                                
                                if( $("input[name='JobID']:radio:checked").length > 0 ) 
                                {
                                   var $rowData = new Array();
                                   $rowData[0] = $("input[name='JobID']:radio:checked").val();
                                   openJob($rowData);   
                                   
                                }
                                else
                                {
                                    alert("{$page['Errors']['job_select']|escape:'html'}");
                                    $("input[name='JobID']").focus();
                                }
                                
                            return false;    
                              
                     });
        
    
    
            
              //click handler for free text job booking link starts here.
               $(document).on('click', ".FreeTextJobBooking", 
                   function() {


                               //It opens color box popup page.              
                               $.colorbox( { href: '{$_subdomain}/Job/networkClientPopUp//'+ Math.random(),
                                               title: 'New Job Booking',
                                               data: $('#newJobBookingForm').serializeArray(), 
                                               opacity: 0.75,
                                               height:210,
                                               width:670,
                                               overlayClose: false,
                                               escKey: false, 
                                               onComplete: function(){


                                                   $(this).colorbox.resize({
                                                   height: ($('#JobNetworkClientForm').height()+150)+"px"                                
                                               });



                                               }

                                               }); 
                     });
                     
                     
                     
              //Previous job result data table starts here...   
                $('#PreviousJobsResults').PCCSDataTable( {

                        displayButtons: "",
                        bServerSide: false,
                        htmlTablePageId:    'PreviousJobsResultsPanel',
                        htmlTableId:        'PreviousJobsResults',
                        fetchDataUrl:       '{$_subdomain}/Job/fetchCaller/{$number|escape:'html'}/',
                        pickCallbackMethod: 'openJob',
                        dblclickCallbackMethod: 'openJob',
                        tooltipTitle: "{$page['Text']['tooltip_title']|escape:'html'}",
                        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                        colorboxForceClose:false,
                        hidePaginationNorows:true,
                        iDisplayLength: 5,
                        sDom: 't<"#dataTables_command">rpli',
                        aoColumns: [
			null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    ]

                    });
             //Previous job result data table ends here...            
    
            
             
             
             
    
    
    });
    
    

</script>


    
{/block}

{block name=body}

    
<div class="breadcrumb">
    
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        {$page['Text']['page_title']|escape:'html'}
    </div>
    
</div>
 


        
<div class="main"  >   
   
    {if $number neq ''}
    <div class="pageSplitPanel" >
           
        <div id="leftDiv" class="secondDiv borderDiv" >
        
            <p>
            <label class="field" ><label class="field" style="margin-bottom:4px;" >{$page['Text']['caller_id']|escape:'html'}</label><br>
            
                <span style="font-size:1.2em;font-weight:bold;" class="phoneNumber" >{$number|escape:'html'}</span>
            
            </label>
            <label class="field" ><label class="field" style="margin-bottom:4px;" >{$page['Text']['call_duration']|escape:'html'}</label><br>
            
                
                 <span style="font-size:1em;font-weight:bold;" id="callDuration" ></span>
                 
            </label>
            </p>   
            
             <table id="CallerResults" border="0" cellpadding="0" cellspacing="0" class="browse"  >
                <thead>
                        <tr>
                                
                                <th  title="{$page['Text']['caller']|escape:'html'}"  >{$page['Text']['caller']|escape:'html'}</th>
                                <th  title="{$page['Text']['caller_address']|escape:'html'}"  >{$page['Text']['caller_address']|escape:'html'}</th>
                                <th  title="{$page['Text']['registered_numbers']|escape:'html'}"  >{$page['Text']['registered_numbers']|escape:'html'}</th>
                                
                        </tr>
                </thead>
                <tbody>
                      {$foo=1}
                      {foreach $callerDetails as $cd}
                        <tr  >
                            
                            {if $foo eq 1}
                                
                                <td><input type="radio" class="CustomerIDRadio"  value="{$cd.0|escape:'html'}" checked="checked"  name="CustomerID" > {$cd.1|escape:'html'}</td>
                                <td rowspan="{$callerDetails|@count}" id="CustomerAddressColumn"  >
                                    
                                    {if $cd.2 neq ''} {$cd.2|escape:'html'}<br> {/if}
                                    {if $cd.3 neq ''} {$cd.3|escape:'html'}<br> {/if}
                                    {if $cd.4 neq ''} {$cd.4|escape:'html'}<br> {/if}
                                    {if $cd.5 neq ''} {$cd.5|escape:'html'}<br> {/if}
                                    {if $cd.6 neq ''} {$cd.6|escape:'html'}<br> {/if}
                                
                                </td>
                                <td rowspan="{$callerDetails|@count}" id="CustomerContactColumn" >
                                    
                                    <label style="font-size:1em;width:60px;margin:0px;text-align:left;" >{$page['Text']['phone_number']|escape:'html'}</label> {if $cd.7 neq ''} <input type="radio" class="CustomerPhoneRadio"  value="{$cd.7|escape:'html'}"  {if $number eq $cd.7} checked="checked" {/if}  name="CustomerPhone" > {$cd.7|escape:'html'} {if $cd.8 neq ''}&nbsp;&nbsp;({$page['Text']['phone_extension']|escape:'html'}) {$cd.8|escape:'html'} {/if} <br>{/if} 
                                    <label style="font-size:1em;width:60px;margin:0px;text-align:left;" >{$page['Text']['mobile_number']|escape:'html'}</label> {if $cd.9 neq ''} <input type="radio" class="CustomerPhoneRadio"  value="{$cd.9|escape:'html'}" {if $number eq $cd.9} checked="checked" {/if} name="CustomerPhone" > {$cd.9|escape:'html'} {/if}
                                
                                </td>
                                
                                {$foo=0}
                             {else}
                                 <td><input type="radio" class="CustomerIDRadio"  value="{$cd.0|escape:'html'}"  name="CustomerID" > {$cd.1|escape:'html'}</td>
                             {/if}
                        </tr>
                      {/foreach}  
                </tbody>
            </table>
                                
                               
            <h4>{$page['Text']['previous_jobs']|escape:'html'}</h4>                 
            <table id="PreviousJobsResults" border="0" cellpadding="0" cellspacing="0" class="browse"  >
                <thead>
                        <tr>
                                <th width="10%" title="{$page['Text']['sl_no']|escape:'html'}" >{$page['Text']['sl_no']|escape:'html'}</th>
                                <th width="17%"  title="{$page['Text']['date_booked']|escape:'html'}"  >{$page['Text']['date_booked']|escape:'html'}</th>
                                <th width="10%" title="{$page['Text']['job_no']|escape:'html'}"  >{$page['Text']['job_no']|escape:'html'}</th>
                                <th width="13%" title="{$page['Text']['make']|escape:'html'}"  >{$page['Text']['make']|escape:'html'}</th>
                                <th width="15%" title="{$page['Text']['model']|escape:'html'}"  >{$page['Text']['model']|escape:'html'}</th>
                                <th width="15%" title="{$page['Text']['type']|escape:'html'}"  >{$page['Text']['type']|escape:'html'}</th>
                                <th width="20%" title="{$page['Text']['fault']|escape:'html'}"  >{$page['Text']['fault']|escape:'html'}</th>
                                
                        </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table> 
                                
                                
            <div style="padding:0px;margin:0px;width:100%;" id="ContactHistoryResultsPanel" >                    
                <h4 id="ContactHistoryResultsHead" >{$page['Text']['contact_history']|escape:'html'}</h4>                 
                <table id="ContactHistoryResults" border="0" cellpadding="0" cellspacing="0" class="browse"  >
                    <thead>
                            <tr>

                                    <th  title="{$page['Text']['date']|escape:'html'}"  >{$page['Text']['date']|escape:'html'}</th>
                                    <th  title="{$page['Text']['time']|escape:'html'}"  >{$page['Text']['time']|escape:'html'}</th>
                                    <th></th>
                                    <th  title="{$page['Text']['type']|escape:'html'}"  >{$page['Text']['type']|escape:'html'}</th>
                                    <th  title="{$page['Text']['action']|escape:'html'}"  >{$page['Text']['action']|escape:'html'}</th>
                                    <th  title="{$page['Text']['notes']|escape:'html'}"  >{$page['Text']['notes']|escape:'html'}</th>


                            </tr>
                    </thead>
                    <tbody>
                        <tr class="odd" >
                            <td colspan="6" >No Results Found.</td>
                        </tr>
                    </tbody>
                </table>  
             </div>                   
                                
                                
            <h4>{$page['Text']['policies']|escape:'html'}</h4>                 
            <table id="PoliciesResults" border="0" cellpadding="0" cellspacing="0" class="browse"  >
                <thead>
                        <tr>
                                
                                <th  title="{$page['Text']['start_date']|escape:'html'}"  >{$page['Text']['start_date']|escape:'html'}</th>
                                <th  title="{$page['Text']['end_date']|escape:'html'}"  >{$page['Text']['end_date']|escape:'html'}</th>
                                <th  title="{$page['Text']['policy_no']|escape:'html'}"  >{$page['Text']['policy_no']|escape:'html'}</th>
                                <th  title="{$page['Text']['cover_plan']|escape:'html'}"  >{$page['Text']['cover_plan']|escape:'html'}</th>
                                <th  title="{$page['Text']['item_covered']|escape:'html'}"  >{$page['Text']['item_covered']|escape:'html'}</th>
                              
                        </tr>
                </thead>
                <tbody>
                    <tr class="odd" >
                        <td colspan="5" >No Results Found.</td>
                    </tr>
                </tbody>
            </table>                     
            
            <br><br><br><br><br><br>
        </div>
        
        <div id="rightDiv" class="firstDiv borderDiv" > 
            
           <table id="CallManagerOptions"  >
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" class="OpenExistingJob" title="{$page['Text']['open_existing_job']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_openjob.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" class="OpenExistingJob" >{$page['Text']['open_existing_job']|escape:'html'}</a>
                    </td>
               </tr> 
               
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" class="FreeTextJobBooking" title="{$page['Text']['book_new_job']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_book.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" class="FreeTextJobBooking" >{$page['Text']['book_new_job']|escape:'html'}</a>
                    </td>
               </tr> 
               
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" title="{$page['Text']['phone_customer']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_phone.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" >{$page['Text']['phone_customer']|escape:'html'}</a>
                    </td>
               </tr> 
               
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" title="{$page['Text']['send_sms']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_sms.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" >{$page['Text']['send_sms']|escape:'html'}</a>
                    </td>
               </tr> 
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" title="{$page['Text']['send_email']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_email.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" >{$page['Text']['send_email']|escape:'html'}</a>
                    </td>
               </tr> 
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" title="{$page['Text']['listen_to_call']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_listen.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" >{$page['Text']['listen_to_call']|escape:'html'}</a>
                    </td>
               </tr>
               
               <tr>
                   <td style="text-align:right;" >
                        <a href="#" title="{$page['Text']['phone_a_colleague']|escape:'html'}" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/service_base_phone_a_colleague.png" width="64" height="64" ></a>
                   </td>
                   
                    <td style="text-align:left;" >
                        <a href="#" >{$page['Text']['phone_a_colleague']|escape:'html'}</a>
                    </td>
               </tr> 
               
            </table>
            
        </div>
        
    </div> 
                    
    {/if}
    
</div>
    
    
{/block}
