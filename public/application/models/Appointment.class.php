<?php

/**
 * Appiontment.class.php
 * 
 * Routines for interaction with the appointments table
 *
 * @author     Simon Tsang <s.tsang@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.09
 * 
 * Changes
 * Date        Version Author                Reason
 * 16/03/2012  1.00    Simon Tsang           Initial Version
 * 25/06/2012  1.01    Andrew J. Williams    Additions required for Skyline API
 * 22/08/2012  1.02    Andrew J. Williams    Additions rqeuired for Samsung API
 * 29/10/2012  1.03    Andrew J. Williams    Issue 113 - Duplicate child files in Appointment and Status History
 * 07/11/2012  1.04    Andrew J. Williams    Issue 120 - Errors in for Samsung comms
 * 16/11/2012  1.05    Andrew J. Williams    Added getSBJobAppointmentList
 * 14/01/2013  1.06    Andrew J. Williams    Issue 177 - Skyline export changes
 * 01/02/2013  1.07    Andrew J. Williams    Trackerbase VMS Log 157 - Added getNextAppointmentNonSkyline
 * 13/02/2013  1.08    Nageswara Rao Kanteti Appointment Section on Job Update Page fIXED
 * 19/04/2013  1.09    Andrew J. Williams    Data Integrity Check
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class Appointment extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    public $db = array(
        'AppointmentDate' => 'Date',
        'AppointmentTime' => 'Time',
        'AppointmentType' => 'Text' 
    );
    

    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Appointment();
    }
    
    public function getAppointmentType(){
        $result = array(
            array('Name'=>'Alpha'),
            array('Name'=>'Beta'),
            array('Name'=>'Charle'),
            array('Name'=>'Delta')
            
        );
    
        return $result;
    }
    
    /**
     * create
     *  
     * Create an appointment
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new appointment
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'appointmentId' => $this->conn->lastInsertId()     /* Return the newly created appointment's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'appointmentId' => 0,                               /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * update
     *  
     * Update an appointment
     * 
     * @param array $args   Associative array of field values for to update the
     *                      appointment. The array must include the primary key
     *                      AppointmentID
     * 
     * @return integer  Number of rows effected
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'appointmentId' => $args['AppointmentID'],
                             'message' => ''
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete an entry from the appointments table. We can delete by either 
     * AppointmentID or SBAppointID. To select the record to delete we must pass
     * a array with record with the key being either AppointmentID or SBAppointID
     * and tghe value being the appropriate ID to delete.
     * 
     * @param array $args (Field { AppointmentID, SBAppointID } => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($arg) {
        $index  = array_keys($arg);
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$arg[$index[0]] );
        
        if ($this->Execute($this->conn, $cmd, $arg)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'message' => 'Deleted'
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * deleteByJobId
     *  
     * Delete all the appointments for a specific job.
     * 
     * This is specifcally used in Data Integrity Check Refresh Process
     * 
     * @param array $jId    JobID
     * 
     * @return true if sucessful, false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function deleteByJobId($jId) {
        $sql = "
                DELETE
		FROM
			`appointment`
		WHERE
			`JobID` = $jId
               ";
        
        $count = $this->conn->exec($sql);

        if ( $count !== false ) {
            return(true);                         
        } else {
            return(false);                                 
        }
    }

    /**
     * getNextAppointment
     *  
     * Get the next appointment for a certain job. The routine achieves this by 
     * searching for the latest appointment but discarding anything beofre todays date
     * 
     * @param array $jId    JobID
     * 
     * @return assoicative array with appointment details or none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getNextAppointment($jId) {
        $sql = "
                SELECT
			*
		FROM
			`appointment`
		WHERE
			`AppointmentDate` > NOW()
                        AND `JobID` = $jId
                            
                        ORDER BY AppointmentID ASC
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Appointment exists */
        } else {
            return(null);                                                       /* Not found return null */
        }    
    }
    
    /**
     * getNextAppointment
     *  
     * Get the next appointment for a certain none skyline job. The routine 
     * achieves this by  searching for the latest appointment but discarding 
     * anything beofre todays date
     * 
     * @param array $nsjId    Non Skyline Job ID
     * 
     * @return assoicative array with appointment details or none
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getNextAppointmentNonSkyline($nsjId) {
        $sql = "
                SELECT
			*
		FROM
			`appointment`
		WHERE
			`AppointmentDate` > NOW()
                        AND `NonSkylineJobID` = $nsjId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Appointment exists */
        } else {
            return(null);                                                       /* Not found return null */
        }    
    }

    
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
       
        $sql = 'SELECT AppointmentID, JobID, AppointmentDate, AppointmentTime, AppointmentType, AppointmentStartTime, AppointmentEndTime, ServiceProviderID, ServiceProviderEngineerID, Notes, NonSkylineJobID, CustContactTime, CustContactType FROM `appointment` WHERE AppointmentID=:AppointmentID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':AppointmentID' => $args['AppointmentID']));
        
        
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    /**
     * getAppointmentList
     *  
     * Return a list of appointments and engineers for a certain day 
     * 
     * @param $d    Date to produce routed list for (in mysql yyyy-mm-dd format)
     *        $scId Service Centre ID
     * 
     * @return      Array containing response from service base 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getAppointmentList($d, $scId) {
        $sql = "
		SELECT
			IF (ISNULL(cu.`CustomerID`),
                                    CONCAT(nsj.`CustomerTitle`,' ',nsj.`CustomerSurname`)
                            ,
                                    CONCAT(cu.`ContactFirstName`,' ',cu.`ContactLastName`)
                            ) AS `name`,
                        IF (ISNULL(cu.`CustomerID`),
                                    nsj.`Postcode`
                            ,
                                    IF (ISNULL(j.`ColAddPostcode`),
                                            cu.`PostalCode`
                                    ,
                                            j.`ColAddPostcode`
                                    )
                            ) AS `location`,
			a.`ViamenteStartTime`,
			spe.`EngineerFirstName`,
			spe.`EngineerLastName`,
			spe.`RouteColour`
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`jobID` = j.`JobID`
                                        LEFT JOIN `customer` cu ON j.`CustomerID` = cu.`CustomerID`
                                        LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`,
			`service_provider` sp,
			`service_provider_engineer` spe
		WHERE
			
			a.`ServiceProviderEngineerID` = spe.`ServiceProviderEngineerID`
			AND a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = $scId
			AND a.`AppointmentDate` = '$d'
		ORDER BY
			a.`ViamenteStartTime`
               ";
        
        $result = $this->Query($this->conn, $sql);

        return($result);
    }
    
    /**
     * doesSBAppointExist
     *  
     * Returns whether an appointmentment with a given Servicbase appointment
     * ID exists.
     * 
     * @param sbAId Servicebasee Appointment ID
     * 
     * @return      Boolean containing whether Sb Appointment ID exists
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function doesSBAppointExist($sbAId) {
        $sql = "
                SELECT
			COUNT(`SBAppointID`) AS `count`
		FROM
		        `appointment`
		WHERE
			`SBAppointID` = $sbAId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ($result[0]['count'] == 0) {
            return(false);
        } else {
            return (true);
        }
    
    }
    
    /**
     * getNumberForJob
     *  
     * Get the number of appointments for a specfifc job
     * 
     * @param sbAId Servicebasee Appointment ID
     * 
     * @return      Boolean containing whether Sb Appointment ID exists
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getNumberForJob($jId) {
        $sql = "
                SELECT
			COUNT(`AppointmentID`) AS `count`
		FROM
		        `appointment`
		WHERE
			`JobID` = $jId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result[0]['count']);
    }
    
    /**
     * getIdFromSBAppoint
     *  
     * Returns the ID of an appointment with a given Servicbase appointment ID
     * 
     * @param sbAId Servicebasee Appointment ID
     * 
     * @return      Boolean containing whether Sb Appointment ID exists
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getIdFromSBAppoint($sbAId) {
        $sql = "
                SELECT
			AppointmentID
		FROM
		        `appointment`
		WHERE
			`SBAppointID` = $sbAId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if (count($result) == 0) {
            return(false);
        } else {
            return ($result[0]['AppointmentID']);
        }
    
    }
    
    
    
    
    /**
     * getAppointment
     *  
     * Returns appointment details for given service provider engineer id and date.
     * 
     * @param int $ServiceProviderEngineerID
     * @param int $AppointmentDate 
     * 
     * @return array
     *
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    
    public function getAppointment($ServiceProviderEngineerID, $AppointmentDate) {
        
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T1.AppointmentID, T2.ServiceCentreJobNo, T4.ServiceProviderJobNo, T1.JobID, T1.NonSkylineJobID, DATE_FORMAT(T1.AppointmentDate, "%d/%m/%Y") AS AppointmentDate, T1.AppointmentTime, T1.AppointmentType, T1.ServiceProviderSkillsetID, T1.ServiceProviderID, T1.ServiceProviderEngineerID, T3.PostalCode AS CustPostCode, T4.Postcode AS NSCustPostCode, T1.ForceEngineerToViamente FROM appointment AS T1 
            
        LEFT JOIN job AS T2 ON T1.JobID=T2.JobID 
        LEFT JOIN customer AS T3 ON T2.CustomerID=T3.CustomerID 
        LEFT JOIN non_skyline_job AS T4 ON T1.NonSkylineJobID=T4.NonSkylineJobID 

        WHERE T1.ServiceProviderEngineerID=:ServiceProviderEngineerID AND T1.AppointmentDate=:AppointmentDate';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
         
        $fetchQuery->execute(array(':ServiceProviderEngineerID' => $ServiceProviderEngineerID, ':AppointmentDate'=>$AppointmentDate));
        $result = $fetchQuery->fetchAll();
        
        return $result;
        
    }
    
    
    
    
    /**
     * getSBJobAppointmentList
     *  
     * Returns the ID of an appointment with a given Servicbase appointment ID
     * 
     * @param       $sbJobNo - Servicebase Job Number
     *              $uId - User ID
     * 
     * @return      Associative array of records
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getSBJobAppointmentList($sbJobNo, $uId) {
        $sql = "
                SELECT
			IF (ISNULL(j.`ServiceCentreJobNo`),
				nsj.`ServiceProviderJobNo`
			,
				j.`ServiceCentreJobNo`
			) AS `SBJobNo`,
			a.*
		FROM
			`appointment` a LEFT JOIN `job` j ON a.`JobID` = j.`JobID`
					LEFT JOIN `non_skyline_job` nsj ON a.`NonSkylineJobID` = nsj.`NonSkylineJobID`,
                        `service_provider` sp,
                        `user` u
		WHERE
			a.`ServiceProviderID` = sp.`ServiceProviderID`
			AND a.`ServiceProviderID` = u.`ServiceProviderID`
                        AND 
                        (
                             nsj.`ServiceProviderJobNo` = $sbJobNo
                             OR j.`ServiceCentreJobNo` = $sbJobNo
                        )
                        AND u.`UserID` = $uId
               ";
        
        if($this->debug) $this->log("Appointment::getSBJobAppointmentList sql = $sql  ",'appointments_api_');
        
        $result = $this->Query($this->conn, $sql);
        
        if($this->debug) $this->log("Appointment::getSBJobAppointmentList result = ".var_export($result,true),'appointments_api_');

        return($result);
    }
}
?>
