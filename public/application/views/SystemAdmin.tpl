{extends "DemoLayout.tpl"}



 

{block name=config}
{$Title = $page['Text']['page_title']|escape:'html'}
{$PageId = $AdminHomePage}


{* ###################################################################
     #  SMARTY Functions:
     # 
     #   name:  admin_menu_element
     #   args:  $title, $description, $tooltip, $link
     #
     ##################################################################### *}
    
    {function admin_menu_element}
    {strip}
         <div class="AdminItemPanel" title="{$tooltip|escape:'html'}" onclick="document.location.href='{$_subdomain}{$link}';" >
                <label>{$title|escape:'html'}</label><br>
                <span>{$description|escape:'html'}</span>
        </div>
        
        
    {/strip}
    {/function}
 {* #################################################################### *}

{/block}

{block name=scripts}



{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

<script type="text/javascript">

jQuery(document).ready(function(){
       
    //2012-09-14 JS snippet inserted by Vic
    //Functionality: descriptions of particular facilities that are still under development are made red.
    //When all these facilities are functional this is to be removed.

    $("label").each(function() {
        var txt=$(this).text();
        if(txt=="System Defaults" || 
           txt=="Financial Setup" || 
           txt=="Procedures & Scheduled Tasks" ||
           txt=="Text & Language Setup" || 
           txt=="Access Permissions "
           ) {
            $(this).next().next().css("color","red");
        }
    });
       
    //End of edit
   
});
        
</script>

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
       
         <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['page_title']|escape:'html'}</a> / 
         
          {if $levelTwoPage neq '' && $levelTwoPageName neq ''}
            <a href="{$_subdomain}/SystemAdmin/index/{$levelTwoPage|escape:'html'}" >{$levelTwoPageName|escape:'html'}</a>
          {/if}   
        
        <!--<span style="float:right" >Welcome Joe Berry:  Argos Northamption - Last logged in Thurday 15 March 2012</span>-->
        
    </div>
</div>
    

                                    
<div class="main" id="home" >
    
           {if $levelTwoPage neq '' && $levelTwoPageName neq ''}
                <div class="systemAdminPanel" >
                     <form id="systemAdminForm" name="systemAdminForm" method="post"  action="#" class="inline">
                         
                          <fieldset>
                          <legend title="" >{$levelTwoPageName|escape:'html'}</legend>
                          <p>
                             <label>{$levelTwoPageDesc|escape:'html'}</label>
                          </p> 
                          
                          </fieldset> 

                             
                    </form>
                </div>   
            
           {/if}       
    
            {foreach $displayMenuItemList as $mItem}
            
                {admin_menu_element link="{$mItem['link']}" title="{$mItem['title']}" tooltip="{$mItem['tooltip']}" description = "{$mItem['shortDesc']}"}
                
            {/foreach}   
    
            
         
</div>
{/block}
