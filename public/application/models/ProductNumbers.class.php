<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Product Numbers Page in Product Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class ProductNumbers extends CustomModel {
    
    private $conn;
    
    private $table                      = "product";
    private $table_network              = "network";
    private $table_client               = "client";
    private $table_model                = "model";
    
    
    
    private $tables                     = "product AS T1 LEFT JOIN model AS T2 ON T1.ModelID=T2.ModelID LEFT JOIN unit_type AS T3 ON T1.UnitTypeID=T3.UnitTypeID";
    private $dbTableColumns             = array('T1.ProductID', 'T1.ProductNo', 'T2.ModelNumber', 'T2.ModelDescription', 'T3.UnitTypeName', 'T1.Status');
    
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables 
     * 
     * @global $this->dbTableColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        
        $NetworkID     = isset($args['firstArg'])?$args['firstArg']:'';
        $ClientID      = isset($args['secondArg'])?$args['secondArg']:'';
        
        
        if($NetworkID!='' && $ClientID!='')
        {
             $args['where']    = "T1.NetworkID='".$NetworkID."' AND T1.ClientID='".$ClientID."'";
        }
        else  if($NetworkID!='')
        {
             $args['where']    = "T1.NetworkID='".$NetworkID."'";
        }
        else 
        {
            $args['where']    = "T1.ProductID='0'";
        }    
        
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbTableColumns, $args);
        
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['ProductID']) || !$args['ProductID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
     
      /**
     * Description
     * 
     * This method is used for to check whether product number exists for client and network.
     *
     * @param interger $NetworkID
     * @param interger $ClientID
     * @param string   $ProductNo
     * @param interger $ProductID
     * @param interger $ReturnProductID 
     * @global $this->table
     * 
     * @return boolean if $ReturnProductID is null otherwise it returns array with product id and model id.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($NetworkID, $ClientID, $ProductNo, $ProductID, $ReturnProductID=null) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ProductID, ModelID, UnitTypeID FROM '.$this->table.' WHERE NetworkID=:NetworkID AND ClientID=:ClientID AND ProductNo=:ProductNo AND ProductID!=:ProductID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ProductID' => $ProductID,  ':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':ProductNo' => $ProductNo));
        $result = $fetchQuery->fetch();
        
        if($ReturnProductID)
        {
            if(is_array($result) && $result['ProductID'])
            {
                    return $result;
            }
            else
            {
                 return false;
            }
        
        }
        else 
        {
            if(is_array($result) && $result['ProductID'])
            {
                    return false;
            }

            return true;
        }
    }
    
   
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
         if($this->isValid($args['NetworkID'], $args['ClientID'], $args['ProductNo'], 0))
         {
                $result = false;
                
                $args['ModelID'] = $this->processModel($args['ModelNumber'], $args['ManufacturerID'], $args['UnitTypeID'], $args['ModelDescription']);
                    
                /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO '.$this->table.' (NetworkID, ClientID, ProductNo, ModelID, UnitTypeID, ActualSellingPrice, AuthorityLimit, PaymentRoute, Status, CreatedDate, ModifiedUserID, ModifiedDate)
                VALUES(:NetworkID, :ClientID, :ProductNo, :ModelID, :UnitTypeID, :ActualSellingPrice, :AuthorityLimit, :PaymentRoute, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                $result =  $insertQuery->execute(array(

                    ':NetworkID' => $args['NetworkID'],
                    ':ClientID' => $args['ClientID'], 
                    ':ProductNo' => $args['ProductNo'], 
                    ':ModelID' => $args['ModelID'], 
                    ':UnitTypeID' => $args['UnitTypeID'], 
                    ':ActualSellingPrice' => $args['ActualSellingPrice'], 
                    ':AuthorityLimit' => $args['AuthorityLimit'], 
                    ':PaymentRoute' => $args['PaymentRoute'],                     
                    ':Status' => $args['Status'],
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

                    ));
                

                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_inserted_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * @global $this->table  
     * @global $this->table_network  
     * @global $this->table_client  
     * @global $this->table_model  
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT NetworkID, ClientID, ProductNo, ModelID, UnitTypeID, ActualSellingPrice, AuthorityLimit, PaymentRoute, Status, ProductID FROM '.$this->table.' WHERE ProductID=:ProductID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ProductID' => $args['ProductID']));
        $result = $fetchQuery->fetch();
        
        
        if(is_array($result) && isset($result['ModelID']))
        {
            //Getting model details.
            $sql2        = "SELECT ModelNumber, ManufacturerID, ModelDescription  FROM ".$this->table_model." WHERE ModelID=:ModelID AND Status='".$this->controller->statuses[0]['Code']."'";
            $fetchQuery2 = $this->conn->prepare($sql2, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery2->execute(array(':ModelID' => $result['ModelID']));
            $result2     = $fetchQuery2->fetch();
            $result['ModelNumber']       = isset($result2['ModelNumber'])?$result2['ModelNumber']:'';
            $result['ManufacturerID']    = isset($result2['ManufacturerID'])?$result2['ManufacturerID']:'';
            $result['ModelDescription']  = isset($result2['ModelDescription'])?$result2['ModelDescription']:'';
            
        }
        
        
        if(is_array($result) && isset($result['NetworkID']))
        {
            //Getting network name.
            $sql3        = "SELECT CompanyName FROM ".$this->table_network." WHERE NetworkID=:NetworkID AND Status='".$this->controller->statuses[0]['Code']."'";
            $fetchQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery3->execute(array(':NetworkID' => $result['NetworkID']));
            $result3     = $fetchQuery3->fetch();
            $result['NetworkName']  = isset($result3['CompanyName'])?$result3['CompanyName']:'';
            
        }
        
        if(is_array($result) && isset($result['ClientID']))
        {
            //Getting client name.
            $sql4        = "SELECT ClientName FROM ".$this->table_client." WHERE ClientID=:ClientID AND Status='".$this->controller->statuses[0]['Code']."'";
            $fetchQuery4 = $this->conn->prepare($sql4, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery4->execute(array(':ClientID' => $result['ClientID']));
            $result4     = $fetchQuery4->fetch();
            $result['ClientName']  = isset($result4['ClientName'])?$result4['ClientName']:'';
            
        }
     
        
        return $result;
     }
    
     
     
     
     
     
     
     
    /**
     * Description
     * 
     * This method is used for to fetch active product nos for given client and network.
     *
     * @param  array $NetworkID
     * @param  array $ClientID 
     * 
     * @return array It contains list of product numbers.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchProductNumbers($NetworkID, $ClientID) {
        
        
        $tables_list = 'product AS t1 LEFT JOIN model  AS t2 ON t1.ModelID = t2.ModelID';
        
        $fields = 't1.ProductNo, t2.ModelNumber, t2.ModelDescription, t1.UnitTypeID, t2.ManufacturerID';
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT '.$fields.' FROM '.$tables_list.' WHERE t1.NetworkID=:NetworkID AND t1.ClientID=:ClientID AND t1.Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':Status' => 'Active'));
        $result = $fetchQuery->fetchAll();
        
     
        
        return $result;
     }
     
     
    
     
      /**
     * Description
     * 
     * This method is used for to insert the model details if its not exist in database and returns model id.
     *
     * @param string   $ModelNumber
     * @param integer  $ManufacturerID
     * @param integer  $UnitTypeID
     * @param string   $ModelDescription
     * @global $this->table_model
     * 
     * @return integer $ModelID.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function processModel($ModelNumber, $ManufacturerID, $UnitTypeID, $ModelDescription) {
        
         $ModelID = 0;
         
         
         //Checking whether model number exists or not
         $sql        = 'SELECT ModelID, Status FROM '.$this->table_model.' WHERE ModelNumber=:ModelNumber';
         $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
         $fetchQuery->execute(array(':ModelNumber' => $ModelNumber));
         $result = $fetchQuery->fetch();
        
         if(is_array($result) && $result['ModelID'])
         {
             $ModelID = $result['ModelID'];
             
             //If status is in-active..then we are updating status to active with new details..
             if($result['Status']==$this->controller->statuses[1]['Code'])
             {
              
                     /* Execute a prepared statement by passing an array of values */
                    $sql = 'UPDATE '.$this->table_model.' SET ModelNumber=:ModelNumber, ManufacturerID=:ManufacturerID, UnitTypeID=:UnitTypeID, ModelDescription=:ModelDescription, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE ModelID=:ModelID';

                    $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                    $result =  $updateQuery->execute(array(

                        ':ModelNumber' => $ModelNumber,
                        ':ManufacturerID' => $ManufacturerID, 
                        ':UnitTypeID' => $UnitTypeID, 
                        ':ModelDescription' => $ModelDescription, 
                        ':Status' => $this->controller->statuses[0]['Code'],
                        ':EndDate' => "0000-00-00 00:00:00",
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':ModelID' => $ModelID

                        ));
                 
             }    
                
         }
         else  //Inserting model details into database.
         {
             
                 /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO '.$this->table_model.' (ModelNumber, ManufacturerID, UnitTypeID, ModelDescription, Status, CreatedDate, ModifiedUserID, ModifiedDate)
                VALUES(:ModelNumber, :ManufacturerID, :UnitTypeID, :ModelDescription, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


                $result =  $insertQuery->execute(array(

                    ':ModelNumber' => $ModelNumber,
                    ':ManufacturerID' => $ManufacturerID, 
                    ':UnitTypeID' => $UnitTypeID, 
                    ':ModelDescription' => $ModelDescription, 
                    ':Status' => $this->controller->statuses[0]['Code'],
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

                    ));
             
                $ModelID = $this->conn->lastInsertId();
             
         }    
          
         
       return $ModelID;
    
    }
     
     
     
     
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
         if($this->isValid($args['NetworkID'], $args['ClientID'], $args['ProductNo'], $args['ProductID']))
         {         
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }

            $args['ModelID'] = $this->processModel($args['ModelNumber'], $args['ManufacturerID'], $args['UnitTypeID'], $args['ModelDescription']);


            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET NetworkID=:NetworkID, ClientID=:ClientID, ProductNo=:ProductNo, ModelID=:ModelID, UnitTypeID=:UnitTypeID, ActualSellingPrice=:ActualSellingPrice, AuthorityLimit=:AuthorityLimit, PaymentRoute=:PaymentRoute, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE ProductID=:ProductID';


            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result = $updateQuery->execute(


                    array(

                            ':NetworkID' => $args['NetworkID'],
                            ':ClientID' => $args['ClientID'], 
                            ':ProductNo' => $args['ProductNo'], 
                            ':ModelID' => $args['ModelID'], 
                            ':UnitTypeID' => $args['UnitTypeID'], 
                            ':ActualSellingPrice' => $args['ActualSellingPrice'], 
                            ':AuthorityLimit' => $args['AuthorityLimit'], 
                            ':PaymentRoute' => $args['PaymentRoute'],  
                            ':Status' => $args['Status'],
                            ':EndDate' => $EndDate,
                            ':ModifiedUserID' => $this->controller->user->UserID,
                            ':ModifiedDate' => date("Y-m-d H:i:s"),
                            ':ProductID' => $args['ProductID']  
                        

                        )

                    );


            if($result)
            {
                    return array('status' => 'OK',
                            'message' => $this->controller->page['Text']['data_updated_msg']);
            }
            else
            {
                return array('status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed']);
            }
         }
         else
         {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
         }   
       
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    
}
?>