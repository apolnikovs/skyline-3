
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    
    <div id="TradeAccountFormPanel" class="SystemAdminFormPanel" >
    
                <form id="TradeAccountForm" name="TradeAccountForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="taSuggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                         
                         <p>
                            <label class="fieldLabel" for="TradeAccount" >{$page['Labels']['trade_account']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="TradeAccount" value="{$datarow.TradeAccount|escape:'html'}" id="TradeAccount" maxlength="40" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="Postcode" >{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; <input  type="text" class="text uppercaseText"  name="Postcode" value="{$datarow.Postcode|escape:'html'}" id="Postcode"  maxlength="10" >

                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="Monday" >{$page['Labels']['day']|escape:'html'}:</label>

                           
                            
                            <div class="checkBoxText" >
                                
                            <input  type="checkbox"  name="Monday" value="Yes" {if $datarow.Monday eq 'Yes' } checked="checked" {/if} id="Monday" > {$page['Text']['monday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Tuesday" value="Yes" {if $datarow.Tuesday eq 'Yes' } checked="checked" {/if}  id="Tuesday" > {$page['Text']['tuesday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Wednesday" value="Yes" {if $datarow.Wednesday eq 'Yes' } checked="checked" {/if}  id="Wednesday" > {$page['Text']['wednesday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Thursday" value="Yes" {if $datarow.Thursday eq 'Yes' } checked="checked" {/if}  id="Thursday" > {$page['Text']['thursday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Friday" value="Yes" {if $datarow.Friday eq 'Yes' } checked="checked" {/if}  id="Friday" > {$page['Text']['friday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Saturday" value="Yes" {if $datarow.Saturday eq 'Yes' } checked="checked" {/if}  id="Saturday" > {$page['Text']['saturday']|escape:'html'}<br>
                            <input  type="checkbox"  name="Sunday" value="Yes" {if $datarow.Sunday eq 'Yes' } checked="checked" {/if}  id="Sunday" > {$page['Text']['sunday']|escape:'html'}<br>
                            
                            </div>

                          </p>

                          
                          
                          {if $datarow.ServiceProviderTradeAccountID neq '' && $datarow.ServiceProviderTradeAccountID neq '0'}
                              
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                               
                               <input  type="radio" name="Status"  value="Active" {if $datarow.Status eq 'Active'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['active']|escape:'html'}</span> 
                               <input  type="radio" name="Status"  value="In-active" {if $datarow.Status eq 'In-active'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['inactive']|escape:'html'}</span> 

                               
                          </p>
                          {/if}
                          
                         

                            <p style="text-align:center;" >
                    
                               

                                    <input type="hidden" name="ServiceProviderTradeAccountID"  value="{$datarow.ServiceProviderTradeAccountID|escape:'html'}" >
                                    <input type="hidden" name="ServiceProviderID"  value="{$ServiceProviderID|escape:'html'}" >
                                   
                                    {if $datarow.ServiceProviderTradeAccountID neq '' && $datarow.ServiceProviderTradeAccountID neq '0'}
                                        
                                        <input type="submit" name="ta_update_save_btn" class="textSubmitButton" id="ta_update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="hidden" name="Status"  value="{$datarow.Status|escape:'html'}" >
                                        
                                        <input type="submit" name="ta_insert_save_btn" class="textSubmitButton" id="ta_insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        &nbsp;&nbsp;
                                        
                                        <input type="submit" name="ta_cancel_btn" class="textSubmitButton" id="ta_cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
