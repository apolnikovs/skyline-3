# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.263');

# ---------------------------------------------------------------------- #
# Add Table sp_part_order_status                                         #
# ---------------------------------------------------------------------- # 
CREATE TABLE sp_part_order_status (
									SPPartOrderStatusID INT(10) NOT NULL AUTO_INCREMENT, 
									OrderStatusName VARCHAR(50) NULL DEFAULT NULL, 
									OrderStatusDescription VARCHAR(250) NULL DEFAULT NULL, 
									Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', 
									PRIMARY KEY (SPPartOrderStatusID) 
								   ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Modify Table sp_part_order                                             #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_order ADD COLUMN SPPartOrderStatusID INT(10) NULL AFTER Status;


# ---------------------------------------------------------------------- #
# Modify Table sp_part_stock_item                                        #
# ---------------------------------------------------------------------- # 
ALTER TABLE sp_part_stock_item CHANGE COLUMN CreatedUserID CreatedUserID INT(11) NULL DEFAULT NULL AFTER CreatedDate, 
							    ADD COLUMN PartID INT(11) NOT NULL AFTER CreatedUserID;

# ---------------------------------------------------------------------- #
# Parts/Stock Tables Additional Data                                     #
# ---------------------------------------------------------------------- # 
INSERT INTO part_status (PartStatusID, PartStatusName, PartStatusDescription, ServiceProviderID, PartStatusDisplayOrder, DisplayOrderSection, Status, Available, InStock) VALUES (18, '18 REQUSITIONED', '?', NULL, 18, 'No', 'Active', 'N', 'N');
INSERT INTO status (StatusID, StatusName, TimelineStatus, Colour, Category, Branch, ServiceProvider, FieldCall, Support, PartsOrders, RelatedTable, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate, StatusTAT, StatusTATType) VALUES (48, '98 ATTENTION REQUIRED', 'parts_ordered', 'bf5f00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '', 0, 'Minutes');
INSERT INTO sp_part_order_status (SPPartOrderStatusID, OrderStatusName, OrderStatusDescription, Status) VALUES (1, 'On Order', NULL, 'Active'); INSERT INTO sp_part_order_status (SPPartOrderStatusID, OrderStatusName, OrderStatusDescription, Status) VALUES (2, 'Partially Received', NULL, 'Active'); INSERT INTO sp_part_order_status (SPPartOrderStatusID, OrderStatusName, OrderStatusDescription, Status) VALUES (3, 'Order Closed', NULL, 'Active'); INSERT INTO sp_part_order_status (SPPartOrderStatusID, OrderStatusName, OrderStatusDescription, Status) VALUES (4, 'Order Cancelled', NULL, 'Active');
REPLACE INTO part_status (PartStatusID, PartStatusName, PartStatusDescription, ServiceProviderID, PartStatusDisplayOrder, DisplayOrderSection, Status, Available, InStock) VALUES (6, '06 PART NO LONGER AVALIABLE', 'Part is not available from supplier', NULL, 6, 'No', 'Active', 'N', 'N');



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.264');
