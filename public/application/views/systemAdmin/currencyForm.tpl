 <script type="text/javascript">   
     
 $(document).ready(function() {
 
    
   }); 
   function checkAtomationStatus()
   {
   if($('#AutomaticExchangeRateCalculation').attr('checked')!="checked"){
   $('#manualFields').show();
   }else{
   $('#manualFields').hide();
   }
   }
  </script>  
    
    <div id="CurrencyFormPanel" class="SystemAdminFormPanel" >
    
                <form id="CurrencyForm" name="CurrencyForm" method="post"  action="{$_subdomain}/LookupTables/saveCurrency" class="inline" >
       
                <fieldset>
                    <legend title="" >Currency</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                       
                         
                         
                         <p>
                            <label class="cardLabel" for="StatusName" >Currency Name:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CurrencyName" value="{$datarow.CurrencyName|escape:'html'}" id="CurrencyName" >
                        
                         </p>
                        
                         <p>
                            <label class="cardLabel" for="CurrencyCode" >Currency Code:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CurrencyCode" value="{$datarow.CurrencyCode|escape:'html'}" id="CurrencyCode" >
                        
                         </p>
                         <p>
                         <label class="cardLabel" for="CurrencySymbol" >Currency Symbol:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="CurrencySymbol" value="{$datarow.CurrencySymbol|escape:'html'}" id="CurrencySymbol" >
                        
                         </p>
                         
                         <p>
                         <label class="cardLabel" for="AutomaticExchangeRateCalculation" >Automatic Exchange Rate Calculation:</label>
                           
                         &nbsp;&nbsp; <input style="margin-top: 20px" onclick="checkAtomationStatus()"  type="checkbox" {if $datarow.AutomaticExchangeRateCalculation=="Yes"}checked="checked"{/if} value="Yes"  name="AutomaticExchangeRateCalculation"  id="AutomaticExchangeRateCalculation" >
                        
                         </p>
                         <div {if $datarow.AutomaticExchangeRateCalculation=="Yes"}style="display:none"{/if} id="manualFields">
                         <p>
                            <label class="cardLabel" for="ConversionCalculation" >Conversion Calculation:</label>
                           
                            &nbsp;&nbsp; <input {if $datarow.ConversionCalculation=="Multiply"}checked=checked{/if}  type="radio"  name="ConversionCalculation" value="Multiply" id="ConversionCalculation1" > <label class="saFormSpan">Multiple By</label>         
                            &nbsp;&nbsp; <input {if $datarow.ConversionCalculation=="Divide"}checked=checked{/if}  type="radio"  name="ConversionCalculation" value="Divide" id="WConversionCalculation2" > <label class="saFormSpan">Divide By</label>         
                        
                        </p>
                         <p><label class="cardLabel" for="ExchangeRate" >Exchange Rate:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="ExchangeRate" value="{$datarow.ExchangeRate|escape:'html'}" id="ExchangeRate" >
                        
                         </p>
                         </div>
                         <p>
                         <label class="cardLabel" for="ExchangeRateLastModified" >Last Updated:</label>
                         <label class="saFormSpan">{$datarow.ExchangeRateLastModified|escape:'html'}</label>
                         </p>
                         <p>
                         <label class="cardLabel" for="HistoricAverageCost" >Historic Average Cost:</label>
                         <label class="saFormSpan">{$datarow.HistoricAverageCost|string_format:"%.4f"}</label>
                         </p>
                         
                         {if $datarow.CurrencyID neq '' && $datarow.CurrencyID neq '0'}
                             
                          <p>
                            <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    

                          </p>
                          
                         {/if}
                         <input type="hidden" name="CurrencyID" value="{$datarow.CurrencyID}">
                     

                         <p>
                <hr>
                               
                                <div style="height:20px;margin-bottom: 10px">
                                <button type="submit" style="float: left" class="gplus-blue">Save</button>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Cancel</button>
                                </div>
            </p>    

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
