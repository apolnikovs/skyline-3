
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.235');


# ---------------------------------------------------------------------- #
# Modify table completion_status                                         #
# ---------------------------------------------------------------------- #

SET foreign_key_checks = 0;

ALTER TABLE `completion_status`
 CHANGE COLUMN `CompletionStatus` `CompletionStatus` VARCHAR(250) NULL DEFAULT NULL AFTER `ServiceCategory`;
 
 REPLACE INTO `completion_status` (`CompletionStatusID`, `JobCategory`, `ServiceCategory`, `CompletionStatus`, `Description`, `Sort`, `BrandID`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, NULL, NULL, 'COMPLETE JOB', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:08'),
	(2, NULL, NULL, 'NFF', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:11'),
	(3, NULL, NULL, 'PARTS UNAVAILABLE', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:14'),
	(4, NULL, NULL, 'BER', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:15'),
	(5, NULL, NULL, 'RETURNED WITHOUT FIX', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:17'),
	(6, NULL, NULL, 'EXCHANGED', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 11:21:18'),
	(7, NULL, NULL, 'SOFT SERVICE', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(8, NULL, NULL, 'INSPECTION', NULL, NULL, 1000, '2012-09-03 16:19:26', '0000-00-00 00:00:00', 'Active', 1, '2012-09-03 16:19:26'),
	(9, 'service', 'physical', 'Technical Fix', 'This is where the product was found to have a genuine \'Technical Fault\' which was resoved by an engineer taking the appropriate action i.e. replacing faulty parts etc.', 1, 1000, '2013-04-24 11:58:59', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:16'),
	(10, 'service', 'physical', 'Software/Firmware Update', 'This is where the product was found to have a genuine \'Technical Fault\' which was resolved by an engineer carrying out a recognised software/firmware update.', 2, 1000, '2013-04-24 11:59:11', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:18'),
	(11, 'service', 'physical', 'Non-Technical Fix', 'This is only where  the problem is found to be a \'non-technical\' fault such as cosmetic damage but is covered by the warrantor such as under an \'Accidental Damage\' policy or on a chargeable basis to the consumer. This does not include NFF etc.', 3, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:20'),
	(12, 'service', 'physical', 'Parts Not Available', 'This is where a part was found to be faulty within the product but was not available from any authorised supplier and is therefore returned unrepaired.', 4, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:21'),
	(13, 'service', 'physical', 'Beyond Economic Repair/Estimate Refused', 'This is where parts are available (if required) to repair the product but the cost of repairing the product was either beyond a given repair limit or the estimate given was rejected by the client or consumer.', 5, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:23'),
	(14, 'service', 'physical', 'No Fault Found', 'This is where either the fault reported did not appear or was proven not to be a genuine \'Technical Fault\' with the product i.e. problem was down to customer expectation, the customer required education in functionality or an external product or accessory has caused the \'fault\'.', 6, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:25'),
	(15, 'service', 'physical', 'Customer Liability – not covered by warranty', 'This is where the problem reported is found to be due to something not covered by the warrantor such as physical damage or water ingression. This is not relevant for product covered by an \'Accidental Damage\' policy.', 7, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:27'),
	(16, 'service', 'physical', 'Customer Refused Service', 'This is where the customer or client stops the repair process, once a repair has been has been attempted i.e. engineer called to the premises or product bought into the workshop. This does not include jobs cancelled prior to a field call.', 8, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:29'),
	(17, 'service', 'physical', 'Insurance Inspection Complete', 'This is where an Insurance Inspection is booked but a full repair is not required. In these instances a report is sent to the client to confirm the problem meets the terms of the policy but is not going to be repaired.', 9, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:32'),
	(18, 'service', 'soft', 'Soft Service – Customer Education', 'This is where an engineer contacts the customer prior to a visit and resolves the problem to the customers satisfaction. This is for non-technical faults such as set up issues etc.', 21, 1000, '2013-04-24 12:06:15', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:41'),
	(19, 'service', 'soft', 'Soft Service – Parts Not Available', 'This is where an engineer contacts the customer prior to a visit and diagnoses the fault, identifying that the part required is not available and therefore can close the job without a visit. ', 22, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:45'),
	(20, 'service', 'soft', 'Soft Service – BER', 'This is where an engineer contacts the customer prior to a visit and diagnoses the fault, identifying that the cost of the repair would either exceed the repair limit given or the estimate given was rejected by the client or consumer without any visit taking place.', 23, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:48'),
	(21, 'service', 'soft', 'Soft Service – Customer Liability', 'This is where an engineer contacts the customer prior to a visit and diagnoses the fault, identifying that the problem is due to something not covered by the warrantor such as physical damage or water ingression. This is not relevant for product covered by an \'Accidental Damage\' policy.', 24, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:52'),
	(22, 'service', 'soft', 'Soft Service – Part/Accessory sent', 'This is where an engineer contacts the customer prior to a visit and diagnoses the fault, identifying that the part required could be sent directly to the customer without any technical interventation taking place such as a faulty remote control or memory stick for a software upgrade.', 25, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:13:56'),
	(23, 'installation', '', 'Installation Completed', 'This is where the installation requested has been completed to the customer\'s satisfaction.', 41, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:13'),
	(24, 'installation', '', 'Installation not Possible', 'This is where the installation requested could not be completed despite the installer attending the job, due to circumstances such as it not being safe to do so and no alternative could be found e.g. a TV wall mount where either the wall or bracket were not suitable.', 42, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:16'),
	(25, 'in_home_support', '', 'Technical Fix - Accessory etc.', 'This is where an engineer has attended a premises to offer technical support and resolve a problem and an item is found to be faulty and fixed. If a product is found to be faulty and fixed the job should be changed to a \'Repair\'.', 61, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:24'),
	(26, 'in_home_support', '', 'Replace Faulty Accessory', 'This is where an engineer has attended a premises to offer technical support and resolve a problem and an accessory is found to be faulty and replaced, such as an HDMI lead.', 62, 1000, '2013-04-24 12:06:16', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:26'),
	(27, 'in_home_support', '', 'Educated Customer', 'This is where an engineer has attended a premises to offer technical support and resolve a problem and the issue is found to be due to a non-technical problem such as the customer misunderstanding functionailty etc.', 63, 1000, '2013-04-24 12:06:17', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:28'),
	(28, 'in_home_support', '', 'Tutorial Completed', 'This is where an engineer has attended a premises to give a tutorial on a product or system and is completed to the customer\'s satisfaction.', 64, 1000, '2013-04-24 12:06:17', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:31'),
	(29, 'in_home_support', '', 'Unable to Complete Successfully', 'This is where an engineer has attended a premises to offer technical support and resolve a problem or give a tutorial but is unable to resolve the issue or complete to the customer\'s satisfaction.', 65, 1000, '2013-04-24 12:08:53', '0000-00-00 00:00:00', 'Active', 1, '2013-04-24 12:14:33');


 SET foreign_key_checks = 1;
 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.236');
